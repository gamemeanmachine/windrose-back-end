module gitlab.com/gamemeanmachine/windrose-back-end

go 1.12

require (
	gitlab.com/gamemeanmachine/golang-support v1.2.2
	gitlab.com/gamemeanmachine/resource-servers/v2 v2.0.1
)
