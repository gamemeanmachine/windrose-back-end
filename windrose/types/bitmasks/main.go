package bitmasks

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"math"
)

// The check types for cells in a bitmask involve choosing
// between "checking all", or "checking until one", and the
// target condition being "blocked" or "free".
type CheckType int

const (
	CheckAnyBlocked = iota
	CheckAnyFree
	CheckAllBlocked
	CheckAllFree
)

// Bitmasks are an efficient way to store bi-dimensional data
// with a size of at most 32768x32768, and query sub-chunks of
// it for other purposes. They will only exist in-memory.
type Bitmask struct {
	width, height uint16
	data          []uint64
}

// Creates a new bitmask, filling it with either 1s or 0s, which
// depends on the fill value.
func NewBitMask(width, height uint16, fill bool) Bitmask {
	if width == 0 || height == 0 {
		panic(errors.Argument("windrose.types.bitmasks.NewBitmask", "width/height", "Neither of them must be zero"))
	}

	data := make([]uint64, (int(width)*int(height)+63)/64)
	// data is already initialized with 0s.
	if fill {
		// In this case, we want to initialize all the data to 1s.
		for index := range data {
			data[index] = math.MaxUint64
		}
	}
	return Bitmask{width, height, data}
}

// Creates a bitmask as a copy of another one.
func CopyBitmask(source Bitmask) Bitmask {
	if source.width == 0 || source.height == 0 {
		panic(errors.Argument("windrose.types.bitmasks.NewBitmask", "source's width/height", "Neither of them must be zero"))
	}

	if source.data == nil {
		panic(errors.Argument("windrose.types.bitmasks.NewBitmask", "source's data", "Neither of them must not be nil"))
	}

	dataCopy := make([]uint64, len(source.data))
	copy(dataCopy, source.data)
	return Bitmask{
		source.width, source.height,
		dataCopy,
	}
}

// Checks that both bitmasks have same dimensions.
func (bitmask Bitmask) CheckSameDimensions(other Bitmask, function string) {
	if bitmask.width != other.width || bitmask.height != other.height {
		panic(errors.Argument("windrose.types.bitmask."+function, "source's width/height", "Must equal current bitmask's width/height"))
	}
}

// Gets a bit at certain (x, y) position.
func (bitmask Bitmask) at(x, y uint16) bool {
	flatIdx := uint32(x) + uint32(y)*uint32(bitmask.width)
	return bitmask.data[flatIdx/64]&uint64(1<<(flatIdx%64)) != 0
}

// Sets a bit at certain (x, y) position.
func (bitmask Bitmask) setAt(x, y uint16, bit bool) {
	flatIdx := uint32(x) + uint32(y)*uint32(bitmask.width)
	if bit {
		bitmask.data[flatIdx/64] |= uint64(1 << (flatIdx % 64))
	} else {
		bitmask.data[flatIdx/64] &= uint64(1 << (flatIdx % 64))
	}
}

// Fills the whole bitmask.
func (bitmask Bitmask) Fill(bit bool) {
	fillValue := uint64(0)
	if bit {
		fillValue = math.MaxUint64
	}
	for index := range bitmask.data {
		bitmask.data[index] = fillValue
	}
}

// Clones the whole bitmask.
func (bitmask Bitmask) Clone() Bitmask {
	return CopyBitmask(bitmask)
}

// Makes the current bitmask = bitmask | other.
func (bitmask Bitmask) Unite(other Bitmask) {
	bitmask.CheckSameDimensions(other, "Bitmap::Unite")
	for idx := range bitmask.data {
		bitmask.data[idx] |= other.data[idx]
	}
}

// Makes the current bitmask = bitmask & other.
func (bitmask Bitmask) Intersect(other Bitmask) {
	bitmask.CheckSameDimensions(other, "Bitmap::Intersect")
	for idx := range bitmask.data {
		bitmask.data[idx] &= other.data[idx]
	}
}

// Makes the current bitmask = ~bitmask.
func (bitmask Bitmask) Invert() {
	for idx := range bitmask.data {
		bitmask.data[idx] = ^bitmask.data[idx]
	}
}

// Makes the current bitmask = bitmask - other.
func (bitmask Bitmask) Subtract(other Bitmask) {
	bitmask.CheckSameDimensions(other, "Bitmap::Subtract")
	for idx := range bitmask.data {
		bitmask.data[idx] &= ^other.data[idx]
	}
}

// Makes the current bitmask = bitmask ^ other.
func (bitmask Bitmask) SymmetricSubtract(other Bitmask) {
	bitmask.CheckSameDimensions(other, "Bitmap::SymmetricSubtract")
	for idx := range bitmask.data {
		bitmask.data[idx] ^= other.data[idx]
	}
}

// Creates a new bitmask = bitmask | other.
func (bitmask Bitmask) Union(other Bitmask) Bitmask {
	clone := CopyBitmask(bitmask)
	clone.Unite(other)
	return clone
}

// Creates a new bitmask = bitmask & other.
func (bitmask Bitmask) Intersection(other Bitmask) Bitmask {
	clone := CopyBitmask(bitmask)
	clone.Intersect(other)
	return clone
}

// Creates a new bitmask = ~bitmask.
func (bitmask Bitmask) Inverted() Bitmask {
	clone := CopyBitmask(bitmask)
	clone.Invert()
	return clone
}

// Creates a new bitmask = bitmask - other.
func (bitmask Bitmask) Difference(other Bitmask) Bitmask {
	clone := CopyBitmask(bitmask)
	clone.Subtract(other)
	return clone
}

// Creates a new bitmask = bitmask ^ other.
func (bitmask Bitmask) SymmetricDifference(other Bitmask) Bitmask {
	clone := CopyBitmask(bitmask)
	clone.SymmetricSubtract(other)
	return clone
}

// Puts the contents of this bitmask into a new one, at a specific
// offset, with a default filling value for the new bitmap, and
// considering the new bitmask can have a new size.
func (bitmask Bitmask) Translated(newWidth, newHeight uint16, offsetX, offsetY int32, newFillingValue bool) Bitmask {
	result := NewBitMask(newWidth, newHeight, newFillingValue)
	if offsetX < int32(newWidth) && offsetY < int32(newHeight) && (offsetX+int32(newWidth)) > 0 && (offsetY+int32(newHeight)) > 0 {
		startX := values.MaxI32(offsetX, 0)
		endX := values.MinI32(offsetX+int32(bitmask.width), int32(newWidth))
		startY := values.MaxI32(offsetY, 0)
		endY := values.MinI32(offsetY+int32(bitmask.height), int32(newHeight))
		for x := startX; x < endX; x++ {
			for y := startY; y < endY; y++ {
				result.setAt(uint16(x), uint16(y), bitmask.at(uint16(x-offsetX), uint16(y-offsetY)))
			}
		}
	}
	return result
}

// Fills with some value the required square delimited from xi->xf
// and yi->yf (including both the -i and -f bounds).
func (bitmask Bitmask) SetSquare(xi, xf, yi, yf uint16, bit bool) {
	xi = values.MinU16(xi, bitmask.width-1)
	xf = values.MinU16(xf, bitmask.width-1)
	yi = values.MinU16(yi, bitmask.height-1)
	yf = values.MinU16(yf, bitmask.height-1)
	xi_ := values.MinU16(xi, xf)
	xf_ := values.MaxU16(xi, xf)
	yi_ := values.MinU16(yi, yf)
	yf_ := values.MaxU16(yi, yf)

	for x := xi_; x <= xf_; x++ {
		for y := yi_; y < yf_; y++ {
			bitmask.setAt(uint16(x), uint16(y), bit)
		}
	}
}

// Performs the required check inside a square delimited from xi->xf
// and yi->yf (including both the -i and -f bounds).
func (bitmask Bitmask) Square(xi, xf, yi, yf uint16, checkType CheckType) bool {
	xi = values.MinU16(xi, bitmask.width-1)
	xf = values.MinU16(xf, bitmask.width-1)
	yi = values.MinU16(yi, bitmask.height-1)
	yf = values.MinU16(yf, bitmask.height-1)
	xi_ := values.MinU16(xi, xf)
	xf_ := values.MaxU16(xi, xf)
	yi_ := values.MinU16(yi, yf)
	yf_ := values.MaxU16(yi, yf)

	for x := xi_; x <= xf_; x++ {
		for y := yi_; y < yf_; y++ {
			switch checkType {
			case CheckAllFree:
				if bitmask.at(x, y) {
					return false
				}
			case CheckAllBlocked:
				if !bitmask.at(x, y) {
					return false
				}
			case CheckAnyFree:
				if !bitmask.at(x, y) {
					return true
				}
			case CheckAnyBlocked:
				if bitmask.at(x, y) {
					return true
				}
			default:
				panic(errors.Argument("windrose.types.bitmasks.Bitmask::Square", "checkType", "Must be one of the default types"))
			}
		}
	}
	if checkType == CheckAllBlocked || checkType == CheckAllFree {
		// The default case for any 'all' check, is to return true.
		return true
	} else {
		// The default case for the other ('any') checks, is to return false.
		return false
	}
}

// Iterates over a square of this bitmask and its values.
func (bitmask Bitmask) Enumerate(xi, xf, yi, yf uint16, callback func(x, y uint16, bit bool)) {
	xi = values.MinU16(xi, bitmask.width-1)
	xf = values.MinU16(xf, bitmask.width-1)
	yi = values.MinU16(yi, bitmask.height-1)
	yf = values.MinU16(yf, bitmask.height-1)
	xi_ := values.MinU16(xi, xf)
	xf_ := values.MaxU16(xi, xf)
	yi_ := values.MinU16(yi, yf)
	yf_ := values.MaxU16(yi, yf)

	for x := xi_; x <= xf_; x++ {
		for y := yi_; y < yf_; y++ {
			callback(x, y, bitmask.at(x, y))
		}
	}
}

// Fills with some value the required row delimited from xi->xf
// and yi->yi (including both the -i and -f bounds).
func (bitmask Bitmask) SetRow(xi, xf, y uint16, bit bool) {
	bitmask.SetSquare(xi, xf, y, y, bit)
}

// Performs the required check inside a row delimited from xi->xf
// and yi->yi (including both the -i and -f bounds).
func (bitmask Bitmask) Row(xi, xf, y uint16, checkType CheckType) bool {
	return bitmask.Square(xi, xf, y, y, checkType)
}

// Fills with some value the required column delimited from xi->xi
// and yi->yf (including both the -i and -f bounds).
func (bitmask Bitmask) SetColumn(x, yi, yf uint16, bit bool) {
	bitmask.SetSquare(x, x, yi, yf, bit)
}

// Performs the required check inside a column delimited from xi->xi
// and yi->yf (including both the -i and -f bounds).
func (bitmask Bitmask) Column(x, yi, yf uint16, checkType CheckType) bool {
	return bitmask.Square(x, x, yi, yf, checkType)
}

// Sets a bit a certain position, clamping it.
func (bitmask Bitmask) SetCell(x, y uint16, bit bool) {
	x = values.MinU16(x, bitmask.width-1)
	y = values.MinU16(x, bitmask.height-1)
	bitmask.setAt(x, y, bit)
}

// Gets a bit from a certain position, clamping it.
func (bitmask Bitmask) Cell(x, y uint16) bool {
	x = values.MinU16(x, bitmask.width-1)
	y = values.MinU16(x, bitmask.height-1)
	return bitmask.at(x, y)
}

// Sets the whole data as a raw array of 64 bits.
func (bitmask Bitmask) Set(data []uint64) {
	if len(data) != len(bitmask.data) {
		panic(errors.Argument("windrose.types.bitmasks.Bitmask::Set", "data", "Has to set the same length as the source's data"))
	}
	for index, value := range data {
		bitmask.data[index] = value
	}
}

// Gets the bitmask's logical Width.
func (bitmask Bitmask) Width() uint16 {
	return bitmask.width
}

// Gets the bitmask's logical Height.
func (bitmask Bitmask) Height() uint16 {
	return bitmask.height
}

// Gets the bitmask's inner data.
func (bitmask Bitmask) Dump() []uint64 {
	dataCopy := make([]uint64, len(bitmask.data))
	copy(dataCopy, bitmask.data)
	return dataCopy
}
