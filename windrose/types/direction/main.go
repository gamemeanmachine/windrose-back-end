package direction


// There are only 4 directions: Up, Down, Left, Right.
type Direction int


const (
	Down Direction = iota
	Left
	Right
	Up
	None = -1
	Front = Down
)


// Directions are meant to know their opposite:
// Up vs. Down, Left vs. Right.
func (direction Direction) Opposite() Direction {
	switch direction {
	case Down:
		return Up
	case Up:
		return Down
	case Left:
		return Right
	case Right:
		return Left
	}
	return None
}