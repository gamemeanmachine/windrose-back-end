package map_templates

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	errors2 "gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/errors"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/entities"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/floors"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/visuals"
	"math"
)

// Map templates can specify up to 32768 width / height,
// and will contain, inside, one or more "template layers"
// that can be added. Each "template layer" will know what
// to provide (or what data to serve) accordingly. The
// idea behind this class is to serve data:
// - To the client, as a resource, considering only the
//   information that would be needed (for floors and
//   ceilings: all the information, since they only have
//   tile references).
// - To the server, by direct access (and object casting),
//   the entire object.
// Aside from the template layers, management strategies
// can also be added.
type MapTemplate struct {
	resources.BaseObject
	// The map width/height, in cells.
	width, height uint16
	// The map's cell width/height, in game units.
	// This will apply to ALL THE LAYERS with grid-based
	// behaviour (both in client and server logic). In
	// server-side, this value is just informative as it
	// will seldom to never affect the logic / experience,
	// since this value will only be used by the front-end.
	cellWidth, cellHeight uint16
	caption               string
	layers                map[string]layers.MapTemplateLayer
}

// Gets the contents of the current map template.
// Also: compliance with the `resources.Object` interface.
func (mapTemplate *MapTemplate) Content() interface{} {
	data := map[string]interface{}{}
	for layerType, layer := range mapTemplate.layers {
		data[layerType] = layer.MapTemplateLayerData()
	}
	return struct {
		Width      uint16
		Height     uint16
		CellWidth  uint16
		CellHeight uint16
		Caption    string
		Layers     map[string]interface{}
	}{
		mapTemplate.width,
		mapTemplate.height,
		mapTemplate.cellWidth,
		mapTemplate.cellHeight,
		mapTemplate.caption,
		data,
	}
}

// Adds a by-default-empty layer.
func (mapTemplate *MapTemplate) addIfMissing(layer layers.MapTemplateLayer) {
	layerTypeKey := layer.MapTemplateLayerType()
	if _, ok := mapTemplate.layers[layerTypeKey]; !ok {
		mapTemplate.AddLayer(layer)
	}
}

// Inflates a new map template from the input data.
// Also: compliance with the `resources.Object` interface.
func (mapTemplate *MapTemplate) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(dump.(map[string]interface{}))
	template := &MapTemplate{
		resources.NewBaseObject(id),
		fromMap.Get("Width").AsUint16(),
		fromMap.Get("Height").AsUint16(),
		fromMap.Get("CellWidth").AsUint16(),
		fromMap.Get("CellHeight").AsUint16(),
		fromMap.Get("Caption").AsString(),
		map[string]layers.MapTemplateLayer{},
	}
	for key, layer := range fromMap["Layers"].(map[string]interface{}) {
		if layerType := layers.GetPlugIn(key); layerType == nil {
			panic(errors2.UnknownMapLayerContentType(key))
		} else {
			template.layers[key] = layerType.MapTemplateLayerLoad(
				template.width, template.height,
				layer.(map[string]interface{}), registry,
			)
		}
	}
	// Adds the 3 layers just in case that, by chance, one of them was not
	// present in the import.
	template.addIfMissing(entities.NewEntitiesTemplateLayer(mapTemplate.width, mapTemplate.height))
	template.addIfMissing(floors.NewFloorsTemplateLayer(mapTemplate.width, mapTemplate.height))
	template.addIfMissing(visuals.NewVisualsTemplateLayer(mapTemplate.width, mapTemplate.height))
	return template
}

// Gets the map's dimensions, in cells.
func (mapTemplate *MapTemplate) Size() (uint16, uint16) {
	return mapTemplate.width, mapTemplate.height
}

// Get the map's cell size, in game units.
func (mapTemplate *MapTemplate) CellSize() (uint16, uint16) {
	return mapTemplate.cellWidth, mapTemplate.cellHeight
}

// Gets a single map's layer.
func (mapTemplate *MapTemplate) Layer(layerType string) layers.MapTemplateLayer {
	layer, _ := mapTemplate.layers[layerType]
	return layer
}

// Adds a layer to this map. The layer must not belong to another map,
// and only one layer can be assigned for each layer type.
func (mapTemplate *MapTemplate) AddLayer(layer layers.MapTemplateLayer) {
	if layer == nil {
		panic(errors.Argument(
			"windrose.resources.map_templates.MapTemplate::AddLayer",
			"layer", "Must not be nil",
		))
	} else if currentLayer, ok := mapTemplate.layers[layer.MapTemplateLayerType()]; ok {
		if currentLayer != layer {
			panic(errors.Argument(
				"windrose.resources.map_templates.MapTemplate::AddLayer",
				"layer", "Another layer of the same type is already added",
			))
		}
	} else {
		width, height := layer.MapTemplateLayerSize()
		if width != mapTemplate.width || height != mapTemplate.height {
			panic(errors.Argument(
				"windrose.resources.map_templates.MapTemplate::AddLayer",
				"layer", "The target layer's dimensions do not match the map's dimensions",
			))
		}
		mapTemplate.layers[layer.MapTemplateLayerType()] = layer
	}
}

// Creates a map template. The caption is purely a debug or
// informative feature that doesn't become visible or relevant
// to the game experience, but just for internal purposes.
// The map comes with 3 layers by default.
func NewMapTemplate(id uint64, caption string, width, height, cellWidth, cellHeight uint16) *MapTemplate {
	width = values.ClampU16(1, width, uint16(math.MaxInt16))
	height = values.ClampU16(1, height, uint16(math.MaxInt16))
	mapLayers := map[string]layers.MapTemplateLayer{}
	template := &MapTemplate{resources.NewBaseObject(id), width, height, cellWidth, cellHeight, caption, mapLayers}
	template.AddLayer(entities.NewEntitiesTemplateLayer(width, height))
	template.AddLayer(floors.NewFloorsTemplateLayer(width, height))
	template.AddLayer(visuals.NewVisualsTemplateLayer(width, height))
	return template
}
