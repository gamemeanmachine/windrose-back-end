package ceilings

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// This layer template provides information regarding the
// tilemaps that make the ceilings in a map.
type CeilingsTemplateLayer struct {
	layers.BaseMapTemplateLayer
	ceilings []Ceiling
}


// The map template layer type is "ceilings" for this type.
func (*CeilingsTemplateLayer) MapTemplateLayerType() string {
	return "ceilings"
}


// Adds another ceiling, on top of the existing ones.
func (ceilingsTemplateLayer *CeilingsTemplateLayer) AddCeiling(ceiling Ceiling) {
	if ceiling == nil {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.ceilings.CeilingsTemplateLayer::AddCeiling",
			"ceiling", "Must not be nil",
		))
	} else {
		ceilingsTemplateLayer.ceilings = append(ceilingsTemplateLayer.ceilings, ceiling)
	}
}


// Dumps the data of all its ceilings as (CeilingType / CeilingData).
func (ceilingsTemplateLayer *CeilingsTemplateLayer) MapTemplateLayerData() interface{} {
	ceilings := make([]interface{}, len(ceilingsTemplateLayer.ceilings))
	for index, ceiling := range ceilingsTemplateLayer.ceilings {
		x, y := ceiling.Offset()
		mode := ceiling.CeilingMode()
		alpha := ceiling.CeilingAlpha()
		ceilings[index] = struct {
			Type    string
			X, Y    int16
			Mode    CeilingMode
			Alpha   float32
			Tilemap interface{}
		} {
			ceiling.CeilingType(),
			x, y, mode,alpha,
			ceiling.Tilemap().TilemapData(),
		}
	}
	return struct {
		Ceilings []interface{}
	} {
		ceilings,
	}
}


// Creates a new instance, by loading all the registered ceilings.
func (*CeilingsTemplateLayer) MapTemplateLayerLoad(width, height uint16, data map[string]interface{}, registry *resources.Registry) layers.MapTemplateLayer {
	ceilingsTemplateLayer := NewCeilingsTemplateLayer(width, height)
	ceilingsData := data["Ceilings"].([]interface{})
	ceilingsTemplateLayer.ceilings = make([]Ceiling, len(ceilingsData))
	for index, value := range ceilingsData {
		value := from.Map(value.(map[string]interface{}))
		ceilingsTemplateLayer.ceilings[index] = plugins[value.Get("Type").AsString()].CeilingLoad(
			value.Get("X").AsInt16(),
			value.Get("Y").AsInt16(),
			CeilingMode(value.Get("CeilingMode").AsUint()),
			value.Get("Alpha").AsFloat32(),
			value["Data"].(map[string]interface{}), registry,
		)
	}
	return ceilingsTemplateLayer
}


// Creates a new instance, by specifying just its dimensions.
func NewCeilingsTemplateLayer(width, height uint16) *CeilingsTemplateLayer {
	return &CeilingsTemplateLayer{
		layers.NewBaseMapTemplateLayer(width, height),
		[]Ceiling{},
	}
}


// Registers this MapTemplateLayer.
var _ = layers.AddPlugIn(&CeilingsTemplateLayer{})


/////////////////////////////////////////////////////////
// Plug-in system for ceilings.
/////////////////////////////////////////////////////////


var plugins = map[string]Ceiling{}


// Register a Ceiling-implementing type.
func AddPlugIn(content Ceiling) bool {
	if content == nil {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.ceilings.AddPlugIn",
			"content", "Must not be nil",
		))
	} else if registered, ok := plugins[content.CeilingType()]; ok && registered != content {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.ceilings.AddPlugIn",
			"content", "Another plugin of the same type is already registered",
		))
	} else if ok {
		return false
	} else {
		plugins[content.CeilingType()] = content
		return true
	}
}
