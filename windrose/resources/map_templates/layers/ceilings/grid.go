package ceilings

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/tilemaps"
)


// Grid floors specify their tiles directly.
type GridCeiling struct {
	BaseCeiling
	tilemap *tilemaps.GridTilemap
}


// Gets the underlying tilemap.
func (gridCeiling *GridCeiling) Tilemap() tilemaps.Tilemap {
	return gridCeiling.tilemap
}


// Dumps its internal data: The underlying tilemap.
func (gridCeiling *GridCeiling) CeilingData() interface{} {
	return struct {
		Tilemap interface{}
	} {
		gridCeiling.Tilemap().TilemapData(),
	}
}


// Gets the ceiling type: "grid".
func (gridCeiling *GridCeiling) CeilingType() string {
	return "grid"
}


// Creates a new mono ceiling instance by loading its data.
func (gridCeiling *GridCeiling) CeilingLoad(x, y int16, mode CeilingMode, alpha float32, data map[string]interface{}, registry *resources.Registry) Ceiling {
	return &MonoCeiling{
		BaseCeiling{x, y, mode, alpha},
		(&tilemaps.GridTilemap{}).TilemapLoad(data["Tilemap"].(map[string]interface{}), registry).(*tilemaps.MonoTilemap),
	}
}


// Creates a new Grid Ceiling.
func NewGridCeiling(x, y int16, width, height uint16, mode CeilingMode, alpha float32) *GridCeiling {
	return &GridCeiling{
		NewBaseCeilingData(x, y, mode, alpha),
		tilemaps.NewGridTilemap(width, height),
	}
}


var _ = AddPlugIn(&GridCeiling{})