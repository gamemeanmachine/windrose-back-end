package ceilings

import (
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/tilemaps"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


type CeilingMode uint


const (
	// This ceiling never disappears. Ideally, these ceilings
	// have few "features" (tiles) or "frame-style" features.
	// Objects (or users) will be totally or partially hidden.
	CeilingPermanent = iota
	// This ceiling switches to certain opacity (satisfying
	// 0 <= opacity < 1) when the "main object" enters inside
	// the ceiling. Opacity == 0 means the ceiling disappears
	// completely. The "main object" is usually understood as
	// the "player character" in M.M.O.R.P.Gs.
	CeilingFading
	// In front-end, choosing between one mode or another will
	// mirror to choosing between adding or not the Locally
	// Triggered Ceiling, besides the base ceiling component.
	// Notes: Permanent ceilings will be opaque if alpha = 1,
	// hidden if alpha = 0, and translucent if alpha is in the
	// open interval (0, 1).
	// Notes: Fading ceilings will be opaque by default and:
	// - Nothing else, if alpha == 1.
	// - Will add L.T.C. to Translucent if alpha in (0, 1).
	// - Will add L.T.C. to Hidden if alpha == 0.
	// (L.T.C. == Locally Triggered Ceiling).
)


// A ceiling is actually a tilemap. It will have (width*height)
// tiles, and a Tile / Enumerate*Tiles methods. Also, a
// CeilingType() / CeilingData() method to dump its contents.
// Different to floors, which are always at (0, 0) and have at
// most Width*Height sizes, the dimensions of ceilings can exceed
// the map size and have any (x, y) position (which is not constrained
// to positive values or relation between map dimensions and ceiling
// dimensions/position).
type Ceiling interface {
	Tilemap()      tilemaps.Tilemap
	Offset()       (int16, int16)
	CeilingType()  string
	CeilingMode()  CeilingMode
	CeilingAlpha() float32
	CeilingData()  interface{}
	CeilingLoad(x, y int16, mode CeilingMode, alpha float32, data map[string]interface{}, registry *resources.Registry) Ceiling
}


// Implements some of the Ceiling interface methods.
type BaseCeiling struct {
	x, y  int16
	mode  CeilingMode
	alpha float32
}


// Gets the base ceiling's offset.
func (baseCeiling BaseCeiling) Offset() (int16, int16) {
	return baseCeiling.x, baseCeiling.y
}


// Gets the base ceiling's mode.
func (baseCeiling BaseCeiling) CeilingMode() CeilingMode {
	return baseCeiling.mode
}


// Gets the base ceiling's alpha.
func (baseCeiling BaseCeiling) CeilingAlpha() float32 {
	return baseCeiling.alpha
}


// Creates a new BaseCeiling struct for other ceilings.
func NewBaseCeilingData(x, y int16, mode CeilingMode, alpha float32) BaseCeiling {
	alpha = values.ClampF32(0, alpha, 1)
	return BaseCeiling{x, y, mode, alpha}
}