package ceilings

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/tilemaps"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
)


// This ceiling has only one tile, and an underlying bitmask.
// Enumerate bitmask position will map to true or false, and
// will imply the the same position (in the tilemap) having
// the "painting" tile or not.
type MonoCeiling struct {
	BaseCeiling
	tilemap *tilemaps.MonoTilemap
}


// Gets the underlying tilemap.
func (monoCeiling *MonoCeiling) Tilemap() tilemaps.Tilemap {
	return monoCeiling.tilemap
}


// Dumps its internal data: The underlying tilemap.
func (monoCeiling *MonoCeiling) CeilingData() interface{} {
	return struct {
		Tilemap interface{}
	} {
		monoCeiling.Tilemap().TilemapData(),
	}
}


// Gets the ceiling type: "mono".
func (monoCeiling *MonoCeiling) CeilingType() string {
	return "mono"
}


// Creates a new mono ceiling instance by loading its data.
func (monoCeiling *MonoCeiling) CeilingLoad(x, y int16, mode CeilingMode, alpha float32, data map[string]interface{}, registry *resources.Registry) Ceiling {
	return &MonoCeiling{
		BaseCeiling{x, y, mode, alpha},
		(&tilemaps.MonoTilemap{}).TilemapLoad(data["Tilemap"].(map[string]interface{}), registry).(*tilemaps.MonoTilemap),
	}
}


// Creates a new Mono Ceiling.
func NewMonoCeiling(x, y int16, width, height uint16, mode CeilingMode, alpha float32, tile *tiles.Tile) *MonoCeiling {
	return &MonoCeiling{
		NewBaseCeilingData(x, y, mode, alpha),
		tilemaps.NewMonoTilemap(width, height, tile),
	}
}


var _ = AddPlugIn(&MonoCeiling{})