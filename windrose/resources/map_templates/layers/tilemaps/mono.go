package tilemaps

import (
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/types/bitmasks"
	"math"
)

// This tilemap has only one tile, and an underlying bitmask.
// Enumerate bitmask position will map to true or false, and will
// imply the the same position (in the tilemap) having the
// "painting" tile or not.
type MonoTilemap struct {
	width, height uint16
	bitmask       *bitmasks.Bitmask
	tile          *tiles.Tile
}

// Gets the tilemap's size.
func (monoTilemap *MonoTilemap) Size() (uint16, uint16) {
	return monoTilemap.bitmask.Width(), monoTilemap.bitmask.Height()
}

// Gets the tilemap's data.
func (monoTilemap *MonoTilemap) TilemapData() interface{} {
	return struct {
		Width   uint16
		Height  uint16
		Tile    string
		Content []uint64
	}{
		monoTilemap.width, monoTilemap.height,
		monoTilemap.tile.URI(),
		monoTilemap.bitmask.Dump(),
	}
}

// Loads a mono tilemap from a data structure.
func (*MonoTilemap) TilemapLoad(data map[string]interface{}, registry *resources.Registry) Tilemap {
	fromMap := from.Map(data)
	width := fromMap.Get("Width").AsUint16()
	height := fromMap.Get("Height").AsUint16()
	tile := registry.Resolve(fromMap.Get("Tile").AsString()).(*tiles.Tile)
	content := from.Slice(fromMap["Content"].([]interface{}))
	bitmapContent := make([]uint64, len(content))
	for index, _ := range content {
		bitmapContent[index] = content.Get(index).AsUint64()
	}
	bitmask := bitmasks.NewBitMask(width, height, false)
	bitmask.Set(bitmapContent)
	return &MonoTilemap{width, height, &bitmask, tile}
}

// Gets the tilemap's tile given a position. The underlying
// check is against a bitmask's cell at that position and, if
// true, then the "painting" tile is returned. Otherwise, nil
// is returned (standing for "no tile in that position").
func (monoTilemap *MonoTilemap) Tile(x, y uint16) *tiles.Tile {
	if monoTilemap.bitmask.Cell(x, y) {
		return monoTilemap.tile
	} else {
		return nil
	}
}

// Returns the underlying bitmask, to edit it directly (which
// will reflect, with no delay, the next tile(s) value(s) when
// iterated further).
func (monoTilemap *MonoTilemap) Bitmask() *bitmasks.Bitmask {
	return monoTilemap.bitmask
}

// Enumerates many of the tiles in this tilemap, which is the
// same as iterating over its underlying bitmask and choosing
// between returning the tile or returning nil, depending on
// the value of the retrieved bit on each step. A range is
// specified for the cells being iterated.
func (monoTilemap *MonoTilemap) EnumerateOnlyTiles(xi, xf, yi, yf uint16, callback func(x, y uint16, tile *tiles.Tile)) {
	monoTilemap.bitmask.Enumerate(xi, xf, yi, yf, func(x, y uint16, bit bool) {
		var tile *tiles.Tile = nil
		if bit {
			tile = monoTilemap.tile
		}
		callback(x, y, tile)
	})
}

// Enumerates all the tiles in this tilemap, which is the same
// as iterating over its underlying bitmask and choosing
// between returning the tile or returning nil, depending on
// the value of the retrieved bit on each step.
func (monoTilemap *MonoTilemap) EnumerateTiles(callback func(x, y uint16, tile *tiles.Tile)) {
	xf := monoTilemap.bitmask.Width() - 1
	yf := monoTilemap.bitmask.Height() - 1
	monoTilemap.EnumerateOnlyTiles(0, xf, 0, yf, callback)
}

// Creates a new Mono TileMap.
func NewMonoTilemap(width, height uint16, tile *tiles.Tile) *MonoTilemap {
	width = values.MinU16(uint16(math.MaxInt16), width)
	height = values.MinU16(uint16(math.MaxInt16), height)
	bitmask := bitmasks.NewBitMask(width, height, false)
	return &MonoTilemap{width, height, &bitmask, tile}
}
