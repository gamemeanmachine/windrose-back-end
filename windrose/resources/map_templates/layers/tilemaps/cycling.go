package tilemaps

import (
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
	"math"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// This tilemap has a limited pattern of tiles, which
// repeats both horizontally and vertically.
type CyclingTilemap struct {
	width, height               uint16
	patternWidth, patternHeight uint16
	patternContent              []*tiles.Tile
	// These fields work like a dump for the cache.
	// They are set to null when a cell is changed,
	// and they are refreshed when they are dumped.
	cachedPatternTiles          []string
	// If any of these cells is < 0, it tells that
	// that cell is empty.
	cachedPatternContent        []int
}


// Gets the tilemap's size.
func (cyclingTilemap *CyclingTilemap) Size() (uint16, uint16) {
	return cyclingTilemap.width, cyclingTilemap.height
}


// Gets the tilemap's content.
func (cyclingTilemap *CyclingTilemap) TilemapData() interface{} {
	if cyclingTilemap.cachedPatternTiles == nil {
		// First, the palette is being collected.
		toCache := map[*tiles.Tile]int{}
		for _, value := range cyclingTilemap.patternContent {
			if value != nil {
				if _, ok := toCache[value]; !ok {
					toCache[value] = len(toCache);
				}
			}
		}
		// Then the cached tiles is allocated to the
		// length of the palette, and filled by their
		// index.
		cyclingTilemap.cachedPatternTiles = make([]string, len(toCache))
		for tile, index := range toCache {
			cyclingTilemap.cachedPatternTiles[index] = tile.URI()
		}
		// Finally, the cells will be cached. If a cell
		// is empty, it will receive index == -1.
		cyclingTilemap.cachedPatternContent = make([]int, len(cyclingTilemap.patternContent))
		for index, value := range cyclingTilemap.patternContent {
			if value == nil {
				cyclingTilemap.cachedPatternContent[index] = -1
			} else {
				cyclingTilemap.cachedPatternContent[index] = toCache[value]
			}
		}
	}

	return struct {
		Width, Height  uint16
		PatternWidth   uint16
		PatternHeight  uint16
		PatternTiles   []string
		PatternContent []int
	} {
		cyclingTilemap.width, cyclingTilemap.height,
		cyclingTilemap.patternWidth,
		cyclingTilemap.patternHeight,
		cyclingTilemap.cachedPatternTiles,
		cyclingTilemap.cachedPatternContent,
	}
}


// Loads a mono tilemap from a data structure.
func (*CyclingTilemap) TilemapLoad(data map[string]interface{}, registry *resources.Registry) Tilemap {
	fromMap := from.Map(data)
	width := fromMap.Get("Width").AsUint16()
	height := fromMap.Get("Height").AsUint16()
	patternWidth := fromMap.Get("PatternWidth").AsUint16()
	patternHeight := fromMap.Get("PatternHeight").AsUint16()
	fromPatternTileURIs := from.Slice(fromMap["PatternTiles"].([]interface{}))
	fromPatternCellIndices := from.Slice(fromMap["PatternContent"].([]interface{}))

	patternTilePalette := make([]*tiles.Tile, len(fromPatternTileURIs))
	for index, _ := range fromPatternTileURIs {
		patternTilePalette[index] = registry.Resolve(fromPatternTileURIs.Get(index).AsString()).(*tiles.Tile)
	}
	patternContent := make([]*tiles.Tile, len(fromPatternCellIndices))
	for index, _ := range fromPatternCellIndices {
		paletteIndex := fromPatternCellIndices.Get(index).AsInt()
		if paletteIndex == -1 {
			patternContent[index] = nil
		} else {
			patternContent[index] = patternTilePalette[paletteIndex]
		}
	}
	width = values.ClampU16(1, width, uint16(math.MaxInt16))
	height = values.ClampU16(1, height, uint16(math.MaxInt16))
	patternWidth = values.ClampU16(1, patternWidth, uint16(math.MaxInt16))
	patternHeight = values.ClampU16(1, patternHeight, uint16(math.MaxInt16))
	return &CyclingTilemap{
		width, height, patternWidth, patternHeight,
		patternContent, nil, nil,
	}
}


// Sets a tile in a specific position in the pattern. That position
// will actually be wrapped and referenced inside the pattern.
func (cyclingTilemap *CyclingTilemap) SetTile(x, y uint16, tile *tiles.Tile) {
	x %= cyclingTilemap.patternWidth
	y %= cyclingTilemap.patternHeight
	cyclingTilemap.patternContent[y * cyclingTilemap.patternWidth + x] = tile
	cyclingTilemap.cachedPatternTiles = nil
	cyclingTilemap.cachedPatternContent = nil
}


// Gets a tile at a given position (it will
// always be the same tile).
func (cyclingTilemap *CyclingTilemap) Tile(x, y uint16) *tiles.Tile {
	x %= cyclingTilemap.patternWidth
	y %= cyclingTilemap.patternHeight
	return cyclingTilemap.patternContent[y * cyclingTilemap.patternWidth + x]
}


// Enumerates all the tiles in a rectangle (all the bounds are included).
// The tiles will repeat horizontally and vertically from the base pattern.
func (cyclingTilemap *CyclingTilemap) EnumerateOnlyTiles(xi, xf, yi, yf uint16, callback func(x, y uint16, tile *tiles.Tile)) {
	xi = values.MinU16(xi, cyclingTilemap.width - 1)
	xf = values.MinU16(xf, cyclingTilemap.width - 1)
	yi = values.MinU16(yi, cyclingTilemap.height - 1)
	yf = values.MinU16(yf, cyclingTilemap.height - 1)
	xi_ := values.MinU16(xi, xf)
	xf_ := values.MaxU16(xi, xf)
	yi_ := values.MinU16(yi, yf)
	yf_ := values.MaxU16(yi, yf)

	patternY := yi_ % cyclingTilemap.patternHeight
	for y := yi_; y <= yf_; y++ {
		basePatternIndex := int(patternY * cyclingTilemap.patternWidth)
		patternX := xi_ % cyclingTilemap.patternWidth
		for x := xi_; x <= xf_; x++ {
			callback(x, y, cyclingTilemap.patternContent[basePatternIndex + int(patternX)])
			patternX += 1
			if patternX == cyclingTilemap.patternWidth {
				patternX = 0
			}
		}
		patternY += 1
		if patternY == cyclingTilemap.patternHeight {
			patternY = 0
		}
	}
}


// Enumerates all the tiles in the whole tilemap. The tiles will
// repeat horizontally and vertically from the base pattern.
func (cyclingTilemap *CyclingTilemap) EnumerateTiles(callback func(x, y uint16, tile *tiles.Tile)) {
	xf := cyclingTilemap.width - 1
	yf := cyclingTilemap.height - 1
	cyclingTilemap.EnumerateOnlyTiles(0, xf, 0, yf, callback)
}


// Creates a new Cycling FloorDump.
func NewCyclingTilemap(width, height, patternWidth, patternHeight uint16) *CyclingTilemap {
	width = values.ClampU16(1, width, uint16(math.MaxInt16))
	height = values.ClampU16(1, height, uint16(math.MaxInt16))
	patternWidth = values.MinU16(uint16(math.MaxInt16), patternWidth)
	patternHeight = values.MinU16(uint16(math.MaxInt16), patternHeight)
	patternTiles := make([]*tiles.Tile, patternWidth * patternHeight)
	return &CyclingTilemap{
		width, height,
		patternWidth, patternHeight,
		patternTiles, nil, nil,
	}
}

