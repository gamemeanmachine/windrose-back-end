package tilemaps

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"math"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// This tilemap has only one tile, and fills
// all its space with it.
type FillTilemap struct {
	width, height uint16
	tile          *tiles.Tile
}


// Gets the tilemap's size.
func (fillTilemap *FillTilemap) Size() (uint16, uint16) {
	return fillTilemap.width, fillTilemap.height
}


// Gets the tilemap's content.
func (fillTilemap *FillTilemap) TilemapData() interface{} {
	return struct {
		Width    uint16
		Height   uint16
		FillTile string
	} {
		fillTilemap.width, fillTilemap.height,
		fillTilemap.tile.URI(),
	}
}


// Loads a fill tilemap from a data structure.
func (*FillTilemap) TilemapLoad(data map[string]interface{}, registry *resources.Registry) Tilemap {
	fromMap := from.Map(data)
	width := fromMap.Get("Width").AsUint16()
	height := fromMap.Get("Height").AsUint16()
	fillTile := registry.Resolve(fromMap.Get("FillTile").AsString()).(*tiles.Tile)
	return NewFillTilemap(width, height, fillTile)
}


// Fills this tilemap with a different tile.
func (fillTilemap *FillTilemap) Fill(tile *tiles.Tile) {
	fillTilemap.tile = tile
}


// Gets a tile at a given position (it will
// always be the same tile).
func (fillTilemap *FillTilemap) Tile(x, y uint16) *tiles.Tile {
	return fillTilemap.tile
}


// Enumerates all the tiles in a rectangle (all the bounds are included).
// The tile will always be the same.
func (fillTilemap *FillTilemap) EnumerateOnlyTiles(xi, xf, yi, yf uint16, callback func(x, y uint16, tile *tiles.Tile)) {
	xi = values.MinU16(xi, fillTilemap.width - 1)
	xf = values.MinU16(xf, fillTilemap.width - 1)
	yi = values.MinU16(yi, fillTilemap.height - 1)
	yf = values.MinU16(yf, fillTilemap.height - 1)
	xi_ := values.MinU16(xi, xf)
	xf_ := values.MaxU16(xi, xf)
	yi_ := values.MinU16(yi, yf)
	yf_ := values.MaxU16(yi, yf)
	for y := yi_; y <= yf_; y++ {
		for x := xi_; x <= xf_; x++ {
			callback(x, y, fillTilemap.tile)
		}
	}
}


// Enumerates all the tiles. The tile will always be the same.
func (fillTilemap *FillTilemap) EnumerateTiles(callback func(x, y uint16, tile *tiles.Tile)) {
	xf := fillTilemap.width - 1
	yf := fillTilemap.height - 1
	fillTilemap.EnumerateOnlyTiles(0, xf, 0, yf, callback)
}


// Creates a new Fill Tilemap.
func NewFillTilemap(width, height uint16, tile *tiles.Tile) *FillTilemap {
	width = values.ClampU16(1, width, uint16(math.MaxInt16))
	height = values.ClampU16(1, height, uint16(math.MaxInt16))
	return &FillTilemap{width, height, tile}
}
