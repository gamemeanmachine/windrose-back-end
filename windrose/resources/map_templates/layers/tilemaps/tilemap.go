package tilemaps

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
)


// Tilemap-based objects involve, directly or not,
// a tilemap that will be filled somehow in the game
// front end. They can have a variety of usages but
// they will mainly serve as floors and/or ceilings.
type Tilemap interface {
	// Gets the tilemap size.
	Size()            (uint16, uint16)
	// Dumps the tilemap content, which will be an arbitrary
	// structure for each sub-type.
	TilemapData()     interface{}
	// Loads a tilemap from a data structure.
	TilemapLoad(data map[string]interface{}, registry *resources.Registry) Tilemap
	// Gets a tile at certain position. Returns nil if the
	// position is out of bounds.
	Tile(x, y uint16) *tiles.Tile
	// Iterates over the tiles in a specific rectangle,
	// bounded by the tilemap's size.
	EnumerateOnlyTiles(xi, xf, yi, yf uint16, callback func(x, y uint16, tile *tiles.Tile))
	// Iterates over all the tiles in the tilemap.
	EnumerateTiles(callback func(x, y uint16, tile *tiles.Tile))
}