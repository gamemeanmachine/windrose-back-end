package tilemaps

import (
	"math"
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
)


// This tilemap has a regular array of potentially
// different tiles.
type GridTilemap struct {
	width, height uint16
	content       []*tiles.Tile
	// These fields work like a dump for the cache.
	// They are set to null when a cell is changed,
	// and they are refreshed when they are dumped.
	cachedTiles   []string
	// If any of these cells is < 0, it tells that
	// that cell is empty.
	cachedContent []int
}


// Gets the tilemap height.
func (gridTilemap *GridTilemap) Size() (uint16, uint16) {
	return gridTilemap.width, gridTilemap.height
}


// Gets the tilemap data. The returned elements will
// be, each, either an ID or nil. The first time, after
// the last edit time, the cach will be pre-generated,
// and further calls will use that cache, without making
// it again until next edit and dump.
func (gridTilemap *GridTilemap) TilemapData() interface{} {
	if gridTilemap.cachedTiles == nil {
		// First, the palette is being collected.
		toCache := map[*tiles.Tile]int{}
		for _, value := range gridTilemap.content {
			if value != nil {
				if _, ok := toCache[value]; !ok {
					toCache[value] = len(toCache)
				}
			}
		}
		// Then the cached tiles is allocated to the
		// length of the palette, and filled by their
		// index.
		gridTilemap.cachedTiles = make([]string, len(toCache))
		for tile, index := range toCache {
			gridTilemap.cachedTiles[index] = tile.URI()
		}
		// Finally, the cells will be cached. If a cell
		// is empty, it will receive index == -1.
		gridTilemap.cachedContent = make([]int, len(gridTilemap.content))
		for index, value := range gridTilemap.content {
			if value == nil {
				gridTilemap.cachedContent[index] = -1
			} else {
				gridTilemap.cachedContent[index] = toCache[value]
			}
		}
	}

	return struct {
		Width   uint16
		Height  uint16
		Tiles   []string
		Content []int
	} {
		gridTilemap.width, gridTilemap.height,
		gridTilemap.cachedTiles,
		gridTilemap.cachedContent,
	}
}


// Loads a mono tilemap from a data structure.
func (*GridTilemap) TilemapLoad(data map[string]interface{}, registry *resources.Registry) Tilemap {
	fromMap := from.Map(data)
	width := fromMap.Get("Width").AsUint16()
	height := fromMap.Get("Height").AsUint16()
	fromTileURIs := from.Slice(fromMap["Tiles"].([]interface{}))
	fromCellIndices := from.Slice(fromMap["Content"].([]interface{}))

	tilePalette := make([]*tiles.Tile, len(fromTileURIs))
	for index, _ := range fromTileURIs {
		tilePalette[index] = registry.Resolve(fromTileURIs.Get(index).AsString()).(*tiles.Tile)
	}
	content := make([]*tiles.Tile, len(fromCellIndices))
	for index, _ := range fromCellIndices {
		paletteIndex := fromCellIndices.Get(index).AsInt()
		if paletteIndex == -1 {
			content[index] = nil
		} else {
			content[index] = tilePalette[paletteIndex]
		}
	}
	width = values.ClampU16(1, width, uint16(math.MaxInt16))
	height = values.ClampU16(1, height, uint16(math.MaxInt16))
	return &GridTilemap{
		width, height,
		content, nil, nil,
	}
}


// Sets a tile at a different position.
func (gridTilemap *GridTilemap) SetTile(x, y uint16, tile *tiles.Tile) {
	x = values.MinU16(x, gridTilemap.width - 1)
	y = values.MinU16(y, gridTilemap.height - 1)
	var position = int(gridTilemap.width) * int(y) + int(x)
	gridTilemap.content[position] = tile
	gridTilemap.cachedTiles = nil
	gridTilemap.cachedContent = nil
}


// Gets a tile at a given position.
func (gridTilemap *GridTilemap) Tile(x, y uint16) *tiles.Tile {
	x = values.MinU16(x, gridTilemap.width - 1)
	y = values.MinU16(y, gridTilemap.height - 1)
	var position = int(gridTilemap.width) * int(y) + int(x)
	return gridTilemap.content[position]
}


// Enumerates all the tiles in a rectangle (all the bounds are included).
func (gridTilemap *GridTilemap) EnumerateOnlyTiles(xi, xf, yi, yf uint16, callback func(x, y uint16, tile *tiles.Tile)) {
	xi = values.MinU16(xi, gridTilemap.width - 1)
	xf = values.MinU16(xf, gridTilemap.width - 1)
	yi = values.MinU16(yi, gridTilemap.height - 1)
	yf = values.MinU16(yf, gridTilemap.height - 1)
	xi_ := values.MinU16(xi, xf)
	xf_ := values.MaxU16(xi, xf)
	yi_ := values.MinU16(yi, yf)
	yf_ := values.MaxU16(yi, yf)
	for y := yi_; y <= yf_; y++ {
		currentIndex := int(y * gridTilemap.width + xi_)
		for x := xi_; x <= xf_; x++ {
			callback(x, y, gridTilemap.content[currentIndex])
			currentIndex++
		}
	}
}


// Enumerates all the tiles.
func (gridTilemap *GridTilemap) EnumerateTiles(callback func(x, y uint16, tile *tiles.Tile)) {
	xf := gridTilemap.width - 1
	yf := gridTilemap.height - 1
	gridTilemap.EnumerateOnlyTiles(0, xf, 0, yf, callback)
}


// Creates a new Grid FloorDump.
func NewGridTilemap(width, height uint16) *GridTilemap {
	width = values.ClampU16(1, width, uint16(math.MaxInt16))
	height = values.ClampU16(1, height, uint16(math.MaxInt16))
	size := width * height
	return &GridTilemap{
		width, height,
		make([]*tiles.Tile, size),
		nil, nil,
	}
}
