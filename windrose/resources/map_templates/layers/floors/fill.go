package floors

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/tilemaps"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// This floor has only one tile, and fills its whole
// space with it.
type FillFloor struct {
	tilemap *tilemaps.FillTilemap
}


// Gets the floor type.
func (fillFloor *FillFloor) FloorType() string {
	return "fill"
}


// Return the inner tilemap.
func (fillFloor *FillFloor) Tilemap() tilemaps.Tilemap {
	return fillFloor.tilemap
}


// Dumps the data of this floor, by dumping the underlying tilemap.
func (fillFloor *FillFloor) FloorData() interface{} {
	return map[string]interface{}{
		"Tilemap": fillFloor.tilemap.TilemapData(),
	}
}


// Creates a new floor instance by loading its data.
func (fillFloor *FillFloor) FloorLoad(data map[string]interface{}, registry *resources.Registry) Floor {
	return &FillFloor{
		tilemap: (&tilemaps.FillTilemap{}).TilemapLoad(data["Tilemap"].(map[string]interface{}), registry).(*tilemaps.FillTilemap),
	}
}


// Creates a new Fill Floor.
func NewFillFloor(width, height uint16, tile *tiles.Tile) *FillFloor {
	return &FillFloor{tilemaps.NewFillTilemap(width, height, tile)}
}


// Installing the plugin floor.
var _ = AddPlugIn(&FillFloor{})