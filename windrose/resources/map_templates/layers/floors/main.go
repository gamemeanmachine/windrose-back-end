package floors

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// This layer template provides information regarding the
// tilemaps that make the floors in a map.
type FloorsTemplateLayer struct {
	layers.BaseMapTemplateLayer
	floors []Floor
}


// The map template layer type is "floors" for this type.
func (*FloorsTemplateLayer) MapTemplateLayerType() string {
	return "floors"
}


// Adds another floor, on top of the existing ones.
func (floorsTemplateLayer *FloorsTemplateLayer) AddFloor(floor Floor) {
	if floor == nil {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.floors.FloorsTemplateLayer::AddFloor",
			"floor", "Must not be nil",
		))
	} else {
		floorsTemplateLayer.floors = append(floorsTemplateLayer.floors, floor)
	}
}


// Dumps the data of all its floors as (FloorType / FloorData).
func (floorsTemplateLayer *FloorsTemplateLayer) MapTemplateLayerData() interface{} {
	data := make([]interface{}, len(floorsTemplateLayer.floors))
	for index, floor := range floorsTemplateLayer.floors {
		data[index] = struct {
			Type string
			Data interface{}
		} {
			floor.FloorType(),
			floor.FloorData(),
		}
	}
	return struct {
		Floors []interface{}
	}{
		data,
	}
}


// Loads a new instance, by loading all the registered floors.
func (*FloorsTemplateLayer) MapTemplateLayerLoad(width, height uint16, data map[string]interface{}, registry *resources.Registry) layers.MapTemplateLayer {
	floorsTemplateLayer := NewFloorsTemplateLayer(width, height)
	floorsData := data["Floors"].([]interface{})
	floorsTemplateLayer.floors = make([]Floor, len(floorsData))
	for index, value := range floorsData {
		value := from.Map(value.(map[string]interface{}))
		floorsTemplateLayer.floors[index] = plugins[value.Get("Type").AsString()].FloorLoad(value["Data"].(map[string]interface{}), registry)
	}
	return floorsTemplateLayer
}


// Creates a new instance, by specifying just its dimensions.
func NewFloorsTemplateLayer(width, height uint16) *FloorsTemplateLayer {
	return &FloorsTemplateLayer{
		layers.NewBaseMapTemplateLayer(width, height),
		[]Floor{},
	}
}


// Registers this MapTemplateLayer.
var _ = layers.AddPlugIn(&FloorsTemplateLayer{})