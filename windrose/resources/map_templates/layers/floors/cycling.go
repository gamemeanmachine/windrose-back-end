package floors

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/tilemaps"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// This floor has a limited pattern of tiles, which
// repeats both horizontally and vertically.
type CyclingFloor struct {
	tilemap *tilemaps.CyclingTilemap
}


// Grid floors are of type "cycling".
func (cyclingFloor *CyclingFloor) FloorType() string {
	return "cycling"
}


// Return the inner tilemap.
func (cyclingFloor *CyclingFloor) Tilemap() tilemaps.Tilemap {
	return cyclingFloor.tilemap
}


// Dumps the data of this floor, by dumping the underlying tilemap.
func (cyclingFloor *CyclingFloor) FloorData() interface{} {
	return map[string]interface{}{
		"Tilemap": cyclingFloor.tilemap.TilemapData(),
	}
}


// Creates a new floor instance by loading its data.
func (cyclingFloor *CyclingFloor) FloorLoad(data map[string]interface{}, registry *resources.Registry) Floor {
	return &CyclingFloor{
		tilemap: (&tilemaps.CyclingTilemap{}).TilemapLoad(data["Tilemap"].(map[string]interface{}), registry).(*tilemaps.CyclingTilemap),
	}
}


// Creates a new Cycling Floor.
func NewCyclingFloor(width, height, patternWidth, patternHeight uint16) *CyclingFloor {
	return &CyclingFloor{tilemaps.NewCyclingTilemap(width, height, patternWidth, patternHeight)}
}


// Installing the plugin floor.
var _ = AddPlugIn(&CyclingFloor{})