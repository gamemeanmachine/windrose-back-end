package floors

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/tilemaps"
)


// A floor is actually a tilemap. It will add its own
// type, and will have (width x height) tiles.
type Floor interface {
	Tilemap() tilemaps.Tilemap
	FloorType() string
	FloorData() interface{}
	FloorLoad(data map[string]interface{}, registry *resources.Registry) Floor
}


/////////////////////////////////////////////////////////
// Plug-in system for map template layers.
/////////////////////////////////////////////////////////


var plugins = map[string]Floor{}


// Register an EntityVisualContent-implementing type.
func AddPlugIn(content Floor) bool {
	if content == nil {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.floors.AddPlugIn",
			"content", "Must not be nil",
		))
	} else if registered, ok := plugins[content.FloorType()]; ok && registered != content {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.floors.AddPlugIn",
			"content", "Another plugin of the same type is already registered",
		))
	} else if ok {
		return false
	} else {
		plugins[content.FloorType()] = content
		return true
	}
}

