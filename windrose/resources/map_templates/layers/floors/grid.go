package floors

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/tilemaps"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// Grid floors specify their tiles directly.
type GridFloor struct {
	tilemap *tilemaps.GridTilemap
}


// Grid floors are of type "grid".
func (gridFloor *GridFloor) FloorType() string {
	return "grid"
}


// Return the inner tilemap.
func (gridFloor *GridFloor) Tilemap() tilemaps.Tilemap {
	return gridFloor.tilemap
}


// Dumps the data of this floor, by dumping the underlying tilemap.
func (gridFloor *GridFloor) FloorData() interface{} {
	return map[string]interface{}{
		"Tilemap": gridFloor.tilemap.TilemapData(),
	}
}


// Creates a new floor instance by loading its data.
func (gridFloor *GridFloor) FloorLoad(data map[string]interface{}, registry *resources.Registry) Floor {
	return &GridFloor{
		tilemap: (&tilemaps.GridTilemap{}).TilemapLoad(data["Tilemap"].(map[string]interface{}), registry).(*tilemaps.GridTilemap),
	}
}


// Creates a new Grid Floor.
func NewGridFloor(width, height uint16) *GridFloor {
	return &GridFloor{tilemaps.NewGridTilemap(width, height)}
}


// Installing the plugin floor.
var _ = AddPlugIn(&GridFloor{})