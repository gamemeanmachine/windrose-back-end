package floors

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/tilemaps"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// This floor has only one tile, and an underlying bitmask.
// Enumerate bitmask position will map to true or false, and will
// imply the the same position (in the tilemap) having the
// "painting" tile or not.
type MonoFloor struct {
	tilemap *tilemaps.MonoTilemap
}


// Grid floors are of type "mono".
func (monoFloor *MonoFloor) FloorType() string {
	return "mono"
}


// Return the inner tilemap.
func (monoFloor *MonoFloor) Tilemap() tilemaps.Tilemap {
	return monoFloor.tilemap
}


// Dumps the data of this floor, by dumping the underlying tilemap.
func (monoFloor *MonoFloor) FloorData() interface{} {
	return map[string]interface{}{
		"Tilemap": monoFloor.tilemap.TilemapData(),
	}
}


// Creates a new floor instance by loading its data.
func (monoFloor *MonoFloor) FloorLoad(data map[string]interface{}, registry *resources.Registry) Floor {
	return &MonoFloor{
		tilemap: (&tilemaps.MonoTilemap{}).TilemapLoad(data["Tilemap"].(map[string]interface{}), registry).(*tilemaps.MonoTilemap),
	}
}


// Creates a new Mono Floor.
func NewMonoFloor(width, height uint16, tile *tiles.Tile) *MonoFloor {
	return &MonoFloor{tilemaps.NewMonoTilemap(width, height, tile)}
}


// Installing the plugin floor.
var _ = AddPlugIn(&MonoFloor{})