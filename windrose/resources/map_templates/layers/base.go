package layers

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// Map template layers will inform their type and will
// also be capable of providing / iterating over their
// contents. They will not be considered in the order
// they were added (this applies specially to both the
// floor and the ceiling layers), for every layer has
// a front-end counterpart that must match against and
// each type will appear once among the layers, and
// will how to sort with respect to the others.
// The contents they will provide, if they do, is
// arbitrary (each layer will know, in front-end, how
// to interpret the data provided by the map template
// layer).
// Map layers are purely representative: they will
// contain represented behaviour. This is a different
// case than with strategies: strategies may -and
// often will- be secret (at least in part) but map
// template layers are purely representational. E.g.:
// - Floor layers will display tiles. The tile data
//   (which is just tile references / ids) will be
//   available to the front-end to fully represent
//   the map, visually. This layer will always
//   be added.
// - Ceiling layers will display tiles but on top,
//   and grouped into ceilings. Ceilings will have
//   data that can be mirrored in the front-end with
//   respect to the ceilings (besides, obviously, the
//   tiles on each ceiling). This layer exists, but
//   may be not added to the map.
// - Darkness layers (if they exist) should only
//   need to provide the color of the mask (usually
//   black and, in few cases, brown or dark-reddish).
// - Drop layers (in the ported "BackPack" package)
//   only tell they are present.
// - Objects layer (will always be added) only tell they
//   are present.
// - Visuals layer (will always be added) only tell they
//   are present.
// Again: Secret behaviours will exist in the strategies,
// not in the layers. Layers are purely representational
// and, with respect to logic / state keeping, only the
// objects layer and their strategies will matter. In
// the worst case, "live" layers will hold the data that
// may interact with some sort of secret behaviour, and
// external objects or data structures may hold the actual
// secret behaviours (e.g. object strategies, inventory /
// drop strategies, ...).
type MapTemplateLayer interface {
	MapTemplateLayerSize() (uint16, uint16)
	MapTemplateLayerType() string
	MapTemplateLayerData() interface{}
	MapTemplateLayerLoad(width, height uint16, data map[string]interface{}, registry *resources.Registry) MapTemplateLayer
}


// Base map template layers only know their size.
type BaseMapTemplateLayer struct {
	width, height uint16
}


// Returns the size of the map layer.
func (baseMapTemplateLayer *BaseMapTemplateLayer) MapTemplateLayerSize() (uint16, uint16) {
	return baseMapTemplateLayer.width, baseMapTemplateLayer.height
}


// Creates a new base for the template layers.
func NewBaseMapTemplateLayer(width, height uint16) BaseMapTemplateLayer {
	return BaseMapTemplateLayer{width, height}
}


/////////////////////////////////////////////////////////
// Plug-in system for map template layers.
/////////////////////////////////////////////////////////


var plugins = map[string]MapTemplateLayer{}


// Register an EntityVisualContent-implementing type.
func AddPlugIn(content MapTemplateLayer) bool {
	if content == nil {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.AddPlugIn",
			"content", "Must not be nil",
		))
	} else if registered, ok := plugins[content.MapTemplateLayerType()]; ok && registered != content {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.AddPlugIn",
			"content", "Another plugin of the same type is already registered",
		))
	} else if ok {
		return false
	} else {
		plugins[content.MapTemplateLayerType()] = content
		return true
	}
}


// Gets the registered plugin by some key.
func GetPlugIn(key string) MapTemplateLayer {
	return plugins[key]
}