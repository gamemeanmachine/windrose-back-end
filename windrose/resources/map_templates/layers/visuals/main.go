package visuals

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers"
)


// This layer template is just presence-informative.
// It does not add any owned/template data by itself.
// It is always added to a map.
type VisualsTemplateLayer struct {
	layers.BaseMapTemplateLayer
}


// The map template layer type is "entities" for this type.
func (*VisualsTemplateLayer) MapTemplateLayerType() string {
	return "entities"
}


// This map template layer does not provide any particular data.
func (*VisualsTemplateLayer) MapTemplateLayerData() interface{} {
	return nil
}


// Loads a new instance, without actually loading anything.
func (*VisualsTemplateLayer) MapTemplateLayerLoad(width, height uint16, data map[string]interface{}, registry *resources.Registry) layers.MapTemplateLayer {
	return NewVisualsTemplateLayer(width, height)
}


// Creates a visuals template layer, by specifying just its dimensions.
func NewVisualsTemplateLayer(width, height uint16) *VisualsTemplateLayer {
	return &VisualsTemplateLayer{layers.NewBaseMapTemplateLayer(width, height)}
}


// Registers this MapTemplateLayer.
var _ = layers.AddPlugIn(&VisualsTemplateLayer{})