package errors

import (
	"fmt"
	"gitlab.com/gamemeanmachine/golang-support/errors"
)

// Raised when trying to inflate an entities
// layer with strategies not meeting their
// dependencies
type StrategyDependencyUnmetError struct {
	errors.GMMError
	dependent  string
	dependency string
}

// Returns the key of the dependent strategy.
func (strategyDependencyUnmet StrategyDependencyUnmetError) Dependent() string {
	return strategyDependencyUnmet.dependent
}

// Returns the key of the dependency strategy.
func (strategyDependencyUnmet StrategyDependencyUnmetError) Dependency() string {
	return strategyDependencyUnmet.dependency
}

// Returns the error description including the type.
func (strategyDependencyUnmet StrategyDependencyUnmetError) Error() string {
	return fmt.Sprintf(
		"entity management strategy template with key '%s' does not meet dependency '%s'",
		strategyDependencyUnmet.dependent, strategyDependencyUnmet.dependency,
	)
}

// Creates an Unknown Entity Visual Content Type error.
func StrategyDependencyUnmet(dependent, dependency string) StrategyDependencyUnmetError {
	return StrategyDependencyUnmetError{
		errors.GMM("unknown-visual-entity-content-type", ""),
		dependent, dependency,
	}
}
