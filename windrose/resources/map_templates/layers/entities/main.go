package entities

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/entities/errors"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates/layers/entities/strategies"
)

// This layer template serves as a placeholder for the
// strategies to be used in the maps, so the strategy
// template(s) is the only data that this layer will
// manage.
type EntitiesTemplateLayer struct {
	layers.BaseMapTemplateLayer
	strategies map[string]strategies.EntityManagementStrategyTemplate
}

// The map template layer type is "entities" for this type.
func (*EntitiesTemplateLayer) MapTemplateLayerType() string {
	return "entities"
}

// This map template layer does not provide any particular data.
func (entitiesLayer *EntitiesTemplateLayer) MapTemplateLayerData() interface{} {
	strategiesMap := make(map[string]interface{})

	for key, value := range entitiesLayer.strategies {
		strategiesMap[key] = value.EntityManagementStrategyData()
	}
	return struct {
		Strategies map[string]interface{}
	}{
		strategiesMap,
	}
}

// Loads a new instance, without actually loading anything.
func (*EntitiesTemplateLayer) MapTemplateLayerLoad(width, height uint16, data map[string]interface{}, registry *resources.Registry) layers.MapTemplateLayer {
	layer := NewEntitiesTemplateLayer(width, height)
	strategiesMap := data["Strategies"].(map[string]interface{})
	for key, strategyData := range strategiesMap {
		loaded := strategies.GetPlugIn(key).EntityManagementStrategyLoad(strategyData.(map[string]interface{}), registry)
		dependencies := loaded.EntityManagementStrategyDependencies()
		for _, dependency := range dependencies {
			if _, ok := strategiesMap[dependency]; !ok {
				panic(errors.StrategyDependencyUnmet(key, dependency))
			}
		}
		layer.strategies[key] = loaded
	}
	return layer
}

// Creates an entities template layer, by specifying just its dimensions.
func NewEntitiesTemplateLayer(width, height uint16) *EntitiesTemplateLayer {
	return &EntitiesTemplateLayer{
		layers.NewBaseMapTemplateLayer(width, height),
		make(map[string]strategies.EntityManagementStrategyTemplate),
	}
}

// Registers this MapTemplateLayer.
var _ = layers.AddPlugIn(&EntitiesTemplateLayer{})
