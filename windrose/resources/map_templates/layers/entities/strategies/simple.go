package strategies

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// This layout strategy template tells that objects in
// the map should act according to a solidness specification
// (in each object) to determine whether objects can traverse
// other objects or not. It also tells how the movement
// cancel (if any) should be performed: either a safe one,
// or an optimistic one.
type SimpleEntitiesManagementStrategyTemplate struct {}


// This entities management strategy has both the solidness
// and the layout strategies as dependencies.
func (*SimpleEntitiesManagementStrategyTemplate) EntityManagementStrategyDependencies() []string {
	return []string{"layout", "solidness"}
}


// This entities management strategy has key: "simple".
func (*SimpleEntitiesManagementStrategyTemplate) EntityManagementStrategyType() string {
	return "simple"
}


// This entities management strategy does not have any
// particular data.
func (strategyTemplate *SimpleEntitiesManagementStrategyTemplate) EntityManagementStrategyData() interface{} {
	return nil
}


// Loading a base management strategy requires no data.
func (*SimpleEntitiesManagementStrategyTemplate) EntityManagementStrategyLoad(data map[string]interface{}, registry *resources.Registry) EntityManagementStrategyTemplate {
	return &SimpleEntitiesManagementStrategyTemplate{}
}


var _ = AddPlugIn(&LayoutEntitiesManagementStrategyTemplate{})


