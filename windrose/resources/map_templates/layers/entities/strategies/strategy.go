package strategies

import "gitlab.com/gamemeanmachine/resource-servers/v2"

// Entity layers will have the notion of entity
// management strategy templates, which are the
// building blocks of entity management strategies.
// They have their custom data, and also a "type key"
// which serves for the "live" layer to match between
// a "live" entity management strategy and one of these
// "template" entity management strategies. Entity
// management strategies have public data - they do
// not keep the internal state logic (although state
// logic, which is necessarily per-game custom, will
// be the responsible of updating these management
// strategies). One entity layer can only have one
// entity management strategy template per given
// type key, and also these strategies will know their
// dependencies.
type EntityManagementStrategyTemplate interface {
	EntityManagementStrategyDependencies() []string
	EntityManagementStrategyType()         string
	EntityManagementStrategyData()         interface{}
	EntityManagementStrategyLoad(data map[string]interface{}, registry *resources.Registry) EntityManagementStrategyTemplate
}
