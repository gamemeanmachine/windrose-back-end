package strategies

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// This layout strategy template tells that objects in
// the map should act according to a solidness specification
// (in each object) to determine whether objects can traverse
// other objects or not. It also tells how the movement
// cancel (if any) should be performed: either a safe one,
// or an optimistic one.
type SolidnessEntitiesManagementStrategyTemplate struct {
	stepType string
}


// This entities management strategy has only the "base"
// strategy as dependency.
func (*SolidnessEntitiesManagementStrategyTemplate) EntityManagementStrategyDependencies() []string {
    return []string{"base"}
}


// This entities management strategy has key: "solidness".
func (*SolidnessEntitiesManagementStrategyTemplate) EntityManagementStrategyType() string {
	return "solidness"
}


// This entities management strategy only exports the step
// type setting.
func (strategyTemplate *SolidnessEntitiesManagementStrategyTemplate) EntityManagementStrategyData() interface{} {
	return struct {
		StepType string
	} {
		strategyTemplate.stepType,
	}
}


// Loading a base management strategy requires the step type
// field as data.
func (*SolidnessEntitiesManagementStrategyTemplate) EntityManagementStrategyLoad(data map[string]interface{}, registry *resources.Registry) EntityManagementStrategyTemplate {
	fromMap := from.Map(data)
	return &SolidnessEntitiesManagementStrategyTemplate{
		stepType: fromMap.Get("StepType").AsString(),
	}
}


var _ = AddPlugIn(&LayoutEntitiesManagementStrategyTemplate{})
