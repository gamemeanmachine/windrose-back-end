package strategies

import "gitlab.com/gamemeanmachine/resource-servers/v2"


// This layout strategy template has no particular data
// and the only thing it observes is that the objects
// are forbidden to move when a blocking tile appears.
type LayoutEntitiesManagementStrategyTemplate struct {}


// This entities management strategy has only the "base"
// strategy as dependency.
func (*LayoutEntitiesManagementStrategyTemplate) EntityManagementStrategyDependencies() []string {
	return []string{"base"}
}


// This entities management strategy has key: "layout".
func (*LayoutEntitiesManagementStrategyTemplate) EntityManagementStrategyType() string {
	return "layout"
}


// This entities management strategy does not export data.
func (*LayoutEntitiesManagementStrategyTemplate) EntityManagementStrategyData() interface{} {
	return nil
}


// Loading a base management strategy requires no data.
func (*LayoutEntitiesManagementStrategyTemplate) EntityManagementStrategyLoad(data map[string]interface{}, registry *resources.Registry) EntityManagementStrategyTemplate {
	return &LayoutEntitiesManagementStrategyTemplate{}
}


var _ = AddPlugIn(&LayoutEntitiesManagementStrategyTemplate{})
