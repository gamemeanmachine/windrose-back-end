package strategies

import "gitlab.com/gamemeanmachine/resource-servers/v2"


// This base strategy template has no particular data
// and the only thing it observes is that the objects
// stick into the map's dimensions when moving (this
// accounts for object's (x, y) and (width, height)
// rectangle to not have any vertices outside the map's
// boundaries - at least disallowing further movement
// in the "offending" direction.
type BaseEntitiesManagementStrategyTemplate struct {}


// This entities management strategy does not have any
// dependencies.
func (*BaseEntitiesManagementStrategyTemplate) EntityManagementStrategyDependencies() []string {
	return nil
}


// This entities management strategy has key: "base".
func (*BaseEntitiesManagementStrategyTemplate) EntityManagementStrategyType() string {
	return "base"
}


// This entities management strategy does not export data.
func (*BaseEntitiesManagementStrategyTemplate) EntityManagementStrategyData() interface{} {
	return nil
}


// Loading a base management strategy requires no data.
func (*BaseEntitiesManagementStrategyTemplate) EntityManagementStrategyLoad(data map[string]interface{}, registry *resources.Registry) EntityManagementStrategyTemplate {
	return &BaseEntitiesManagementStrategyTemplate{}
}


var _ = AddPlugIn(&BaseEntitiesManagementStrategyTemplate{})
