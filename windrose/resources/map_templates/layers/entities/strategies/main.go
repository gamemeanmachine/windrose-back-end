package strategies

import "gitlab.com/gamemeanmachine/golang-support/errors"

/////////////////////////////////////////////////////////
// Plug-in system for object management strategies.
/////////////////////////////////////////////////////////

var plugins = map[string]EntityManagementStrategyTemplate{}

// Register a EntityManagementStrategyTemplate-implementing type.
func AddPlugIn(content EntityManagementStrategyTemplate) bool {
	if content == nil {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.entities.strategies.AddPlugIn",
			"content", "Must not be nil",
		))
	} else if registered, ok := plugins[content.EntityManagementStrategyType()]; ok && registered != content {
		panic(errors.Argument(
			"windrose.resources.map_templates.layers.entities.strategies.AddPlugIn",
			"content", "Another plugin of the same type is already registered",
		))
	} else if ok {
		return false
	} else {
		plugins[content.EntityManagementStrategyType()] = content
		return true
	}
}

// Gets a plugin.
func GetPlugIn(key string) EntityManagementStrategyTemplate {
	return plugins[key]
}
