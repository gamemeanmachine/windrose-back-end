package map_templates

import "gitlab.com/gamemeanmachine/resource-servers/v2"


// Makes a space for all the resource objects defined in this
// package: map templates (with their layers being embedded).
func MakeMapTemplatesSpace() *resources.Space {
	space := resources.NewSpace("map-templates", "World Map Templates (all the involved objects)")
	space.Register(resources.NewList(
		"map-templates",
		"World Map Templates",
		&MapTemplate{},
	))
	return space
}
