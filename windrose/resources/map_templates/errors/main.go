package errors

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
)


// Raised when trying to inflate a map layer
// but the sub-type is not registered as a plug-in.
type UnknownMapLayerContentTypeError struct {
	errors.GMMError
	contentType string
}


// Returns the content type.
func (unknownMapLayerContentTypeError UnknownMapLayerContentTypeError) ContentType() string {
	return unknownMapLayerContentTypeError.contentType
}


// Returns the error description including the type.
func (unknownMapLayerContentTypeError UnknownMapLayerContentTypeError) Error() string {
	return "Unknown map layer content type: " + unknownMapLayerContentTypeError.contentType
}


// Creates an Unknown Map Layer Content Type error.
func UnknownMapLayerContentType(contentType string) UnknownMapLayerContentTypeError {
	return UnknownMapLayerContentTypeError{
		errors.GMM("unknown-map-layer-content-type", ""),
		contentType,
	}
}
