package tile_sprites

import (
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// Sprite sheets belong to a path, have a sheet id, the filename
// (which will closely relate to the directory/url in its parent
// path), and also the dimensions, in pixels, of each of the tiles
// (e.g. (32, 32)). The front-end must properly process the size
// of the tiles to make use of it in, say, Unity, properly.
// An additional field is needed for unity: the pixels/unit ratio.
type TileSpriteSheet struct {
	resources.BaseObject
	path          *TileSpriteSheetPath
	tileWidth     uint32
	tileHeight    uint32
	pixelsPerUnit uint16
	name          string
}


// Returns the content of this sprite sheet.
// Also: Compliance with the `resources.Object` interface.
func (tileSpriteSheet *TileSpriteSheet) Content() interface{} {
	return struct {
		Path          string
		TileWidth     uint32
		TileHeight    uint32
		PixelsPerUnit uint16
		Name          string
	}{
		tileSpriteSheet.path.URI(),
		tileSpriteSheet.tileWidth,
		tileSpriteSheet.tileHeight,
		tileSpriteSheet.pixelsPerUnit,
		tileSpriteSheet.name,
	}
}


// Loads a new tile sheet from its data.
func (tileSpriteSheet *TileSpriteSheet) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(dump.(map[string]interface{}))
	return NewTileSpriteSheet(
		registry.Resolve(fromMap.Get("Path").AsString()).(*TileSpriteSheetPath),
		id,
		fromMap.Get("Name").AsString(),
		fromMap.Get("TileWidth").AsUint32(),
		fromMap.Get("TileHeight").AsUint32(),
		fromMap.Get("PixelsPerUnit").AsUint16(),
	)
}


// Returns the parent path of a tile sprite sheet.
func (tileSpriteSheet *TileSpriteSheet) Path() *TileSpriteSheetPath {
	return tileSpriteSheet.path
}


// Returns the tile dimensions of this sprite sheet as (width, height)
// expressed in image pixels.
func (tileSpriteSheet *TileSpriteSheet) TileSize() (uint32, uint32) {
	return tileSpriteSheet.tileWidth, tileSpriteSheet.tileHeight
}


// Returns the "pixels per unit" for this sprite sheet.
func (tileSpriteSheet *TileSpriteSheet) PixelPerUnits() uint16 {
	return tileSpriteSheet.pixelsPerUnit
}


// Returns the name of this sprite sheet. Usually, the name corresponds
// to a filename or some sort of object key (e.g. in AWS). It will be
// deeply related to the path it belongs to.
func (tileSpriteSheet *TileSpriteSheet) Name() string {
	return tileSpriteSheet.name
}


// Creates a new tile sprite sheet for some path and tile dimensions.
func NewTileSpriteSheet(path *TileSpriteSheetPath, id uint64, name string, tileWidth, tileHeight uint32, pixelsPerUnit uint16) *TileSpriteSheet {
	pixelsPerUnit = values.MaxU16(1, pixelsPerUnit)
	return &TileSpriteSheet{resources.NewBaseObject(id), path, tileWidth, tileHeight, pixelsPerUnit, name}
}

