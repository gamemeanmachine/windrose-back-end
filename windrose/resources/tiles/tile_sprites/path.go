package tile_sprites

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// Sprite sheets paths contain a base path for the sprite
// sheets they contain.
type TileSpriteSheetPath struct {
	resources.BaseObject
	path    string
}


// Returns the content of this sprite sheet path.
// Also: Compliance with the `resources.Object` interface.
func (tileSpriteSheetPath *TileSpriteSheetPath) Content() interface{} {
	return struct{
		Path string
	}{
		tileSpriteSheetPath.path,
	}
}


// Loads a new tile sprite from its data.
func (tileSpriteSheetPath *TileSpriteSheetPath) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	path := from.Map(dump.(map[string]interface{})).Get("Path").AsString()
	return NewTileSpriteSheetPath(id, path)
}


// Returns the actual path of this sprite sheet path.
func (tileSpriteSheetPath *TileSpriteSheetPath) Path() string {
	return tileSpriteSheetPath.path
}


// Creates a new tile sprite sheet path for some url/directory.
func NewTileSpriteSheetPath(id uint64, path string) *TileSpriteSheetPath {
	return &TileSpriteSheetPath{resources.NewBaseObject(id), path}
}

