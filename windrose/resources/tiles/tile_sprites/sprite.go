package tile_sprites

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// Tile sprites are references to a specific tile inside a sprite sheet
// tile. Since tile sprite sheets are images, tiles can be identified in
// terms of a specific row and a specific column. Inner cells must be
// arranged in the order Unity understands for textures, so they match
// with the in-sheet visual order.
type TileSprite struct {
	resources.BaseObject
	tileSpriteSheet *TileSpriteSheet
	tileRow         uint32
	tileColumn      uint32
}


// Returns the content of this sprite sheet.
// Also: Compliance with the `resources.Object` interface.
func (tileSprite *TileSprite) Content() interface{} {
	return struct {
		TileSpriteSheet string
		TileRow         uint32
		TileColumn      uint32
	}{
		tileSprite.tileSpriteSheet.URI(),
		tileSprite.tileRow,
		tileSprite.tileColumn,
	}
}


// Loads a new tile sprite from its data.
func (tileSprite *TileSprite) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(dump.(map[string]interface{}))
	return NewTileSprite(
		registry.Resolve(fromMap.Get("TileSpriteSheet").AsString()).(*TileSpriteSheet),
		id,
		fromMap.Get("TileRow").AsUint32(),
		fromMap.Get("TileColumn").AsUint32(),
	)
}


// Gets the tile sheet this tile belongs to.
func (tileSprite *TileSprite) Sheet() *TileSpriteSheet {
	return tileSprite.tileSpriteSheet
}


// Gets the in-sheet position of this tile sprite as (row, column)
// expressed in tile offsets.
func (tileSprite *TileSprite) TilePosition() (uint32, uint32) {
	return tileSprite.tileRow, tileSprite.tileColumn
}


// Creates a new tile sprite for some sheet and coordinates. The arguments
// must be chosen carefully, for validation will not be performed for them.
func NewTileSprite(sheet *TileSpriteSheet, id uint64, tileRow, tileColumn uint32) *TileSprite {
	return &TileSprite{resources.NewBaseObject(id), sheet, tileRow, tileColumn}
}