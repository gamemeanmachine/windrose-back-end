package tile_displays

import "gitlab.com/gamemeanmachine/resource-servers/v2"


// Local tile displays exist in the game logic, but it is
// responsibility of the front-end to choose how to render
// them. These ones are used when skinning is desirable and
// the server-side doesn't actually care about the type of
// tiles being used in the front-end.
type LocalTileDisplayContent struct {}


// Returns "local" as the display type.
func (*LocalTileDisplayContent) TileDisplayType() string {
	return "local"
}


// Returns the display data. For the local tile displays,
// this value is empty since only the display id and display
// type are needed: the front-end must be able to render this
// tile appropriately (e.g. via a local tile registry in the
// front-end of the game).
func (localTileDisplay *LocalTileDisplayContent) TileDisplayData() interface{}{
	return nil
}


// Loads a new local type display content... but needs nothing
// to do that. No data is used.
func (localTileDisplay *LocalTileDisplayContent) TileDisplayLoad(data map[string]interface{}, registry *resources.Registry) TileDisplayContent {
	return &LocalTileDisplayContent{}
}


// Registers this TileDisplayContent.
var _ = AddPlugIn(&LocalTileDisplayContent{})