package tile_displays

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
	errors2 "gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/errors"
)


// Tile displays involve different mechanisms and
// have arbitrary contents: They will have a display
// id, and a display type (initially, only 4 will
// exist), but they have additional content, which
// is different depending on the type.
type TileDisplayContent interface {
	TileDisplayType() string
	TileDisplayData() interface{}
	TileDisplayLoad(data map[string]interface{}, registry *resources.Registry) TileDisplayContent
}


// Tile displays have only the display ID, and wrap
// the actual content / implementation of a display.
type TileDisplay struct {
	resources.BaseObject
	content TileDisplayContent
}



// Returns the display content of a display.
func (tileDisplay *TileDisplay) Content() interface{} {
	return struct {
		Type string
		Data interface{}
	}{
		tileDisplay.content.TileDisplayType(),
		tileDisplay.content.TileDisplayData(),
	}
}


// Inflates this object with the given data, accounting for the
// appropriate type of display content.
func (tileDisplay *TileDisplay) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(dump.(map[string]interface{}))
	contentType := fromMap.Get("Type").AsString()
	contentData := fromMap["Data"].(map[string]interface{})
	if plugin, ok := plugins[contentType]; !ok {
		panic(errors2.UnknownTileDisplayContentType(contentType))
	} else {
		return &TileDisplay{
			BaseObject: resources.NewBaseObject(id),
			content:    plugin.TileDisplayLoad(contentData, registry),
		}
	}
}


/////////////////////////////////////////////////////////
// Plug-in system for tile displays.
/////////////////////////////////////////////////////////


var plugins = map[string]TileDisplayContent{}


// Register an EntityVisualContent-implementing type.
func AddPlugIn(content TileDisplayContent) bool {
	if content == nil {
		panic(errors.Argument(
			"windrose.resources.tiles.tile_displays.AddPlugIn",
			"content", "Must not be nil",
		))
	} else if registered, ok := plugins[content.TileDisplayType()]; ok && registered != content {
		panic(errors.Argument(
			"windrose.resources.tiles.tile_displays.AddPlugIn",
			"content", "Another plugin of the same type is already registered",
		))
	} else if ok {
		return false
	} else {
		plugins[content.TileDisplayType()] = content
		return true
	}
}