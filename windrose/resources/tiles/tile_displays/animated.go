package tile_displays

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_sprites"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// Animated tile displays have min and max speeds, and they
// correspond to Animated Tiles in the Unity2D Extras library.
type AnimatedTileDisplayContent struct {
	minSpeed  float64
	maxSpeed  float64
	startTime float64
	frames    []*tile_sprites.TileSprite
}


// Returns "animated" as the display type.
func (*AnimatedTileDisplayContent) TileDisplayType() string {
	return "animated"
}


// Returns the display data. This just involves returning
// the `content` private field, which will rendered as json.
func (animatedTileDisplayContent *AnimatedTileDisplayContent) TileDisplayData() interface{}{
	frameIDs := make([]string, len(animatedTileDisplayContent.frames))
	for index, frame := range animatedTileDisplayContent.frames {
		frameIDs[index] = frame.URI()
	}
	return struct{
		MinSpeed  float64
		MaxSpeed  float64
		StartTime float64
		Frames    []string
	}{
		animatedTileDisplayContent.minSpeed,
		animatedTileDisplayContent.maxSpeed,
		animatedTileDisplayContent.startTime,
		frameIDs,
	}
}


// Loads the data into a new static tile display content.
func (animatedTileDisplayContent *AnimatedTileDisplayContent) TileDisplayLoad(data map[string]interface{}, registry *resources.Registry) TileDisplayContent {
	fromMap := from.Map(data)
	frameURIs := fromMap["Frames"].([]string)
	frames := make([]*tile_sprites.TileSprite, len(frameURIs))
	for index, frameURI := range frameURIs {
		frames[index] = registry.Resolve(frameURI).(*tile_sprites.TileSprite)
	}
	return &AnimatedTileDisplayContent{
		fromMap.Get("MinSpeed").AsFloat64(),
		fromMap.Get("MaxSpeed").AsFloat64(),
		fromMap.Get("StartTime").AsFloat64(),
		frames,
	}
}


// Registers this TileDisplayContent.
var _ = AddPlugIn(&AnimatedTileDisplayContent{})