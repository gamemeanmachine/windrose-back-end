package tile_displays

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_sprites"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// Creates a local tile display.
func NewLocalTileDisplay(id uint64) *TileDisplay {
	return &TileDisplay{resources.NewBaseObject(id), &LocalTileDisplayContent{}}
}


// Creates a static tile display.
func NewStaticTileDisplay(id uint64, sprite *tile_sprites.TileSprite) *TileDisplay {
	return &TileDisplay{resources.NewBaseObject(id), &StaticTileDisplayContent{sprite}}
}


// Creates an animated tile display.
func NewAnimatedTileDisplay(id uint64, minSpeed, maxSpeed, startTime float64, frames []*tile_sprites.TileSprite) *TileDisplay{
	framesCopy := make([]*tile_sprites.TileSprite, len(frames))
	copy(framesCopy, frames)
	return &TileDisplay{resources.NewBaseObject(id), &AnimatedTileDisplayContent{
		minSpeed,
		maxSpeed,
		startTime,
		framesCopy,
	}}
}