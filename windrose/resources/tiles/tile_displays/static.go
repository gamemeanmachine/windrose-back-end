package tile_displays

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_sprites"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// Static tile displays have only one frame. This is the
// most basic type games can have (and the most frequent one)
// rendered from server-side data.
type StaticTileDisplayContent struct {
	sprite *tile_sprites.TileSprite
}


// Returns "static" as the display type.
func (*StaticTileDisplayContent) TileDisplayType() string {
	return "static"
}


// Returns the display data. This just involves returning
// the content for this tile.
func (staticTileDisplayContent *StaticTileDisplayContent) TileDisplayData() interface{}{
	return struct{
		Sprite string
	}{
		staticTileDisplayContent.sprite.URI(),
	}
}


// Loads the data into a new static tile display content.
func (staticTileDisplayContent *StaticTileDisplayContent) TileDisplayLoad(data map[string]interface{}, registry *resources.Registry) TileDisplayContent {
	fromMap := from.Map(data)
	return &StaticTileDisplayContent{
		registry.Resolve(fromMap.Get("Sprite").AsString()).(*tile_sprites.TileSprite),
	}
}


// Registers this TileDisplayContent.
var _ = AddPlugIn(&StaticTileDisplayContent{})