package tiles

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_displays"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_strategies"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// A tile is defined by three components: its ID, its display, and its strategies
// (both for the usage and for the dump purposes).
type Tile struct {
	resources.BaseObject
	display    *tile_displays.TileDisplay
	strategies tile_strategies.TileStrategies
}


// Dumps all the strategy content into a map suitable
// to be dumped via some sort of "resource server".
func (tile *Tile) Content() interface{} {
	strategyURIs := map[string]string{}
	for _, strategy := range tile.strategies {
		if !strategy.Secret() {
			strategyURIs[strategy.TileStrategyType()] = strategy.URI()
		}
	}
	return struct {
		Display    string
		Strategies map[string]string
	}{
		tile.display.URI(),
		strategyURIs,
	}
}


// Loads a new tile from its data.
func (tile *Tile) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(dump.(map[string]interface{}))
	strategies := make(tile_strategies.TileStrategies)
	strategyURIs := fromMap["Strategies"].(map[string]string)
	for key, uri := range strategyURIs {
		strategies[key] = registry.Resolve(uri).(*tile_strategies.TileStrategy)
	}
	return &Tile{
		resources.NewBaseObject(id),
		registry.Resolve(fromMap.Get("Display").AsString()).(*tile_displays.TileDisplay),
		strategies,
	}
}


// Creates a new tile, given an id and display.
func NewTile(id uint64, display *tile_displays.TileDisplay) *Tile {
	return &Tile{resources.NewBaseObject(id), display, tile_strategies.TileStrategies{}}
}
