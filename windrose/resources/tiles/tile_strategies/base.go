package tile_strategies

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	errors2 "gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/errors"
)

// Tile strategies have arbitrary contents that will
// be used by map strategies and perhaps entity
// strategies. Each strategy will provide their
// own result as an arbitrary structure, and its key
// will be used just for informative purposes by the
// client-side (usually, Unity)... most notably for
// the "layout" to also check in the front-end, to
// ease the user experience.
//
// Please note: dumping the content is only meaningful
// for clients, and not for servers. Tile strategies
// should only dump content when a client-side "help"
// is needed beforehand, and should dump `null` when
// no content is needed. If the strategy presence it
// meant to be secret (i.e. the client must not be
// aware of it) TileStrategyIsSecret() must return
// true. Otherwise, it should return false.
//
// Also note: non-secret strategies MAY choose not to
// serve the strategy content to the UI. In these
// cases, this means that it is expected that the
// client already has a default "local" copy of the
// strategy. This is not the same as "secret" tile
// strategies, for even their presence is concealed
// (for the sake of cheat-prevention), while the tile
// strategies that omit data do not conceal their
// presence but just rely on the client side to know
// how to "help" themselves for the sake of game
// experience.
type TileStrategyContent interface {
	TileStrategyType() string
	TileStrategyData() interface{}
	TileStrategyLoad(data map[string]interface{}, registry *resources.Registry) TileStrategyContent
	TileStrategySecret() bool
}

// Tile strategies have only the strategy id, and wrap
// the actual content / implementation of a strategy.
type TileStrategy struct {
	resources.BaseObject
	content TileStrategyContent
}

// Maps of tile features are done by type.
// Each tile will have its own feature.
type TileStrategies map[string]*TileStrategy

// Returns whether the underlying strategy is secret or not.
func (tileStrategy *TileStrategy) Secret() bool {
	return tileStrategy.content.TileStrategySecret()
}

// Returns the underlying strategy type.
func (tileStrategy *TileStrategy) TileStrategyType() string {
	return tileStrategy.content.TileStrategyType()
}

// Dumps the content of this strategy, in terms of its type
// key and the actual content.
func (tileStrategy *TileStrategy) Content() interface{} {
	return struct {
		Type string
		Data interface{}
	}{
		tileStrategy.content.TileStrategyType(),
		tileStrategy.content.TileStrategyData(),
	}
}

// Loads the data to populate this strategy with its content.
func (tileStrategy *TileStrategy) Inflate(id uint64, data interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(data.(map[string]interface{}))
	contentType := fromMap.Get("Type").AsString()
	contentData := fromMap["Data"].(map[string]interface{})
	if plugin, ok := plugins[contentType]; !ok {
		panic(errors2.UnknownTileStrategyContentType(contentType))
	} else {
		return &TileStrategy{
			BaseObject: resources.NewBaseObject(id),
			content:    plugin.TileStrategyLoad(contentData, registry),
		}
	}
}

// Creates a tile strategy object, using a tile strategy
// content as wrapped logic.
func NewTileStrategy(id uint64, content TileStrategyContent) *TileStrategy {
	return &TileStrategy{resources.NewBaseObject(id), content}
}

/////////////////////////////////////////////////////////
// Plug-in system for tile strategies.
/////////////////////////////////////////////////////////

var plugins = map[string]TileStrategyContent{}

// Register an EntityVisualContent-implementing type.
func AddPlugIn(content TileStrategyContent) bool {
	if content == nil {
		panic(errors.Argument(
			"windrose.resources.tiles.tile_strategies.AddPlugIn",
			"content", "Must not be nil",
		))
	} else if registered, ok := plugins[content.TileStrategyType()]; ok && registered != content {
		panic(errors.Argument(
			"windrose.resources.tiles.tile_strategies.AddPlugIn",
			"content", "Another plugin of the same type is already registered",
		))
	} else if ok {
		return false
	} else {
		plugins[content.TileStrategyType()] = content
		return true
	}
}
