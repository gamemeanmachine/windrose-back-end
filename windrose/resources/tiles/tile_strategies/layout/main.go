package layout

import (
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_strategies"
)

// Layout strategies will only tell whether a tile
// will block, or release blocking. For tiles that
// intend to respect the layout setting from a tile
// in the lower-depth tilemaps (in the same map),
// this strategy must NOT be added.
type LayoutTileStrategyContent struct {
	blocks bool
}

// Returns "layout" as tile strategy key. Remember:
// custom/external strategies should use a fully
// qualified name (e.g. a domain name).
func (*LayoutTileStrategyContent) TileStrategyType() string {
	return "layout"
}

// Returns the layout strategy content, which has only
// one field.
func (layoutTileStrategyContent *LayoutTileStrategyContent) TileStrategyData() interface{} {
	return struct {
		Blocks bool
	}{
		layoutTileStrategyContent.blocks,
	}
}

// Loads the data into a new tile strategy instance.
func (layoutTileStrategyContent *LayoutTileStrategyContent) TileStrategyLoad(data map[string]interface{}, registry *resources.Registry) tile_strategies.TileStrategyContent {
	fromMap := from.Map(data)
	return &LayoutTileStrategyContent{
		fromMap.Get("Blocks").AsBool(),
	}
}

// Layout strategies are not secret.
func (*LayoutTileStrategyContent) TileStrategySecret() bool {
	return false
}

// For this particular strategy, get whether this strategy
// blocks or releases.
func (layoutTileStrategyContent *LayoutTileStrategyContent) Blocks() bool {
	return layoutTileStrategyContent.blocks
}

// Creates a new layout strategy content.
func NewLayoutTileStrategy(id uint64, blocks bool) *tile_strategies.TileStrategy {
	return tile_strategies.NewTileStrategy(id, &LayoutTileStrategyContent{blocks})
}

// Registers this TileStrategyContent.
var _ = tile_strategies.AddPlugIn(&LayoutTileStrategyContent{})
