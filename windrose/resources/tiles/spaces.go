package tiles

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_displays"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_sprites"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles/tile_strategies"
)


// Makes a space for all the resource objects defined in this
// package: tile sprite sheet paths, tile sprite sheets, tile sprites,
// tile displays, tile strategies, and tiles.
func MakeTilesSpace() *resources.Space {
	space := resources.NewSpace("tiles", "Tiles (all the involved objects)")
	space.Register(resources.NewList(
		"tile-sprite-sheet-paths",
		"Tile Spritesheet Paths",
		&tile_sprites.TileSpriteSheetPath{},
	))
	space.Register(resources.NewList(
		"tile-sprite-sheets",
		"Tile Spritesheets",
		&tile_sprites.TileSpriteSheet{},
	))
	space.Register(resources.NewList(
		"tile-sprites",
		"Tile Sprites",
		&tile_sprites.TileSprite{},
	))
	space.Register(resources.NewList(
		"tile-displays",
		"Tile Displays",
		&tile_displays.TileDisplay{},
	))
	space.Register(resources.NewList(
		"tile-strategies",
		"Tile Strategies",
		&tile_strategies.TileStrategy{},
	))
	space.Register(resources.NewList(
		"tiles",
		"Tiles",
		&Tile{},
	))
	return space
}