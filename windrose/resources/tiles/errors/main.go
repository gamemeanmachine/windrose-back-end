package errors

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
)


// Raised when trying to inflate a tile display
// but the sub-type is not registered as a plug-in.
type UnknownTileDisplayContentTypeError struct {
	errors.GMMError
	contentType string
}


// Returns the content type.
func (unknownTileDisplayContentTypeError UnknownTileDisplayContentTypeError) ContentType() string {
	return unknownTileDisplayContentTypeError.contentType
}


// Returns the error description including the type.
func (unknownTileDisplayContentTypeError UnknownTileDisplayContentTypeError) Error() string {
	return "Unknown tile display content type: " + unknownTileDisplayContentTypeError.contentType
}


// Creates an Unknown Tile Display Content Type error.
func UnknownTileDisplayContentType(contentType string) UnknownTileDisplayContentTypeError {
	return UnknownTileDisplayContentTypeError{
		errors.GMM("unknown-tile-display-content-type", ""),
		contentType,
	}
}


// Raised when trying to inflate a tile strategy
// but the sub-type is not registered as a plug-in.
type UnknownTileStrategyContentTypeError struct {
	errors.GMMError
	contentType string
}


// Returns the content type.
func (unknownTileStrategyContentTypeError UnknownTileStrategyContentTypeError) ContentType() string {
	return unknownTileStrategyContentTypeError.contentType
}


// Returns the error description including the type.
func (unknownTileStrategyContentTypeError UnknownTileStrategyContentTypeError) Error() string {
	return "Unknown tile strategy content type: " + unknownTileStrategyContentTypeError.contentType
}


// Creates an Unknown Tile Strategy Content Type error.
func UnknownTileStrategyContentType(contentType string) UnknownTileStrategyContentTypeError {
	return UnknownTileStrategyContentTypeError{
		errors.GMM("unknown-tile-strategy-content-type", ""),
		contentType,
	}
}
