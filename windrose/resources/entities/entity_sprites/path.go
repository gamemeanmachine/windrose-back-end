package entity_sprites

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// Sprite sheets paths contain a base path for the sprite
// sheets they contain.
type EntitySpriteSheetPath struct {
	resources.BaseObject
	path string
}


// Returns the content of this sprite sheet path.
// Also: compliance with the resources.Object interface.
func (entitySpriteSheetPath *EntitySpriteSheetPath) Content() interface{} {
	return struct{
		Path string
	}{
		entitySpriteSheetPath.path,
	}
}


// Returns the actual path of this sprite sheet path.
func (entitySpriteSheetPath *EntitySpriteSheetPath) Path() string {
	return entitySpriteSheetPath.path
}


// Inflates a path from JSON data.
func (*EntitySpriteSheetPath) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	path := from.Map(dump.(map[string]interface{})).Get("Path").AsString()
	return NewEntitySpriteSheetPath(id, path)
}


// Creates a new entity sprite sheet path for some url/directory.
func NewEntitySpriteSheetPath(id uint64, path string) *EntitySpriteSheetPath {
	return &EntitySpriteSheetPath{resources.NewBaseObject(id), path}
}
