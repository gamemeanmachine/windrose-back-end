package entity_sprites

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// Entity sprites are references to a specific entity inside a sprite sheet
// entity. Since entity sprite sheets are images, entities can be identified in
// terms of a specific row and a specific column. Inner cells must be
// arranged in the order Unity understands for textures, so they match
// with the in-sheet visual order.
type EntitySprite struct {
	resources.BaseObject
	entitySpriteSheet *EntitySpriteSheet
	entityRow         uint32
	entityColumn      uint32
}


// Gets the entity sheet this entity belongs to.
func (entitySprite *EntitySprite) Sheet() *EntitySpriteSheet {
	return entitySprite.entitySpriteSheet
}


// Gets the content of this entity sprite.
// Also: compliance with the resources.Object interface.
func (entitySprite *EntitySprite) Content() interface{} {
	return struct{
		EntitySpriteSheet string
		EntityRow         uint32
		EntityColumn      uint32
	}{
		entitySprite.entitySpriteSheet.URI(),
		entitySprite.entityRow,
		entitySprite.entityColumn,
	}
}


// Gets the in-sheet position of this entity sprite as (row, column)
// expressed in entity offsets.
func (entitySprite *EntitySprite) EntityPosition() (uint32, uint32) {
	return entitySprite.entityRow, entitySprite.entityColumn
}


// Inflates the content appropriately.
func (*EntitySprite) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(dump.(map[string]interface{}))
	entitySpriteSheet := registry.Resolve(fromMap.Get("EntitySpriteSheet").AsString()).(*EntitySpriteSheet)
	entityRow := fromMap.Get("EntityRow").AsUint32()
	entityColumn := fromMap.Get("EntityColumn").AsUint32()
	return NewEntitySprite(entitySpriteSheet, id, entityRow, entityColumn)
}


// Creates a new entity sprite for some sheet and coordinates. The arguments
// must be chosen carefully, for validation will not be performed for them.
func NewEntitySprite(sheet *EntitySpriteSheet, id uint64, entityRow, entityColumn uint32) *EntitySprite {
	return &EntitySprite{resources.NewBaseObject(id), sheet, entityRow, entityColumn}
}
