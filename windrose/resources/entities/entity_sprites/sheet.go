package entity_sprites

import (
	"gitlab.com/gamemeanmachine/golang-support/utils/values"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// Sprite sheets belong to a path, have a sheet id, the filename
// (which will closely relate to the directory/url in its parent
// path), and also the dimensions, in pixels, of each of the entities
// (e.g. (32, 32)). The front-end must properly process the size
// of the entities to make use of it in, say, Unity, properly.
// An additional field is needed for unity: the pixels/unit ratio.
type EntitySpriteSheet struct {
	resources.BaseObject
	path          *EntitySpriteSheetPath
	entityWidth   uint32
	entityHeight  uint32
	pixelsPerUnit uint16
	name          string
}


// Returns the parent path of a entity sprite sheet.
func (entitySpriteSheet *EntitySpriteSheet) Path() *EntitySpriteSheetPath {
	return entitySpriteSheet.path
}


// Returns the content of this sprite sheet.
// Also: compliance with the resources.Object interface.
func (entitySpriteSheet *EntitySpriteSheet) Content() interface{} {
	return struct{
		Path          string
		EntityWidth   uint32
		EntityHeight  uint32
		PixelsPerUnit uint16
		Name          string
	}{
		entitySpriteSheet.path.URI(),
		entitySpriteSheet.entityWidth,
		entitySpriteSheet.entityHeight,
		entitySpriteSheet.pixelsPerUnit,
		entitySpriteSheet.name,
	}
}


// Returns the entity dimensions of this sprite sheet as (width, height)
// expressed in image pixels.
func (entitySpriteSheet *EntitySpriteSheet) EntitySize() (uint32, uint32) {
	return entitySpriteSheet.entityWidth, entitySpriteSheet.entityHeight
}


// Returns the "pixels per unit" for this sprite sheet.
func (entitySpriteSheet *EntitySpriteSheet) PixelPerUnits() uint16 {
	return entitySpriteSheet.pixelsPerUnit
}


// Returns the name of this sprite sheet. Usually, the name corresponds
// to a filename or some sort of object key (e.g. in AWS). It will be
// deeply related to the path it belongs to.
func (entitySpriteSheet *EntitySpriteSheet) Name() string {
	return entitySpriteSheet.name
}


// Inflates the content appropriately.
func (*EntitySpriteSheet) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(dump.(map[string]interface{}))
	path := registry.Resolve(fromMap.Get("Path").AsString()).(*EntitySpriteSheetPath)
	entityWidth := fromMap.Get("EntityWidth").AsUint32()
	entityHeight := fromMap.Get("EntityHeight").AsUint32()
	pixelsPerUnit := fromMap.Get("PixelsPerUnit").AsUint16()
	name := fromMap.Get("Name").AsString()
	return NewEntitySpriteSheet(path, id, name, entityWidth, entityHeight, pixelsPerUnit)
}


// Creates a new entity sprite sheet for some path and entity dimensions.
func NewEntitySpriteSheet(path *EntitySpriteSheetPath, id uint64, name string, entityWidth, entityHeight uint32, pixelsPerUnit uint16) *EntitySpriteSheet {
	pixelsPerUnit = values.MaxU16(1, pixelsPerUnit)
	return &EntitySpriteSheet{resources.NewBaseObject(id), path, entityWidth, entityHeight, pixelsPerUnit, name}
}

