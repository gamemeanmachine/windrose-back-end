package local

import "gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals"


// Local base entity visual is a parent class of
// entity visuals that are entire responsibility
// of the front-end to render.
type BaseEntityVisualContent struct {
	entity_visuals.EntityVisual
}


// Local entity visuals have no content. They only have ID (and their type).
func (baseEntityVisualContent *BaseEntityVisualContent) VisualData() interface{} {
	return nil
}
