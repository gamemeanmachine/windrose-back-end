package local

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)

// A client-rendered local animated entity visual.
// The client must map this resource appropriately.
type AnimatedEntityVisualContent struct {
	BaseEntityVisualContent
}


// Returns "animated:local" as its type.
func (*AnimatedEntityVisualContent) VisualType() string {
	return "animated:local"
}


// Since this is a local object, it will not inflate but create a dummy instance.
func (*AnimatedEntityVisualContent) VisualLoad(data map[string]interface{}, registry *resources.Registry) entity_visuals.EntityVisualContent {
	return &AnimatedEntityVisualContent{}
}


// Creates a locally-rendered animated visual.
func NewAnimatedEntityVisual(id uint64) *entity_visuals.EntityVisual {
	return &entity_visuals.EntityVisual{
		resources.NewBaseObject(id),
		&AnimatedEntityVisualContent{},
	}
}


// Registers this EntityVisualContent.
var _ = entity_visuals.AddPlugIn(&AnimatedEntityVisualContent{})