package local

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// A client-rendered local static entity visual.
// The client must map this resource appropriately.
type StaticEntityVisualContent struct {
	BaseEntityVisualContent
}


// Returns "static:local" as its type.
func (*StaticEntityVisualContent) VisualType() string {
	return "static:local"
}


// Since this is a local object, it will not inflate but create a dummy instance.
func (*StaticEntityVisualContent) VisualLoad(data map[string]interface{}, registry *resources.Registry) entity_visuals.EntityVisualContent {
	return &StaticEntityVisualContent{}
}


// Creates a locally-rendered animated visual.
func NewStaticEntityVisual(id uint64) *entity_visuals.EntityVisual {
	return &entity_visuals.EntityVisual{
		resources.NewBaseObject(id),
		&StaticEntityVisualContent{},
	}
}


// Registers this EntityVisualContent.
var _ = entity_visuals.AddPlugIn(&StaticEntityVisualContent{})