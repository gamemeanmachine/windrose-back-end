package local

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// A client-rendered local rose-animated entity visual.
// The client must map this resource appropriately.
type RoseAnimatedEntityVisualContent struct {
	BaseEntityVisualContent
}


// Returns "rose-animated:local" as its type.
func (*RoseAnimatedEntityVisualContent) VisualType() string {
	return "rose-animated:local"
}


// Since this is a local object, it will not inflate but create a dummy instance.
func (*RoseAnimatedEntityVisualContent) VisualLoad(data map[string]interface{}, registry *resources.Registry) entity_visuals.EntityVisualContent {
	return &RoseAnimatedEntityVisualContent{}
}


// Creates a locally-rendered animated visual.
func NewRoseAnimatedEntityVisual(id uint64) *entity_visuals.EntityVisual {
	return &entity_visuals.EntityVisual{
		resources.NewBaseObject(id),
		&RoseAnimatedEntityVisualContent{},
	}
}


// Registers this EntityVisualContent.
var _ = entity_visuals.AddPlugIn(&RoseAnimatedEntityVisualContent{})