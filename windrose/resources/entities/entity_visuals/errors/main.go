package errors

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
)


// Raised when trying to inflate an entity
// visual, but the sub-type is not registered
// as a plug-in.
type UnknownEntityVisualContentTypeError struct {
	errors.GMMError
	contentType string
}


// Returns the content type.
func (unknownEntityVisualContentTypeError UnknownEntityVisualContentTypeError) ContentType() string {
	return unknownEntityVisualContentTypeError.contentType
}


// Returns the error description including the type.
func (unknownEntityVisualContentTypeError UnknownEntityVisualContentTypeError) Error() string {
	return "Unknown entity visual content type: " + unknownEntityVisualContentTypeError.contentType
}


// Creates an Unknown Entity Visual Content Type error.
func UnknownEntityVisualContentType(contentType string) UnknownEntityVisualContentTypeError {
	return UnknownEntityVisualContentTypeError{
		errors.GMM("unknown-visual-entity-content-type", ""),
		contentType,
	}
}