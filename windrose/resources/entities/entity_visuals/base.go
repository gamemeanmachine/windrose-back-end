package entity_visuals

import (
	"gitlab.com/gamemeanmachine/golang-support/errors"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
	errors2 "gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals/errors"
)


// Entity visuals are components that can be added
// to an entity (more than one is allowed). They have
// specific settings that have to be taken by the
// front-end to create the actual visual objects.
// This interface defines the behaviour of the content.
type EntityVisualContent interface {
	VisualType() string
	VisualData() interface{}
	// This method is invoked as class-only and
	// inflates a new instance (of the same type)
	// given just-parsed JSON data.
	VisualLoad(data map[string]interface{}, registry *resources.Registry) EntityVisualContent
}


// Entity Visuals are the things one sees when looking
// at an in-map object.
type EntityVisual struct {
	resources.BaseObject
	content EntityVisualContent
}


// Returns the content of a display.
// Also: compliance with the resources.Object interface.
func (entityVisual *EntityVisual) Content() interface{} {
	return struct {
		Type string
		Data interface{}
	} {
		entityVisual.content.VisualType(),
		entityVisual.content.VisualData(),
	}
}


// Inflates an entity visual given the input dump.
// The dump, here, will only contain two fields: the
// sub-type and the sub-data, that are used to pick
// and instantiate the wrapped component. It bubbles
// any panic and also panics if the parsed type is
// not registered.
func (entityVisual *EntityVisual) Inflate(id uint64, dump interface{}, registry *resources.Registry) resources.Object {
	fromMap := from.Map(dump.(map[string]interface{}))
	contentType := fromMap.Get("Type").AsString()
	contentData := fromMap["Data"].(map[string]interface{})
	if plugin, ok := plugins[contentType]; !ok {
		panic(errors2.UnknownEntityVisualContentType(contentType))
	} else {
		return &EntityVisual{
			BaseObject: resources.NewBaseObject(id),
			content:    plugin.VisualLoad(contentData, registry),
		}
	}
}


/////////////////////////////////////////////////////////
// Plug-in system for visuals.
/////////////////////////////////////////////////////////


var plugins = map[string]EntityVisualContent{}


// Register an EntityVisualContent-implementing type.
func AddPlugIn(content EntityVisualContent) bool {
	if content == nil {
		panic(errors.Argument(
			"windrose.resources.entities.entity_visuals.PlugIn",
			"content", "Must not be nil",
		))
	} else if registered, ok := plugins[content.VisualType()]; ok && registered != content {
		panic(errors.Argument(
			"windrose.resources.entities.entity_visuals.PlugIn",
			"content", "Another plugin of the same type is already registered",
		))
	} else if ok {
		return false
	} else {
		plugins[content.VisualType()] = content
		return true
	}
}
