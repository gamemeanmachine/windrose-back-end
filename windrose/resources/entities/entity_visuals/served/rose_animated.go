package served

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_sprites"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
)


// These visual consists of several sprites
// being animated at a specified frame rate,
// which are specified in different sets for
// each allowed direction.
type RoseAnimatedEntityVisualContent struct {
	up, left, right, down AnimatedEntityVisualContent
}


// Returns "rose-animated" as the display type.
func (*RoseAnimatedEntityVisualContent) VisualType() string {
	return "rose-animated"
}


// Returns the visual data. This just involves returning
// the content for this visual.
func (roseAnimatedEntityVisualContent *RoseAnimatedEntityVisualContent) VisualData() interface{}{
	return struct {
		Up, Left, Right, Down interface {}
	} {
		roseAnimatedEntityVisualContent.up.VisualData(),
		roseAnimatedEntityVisualContent.left.VisualData(),
		roseAnimatedEntityVisualContent.right.VisualData(),
		roseAnimatedEntityVisualContent.down.VisualData(),
	}
}


// Parses the visual data from a dump of the content.
func (roseAnimatedEntityVisualContent *RoseAnimatedEntityVisualContent) VisualLoad(data map[string]interface{}, registry *resources.Registry) entity_visuals.EntityVisualContent {
	return &RoseAnimatedEntityVisualContent{
		up: *AnimatedEntityVisualContent{}.VisualLoad(data["Up"].(map[string]interface{}), registry).(*AnimatedEntityVisualContent),
		down: *AnimatedEntityVisualContent{}.VisualLoad(data["Down"].(map[string]interface{}), registry).(*AnimatedEntityVisualContent),
		left: *AnimatedEntityVisualContent{}.VisualLoad(data["Left"].(map[string]interface{}), registry).(*AnimatedEntityVisualContent),
		right: *AnimatedEntityVisualContent{}.VisualLoad(data["Right"].(map[string]interface{}), registry).(*AnimatedEntityVisualContent),
	}
}


// Creates a new RoseAnimated visual by providing the id and the
// sprite.
func NewRoseAnimatedEntityVisual(
	id uint64, upFPS, leftFPS, rightFPS, downFPS uint32,
	upSprites, leftSprites, rightSprites, downSprites []*entity_sprites.EntitySprite,
) *entity_visuals.EntityVisual {
	upSpritesCopy := make([]*entity_sprites.EntitySprite, len(upSprites))
	leftSpritesCopy := make([]*entity_sprites.EntitySprite, len(leftSprites))
	rightSpritesCopy := make([]*entity_sprites.EntitySprite, len(rightSprites))
	downSpritesCopy := make([]*entity_sprites.EntitySprite, len(downSprites))
	copy(upSpritesCopy, upSprites)
	copy(leftSpritesCopy, leftSprites)
	copy(rightSpritesCopy, rightSprites)
	copy(downSpritesCopy, downSprites)
	return &entity_visuals.EntityVisual{
		resources.NewBaseObject(id),
		&RoseAnimatedEntityVisualContent{
			AnimatedEntityVisualContent{upFPS, upSpritesCopy},
			AnimatedEntityVisualContent{leftFPS, leftSpritesCopy},
			AnimatedEntityVisualContent{rightFPS, rightSpritesCopy},
			AnimatedEntityVisualContent{downFPS, downSpritesCopy},
		},
	}
}


// Registers this EntityVisualContent.
var _ = entity_visuals.AddPlugIn(&RoseAnimatedEntityVisualContent{})