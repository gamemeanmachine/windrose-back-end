package served

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_sprites"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// These visual consists of several frames
// being animated at a specified frame rate.
// Serves well for a default asset, chosen at
// server side, which has no movement or even
// orientation (e.g. a glowing crystal, a pillar,
// a flowing waterfall, a fountain, an animated
// furniture piece, ...).
type AnimatedEntityVisualContent struct {
	fps    uint32
	frames []*entity_sprites.EntitySprite
}


// Returns "animated" as the display type.
func (*AnimatedEntityVisualContent) VisualType() string {
	return "animated"
}


// Returns the visual data. This just involves returning
// the content for this visual.
func (animatedEntityVisualContent *AnimatedEntityVisualContent) VisualData() interface{}{
	frameUris := make([]string, len(animatedEntityVisualContent.frames))
	for index, sprite := range animatedEntityVisualContent.frames {
		frameUris[index] = sprite.URI()
	}
	return struct{
		FPS    uint32
		Frames []string
	} {
		animatedEntityVisualContent.fps,
		frameUris,
	}
}


// Parses the visual data from a dump of the content.
func (animatedEntityVisualContent *AnimatedEntityVisualContent) VisualLoad(data map[string]interface{}, registry *resources.Registry) entity_visuals.EntityVisualContent {
	fromMap := from.Map(data)
	spritesURIs := fromMap["Frames"].([]string)
	sprites := make([]*entity_sprites.EntitySprite, len(spritesURIs))
	for index, spriteURI := range spritesURIs {
		sprites[index] = registry.Resolve(spriteURI).(*entity_sprites.EntitySprite)
	}
	return &AnimatedEntityVisualContent{
		fromMap.Get("FPS").AsUint32(),
		sprites,
	}
}


// Creates a new Animated visual by providing the id and the sprite.
func NewAnimatedEntityVisual(id uint64, fps uint32, frames []*entity_sprites.EntitySprite) *entity_visuals.EntityVisual {
	framesCopy := make([]*entity_sprites.EntitySprite, len(frames))
	copy(framesCopy, frames)
	return &entity_visuals.EntityVisual{
		resources.NewBaseObject(id),
		&AnimatedEntityVisualContent{fps, framesCopy},
	}
}


// Registers this EntityVisualContent.
var _ = entity_visuals.AddPlugIn(&AnimatedEntityVisualContent{})