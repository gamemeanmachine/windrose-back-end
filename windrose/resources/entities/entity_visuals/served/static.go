package served

import (
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_sprites"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals"
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/golang-support/utils/from"
)


// These visual consists of just one sprite.
// Serves well for a default asset, chosen at
// server side, which has no movement or even
// orientation (e.g. a rock, or a door).
type StaticEntityVisualContent struct {
	Sprite *entity_sprites.EntitySprite
}


// Returns "static" as the display type.
func (*StaticEntityVisualContent) VisualType() string {
	return "static"
}


// Returns the visual data. This just involves returning
// the content for this visual.
func (staticEntityVisualContent *StaticEntityVisualContent) VisualData() interface{}{
	return struct {
		Sprite string
	} {
		staticEntityVisualContent.Sprite.URI(),
	}
}


// Parses the visual data from a dump of the content.
func (staticEntityVisualContent *StaticEntityVisualContent) VisualLoad(data map[string]interface{}, registry *resources.Registry) entity_visuals.EntityVisualContent {
	fromMap := from.Map(data)
	return &StaticEntityVisualContent{
		registry.Resolve(fromMap.Get("Sprite").AsString()).(*entity_sprites.EntitySprite),
	}
}


// Creates a new Static visual by providing the id and the
// sprite.
func NewStaticEntityVisual(id uint64, sprite *entity_sprites.EntitySprite) *entity_visuals.EntityVisual {
	return &entity_visuals.EntityVisual{
		resources.NewBaseObject(id),
		&StaticEntityVisualContent{
			sprite,
		},
	}
}


// Registers this EntityVisualContent.
var _ = entity_visuals.AddPlugIn(&StaticEntityVisualContent{})