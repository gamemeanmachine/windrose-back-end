package entities

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_visuals"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities/entity_sprites"
)


// Makes a space for all the resource objects defined in this
// package: entity sprite sheet paths, entity sprite sheets,
// entity sprites, and entity visuals.
func MakeVisualsSpace() *resources.Space {
	space := resources.NewSpace("entities", "Entities (all the involved objects)")
	space.Register(resources.NewList(
		"entity-sprite-sheet-paths",
		"Entity Sprite Sheet Paths",
		&entity_sprites.EntitySpriteSheetPath{},
	))
	space.Register(resources.NewList(
		"entity-sprites",
		"Entity Sprite Sheets",
		&entity_sprites.EntitySpriteSheet{},
	))
	space.Register(resources.NewList(
		"entity-sprites",
		"Entity Sprites",
		&entity_sprites.EntitySprite{},
	))
	space.Register(resources.NewList(
		"entity-visuals",
		"Entity Visuals",
		&entity_visuals.EntityVisual{},
	))
	return space
}
