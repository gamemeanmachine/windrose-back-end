package resources

import (
	"gitlab.com/gamemeanmachine/resource-servers/v2"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/entities"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/tiles"
	"gitlab.com/gamemeanmachine/windrose-back-end/windrose/resources/map_templates"
)


// Makes an entire WindRose package, ready to be filled with appropriate
// resource entries.
func MakeWindRoseResourcesPackage() *resources.Package {
	packg := resources.NewPackage("windrose", "WindRose resources (related to maps, tiles, and entities)")
	packg.Register(tiles.MakeTilesSpace())
	packg.Register(map_templates.MakeMapTemplatesSpace())
	packg.Register(entities.MakeVisualsSpace())
	packg.Register(map_templates.MakeMapTemplatesSpace())
	return packg
}
