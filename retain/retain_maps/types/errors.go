package types

import "errors"

var InvalidBlockMaskCells = errors.New("invalid cells for the block mask (it doesn't have the same size)")
var CannotIncrement = errors.New("cannot increment position beyond its maximum")
var CannotDecrement = errors.New("cannot decrement position beyond its minimum")