package types

import (
	"../../gmm/utils"
	"../../gmm/ortho"
)

/**
 * A solidness mask involves block checking. Blocked positions are immutable flags, and no entity
 *   or even a ghost may traverse them because these blocks are actually part of the map's topology.
 */
type BlockMask struct {
	width uint8
	height uint8
	cells []bool
}

/**
 * Trivial constructor that initializes an empty array.
 */
func NewBlockMask(width, height uint8, cells []bool) (*BlockMask, error) {
	if len(cells) != int(width) * int(height) {
		return nil, InvalidBlockMaskCells
	}
	width = utils.Clamp8(1, width, 100)
	height = utils.Clamp8(1, height, 100)
	return &BlockMask{width: width, height: height, cells: make([]bool, width * height)}, nil
}

/**
 * Performs an operation on each square while the callback evaluates to false. May return:
 * - false, error: if dimensions are invalid.
 * - false, nil: the operation ran on each cell without halting (i.e. callback returned always false)
 * - true, nil: the operation halted on a cell (i.e. callback returned true on a cell)
 */
func (blockMask *BlockMask) each(x, y, width, height uint8, callback func(uint8, uint8, uint16) bool) (bool, error) {
	err := ortho.CheckBounds(ortho.Bounds{
		ortho.Position{x, y}, ortho.Size{width, height},
	}, blockMask)
	if err != nil {
		return false, err
	}

	yEnd := y + height
	for j := y; j < yEnd; j++ {
		offset := uint16(j) * uint16(blockMask.width) + uint16(x)
		var i uint8;
		for i = 0; i < width; i++ {
			if callback(i, j, offset) {
				return true, nil
			}
		}
	}
	return false, nil
}

/**
 * Functions to test blocked cells. They all return an error if the dimensions
 *   are invalid for this map.
 */

func (blockMask *BlockMask) BlockedSquare(x, y, width, height uint8) (bool, error) {
	res, err := blockMask.each(x, y, width, height, func(x, y uint8, offset uint16) bool {
		return blockMask.cells[offset]
	})
	return res, err
}

func (blockMask *BlockMask) BlockedRow(x, y, width uint8) (bool, error) {
	return blockMask.BlockedSquare(x, y, width, 1)
}

func (blockMask *BlockMask) BlockedColumn(x, y, height uint8) (bool, error) {
	return blockMask.BlockedSquare(x, y, 1, height)
}

/**
 * This one is to get a single position
 */
func (blockMask *BlockMask) GetAt(x, y uint8) bool {
	return blockMask.cells[uint16(y) * uint16(blockMask.height) + uint16(x)]
}

/**
 * Getters for width
 */

func (blockMask *BlockMask) GetWidth() uint8 {
	return blockMask.width
}

func (blockMask *BlockMask) GetHeight() uint8 {
	return blockMask.height
}