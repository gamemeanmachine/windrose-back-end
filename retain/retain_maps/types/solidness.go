package types

import (
	"../../gmm/ortho"
	"../../gmm/utils"
)

/**
 * A solidness mask involves solidness checking. Several objects can occupy same positions, but
 *   no objects being SOLID can traverse positions with ANY positive value.
 */
type SolidnessMask struct {
	width uint8
	height uint8
	cells []int16
}

/**
 * Trivial constructor that initializes an empty array.
 */
func NewSolidnessMask(width, height uint8) *SolidnessMask {
	width = utils.Clamp8(1, width, 100)
	height = utils.Clamp8(1, height, 100)
	return &SolidnessMask{width: width, height: height, cells: make([]int16, width * height)}
}

/**
 * Getters for bounds.
 */

func (solidnessMask *SolidnessMask) GetWidth() uint8 {
	return solidnessMask.width
}

func (solidnessMask *SolidnessMask) GetHeight() uint8 {
	return solidnessMask.height
}

/**
 * Performs an operation on each square while the callback evaluates to false. May return:
 * - false, error: if dimensions are invalid.
 * - false, nil: the operation ran on each cell without halting (i.e. callback returned always false)
 * - true, nil: the operation halted on a cell (i.e. callback returned true on a cell)
 */
func (solidnessMask *SolidnessMask) each(x, y, width, height uint8, callback func(uint8, uint8, uint16) bool) (bool, error) {
	err := ortho.CheckBounds(ortho.Bounds{
		ortho.Position{x, y}, ortho.Size{width, height},
	}, solidnessMask)
	if err != nil {
		return false, err
	}

	yEnd := y + height
	for j := y; j < yEnd; j++ {
		offset := uint16(j) * uint16(solidnessMask.width) + uint16(x)
		var i uint8;
		for i = 0; i < width; i++ {
			if callback(i, j, offset) {
				return true, nil
			}
		}
	}
	return false, nil
}

/**
 * Functions to increment, decrement, and test>0 cells. They all return an error if the dimensions
 *   are invalid for this map.
 */

func (solidnessMask *SolidnessMask) IncSquare(x, y, width, height uint8) error {
	var maxError error
	_, err := solidnessMask.each(x, y, width, height, func(x, y uint8, offset uint16) bool {
		if solidnessMask.cells[offset] == (1<<15 - 1) {
			maxError = CannotIncrement
			return true
		} else {
			solidnessMask.cells[offset]++
			return false
		}
	})
	if err != nil {
		return err
	} else {
		return maxError
	}
}

func (solidnessMask *SolidnessMask) DecSquare(x, y, width, height uint8) error {
	var minError error
	_, err := solidnessMask.each(x, y, width, height, func(x, y uint8, offset uint16) bool {
		if solidnessMask.cells[offset] == 0 {
			minError = CannotDecrement
			return true
		} else {
			solidnessMask.cells[offset]--
			return false
		}
	})
	if err != nil {
		return err
	} else {
		return minError
	}
}

func (solidnessMask *SolidnessMask) EmptySquare(x, y, width, height uint8) (bool, error) {
	res, err := solidnessMask.each(x, y, width, height, func(x, y uint8, offset uint16) bool {
		return solidnessMask.cells[offset] > 0
	})
	return !res, err
}

/**
 * Same functions but for rows/columns instead of squares.
 */

func (solidnessMask *SolidnessMask) IncRow(x, y, width uint8) error {
	return solidnessMask.IncSquare(x, y, width, 1)
}

func (solidnessMask *SolidnessMask) IncColumn(x, y, height uint8) error {
	return solidnessMask.IncSquare(x, y, 1, height)
}

func (solidnessMask *SolidnessMask) DecRow(x, y, width uint8) error {
	return solidnessMask.DecSquare(x, y, width, 1)
}

func (solidnessMask *SolidnessMask) DecColumn(x, y, height uint8) error {
	return solidnessMask.DecSquare(x, y, 1, height)
}

func (solidnessMask *SolidnessMask) EmptyRow(x, y, width uint8) (bool, error) {
	return solidnessMask.EmptySquare(x, y, width, 1)
}

func (solidnessMask *SolidnessMask) EmptyColumn(x, y, height uint8) (bool, error) {
	return solidnessMask.EmptySquare(x, y, 1, height)
}

/**
 * This one is to get a single position
 */
func (solidnessMask *SolidnessMask) GetAt(x, y uint8) int16 {
	return solidnessMask.cells[uint16(y) * uint16(solidnessMask.height) + uint16(x)]
}