package spec

import "errors"

/**
 * A tilemap has a reference to the tiles to use and the cells with data.
 *   Once instantiated, it is not meant to be modified.
 */
type Tilemap struct {
	tiles []*Tile
	width uint8
	height uint8
	cells []int
}

var InvalidTilemapXPosition = errors.New("invalid X position inside tilemap")
var InvalidTilemapYPosition = errors.New("invalid Y position inside tilemap")
var InvalidTilemapXRange = errors.New("initial and final X must satisfy: 0 <= xi <= xf < Map width")
var InvalidTilemapYRange = errors.New("initial and final Y must satisfy: 0 <= yi <= yf < Map width")

/**
 * Gets cells data.
 */
func (tilemap *Tilemap) getTileFromIndex(index int) *Tile {
	if index == -1 {
		return emptyTile
	} else {
		// When adding the tilemaps, the indexes are clamped.
		// This means we can be sure the indexes will be in
		//   -1, 0 ... length-1.
		var tile = tilemap.tiles[index]
		if tile == nil {
			return emptyTile
		} else {
			return tile
		}
	}
}

/**
 * Gets one single in-map tile index.
 */
func (tilemap *Tilemap) GetCellTileIndex(x, y uint8) (int, error) {
	if x >= tilemap.width {
		return 0, InvalidTilemapXPosition
	}
	if y >= tilemap.height {
		return 0, InvalidTilemapYPosition
	}
	return tilemap.cells[int16(y) * int16(tilemap.width) + int16(x)], nil
}

/**
 * Same, but returns directly the in-map tile.
 */
func (tilemap *Tilemap) GetCellTile(x, y uint8) (*Tile, error) {
	index, err := tilemap.GetCellTileIndex(x, y)
	if err != nil {
		return nil, err
	}
	return tilemap.getTileFromIndex(index), nil
}

/**
 * Gets a sub-map of indexes. This means: an array of integer positions which
 *   would be a flattened version of a cropped square of the tilemap.
 */
func (tilemap *Tilemap) GetCellTilesIndexes(xi, yi, xf, yf uint8) ([]int, error) {
	if xi > xf || xf > tilemap.width {
		return nil, InvalidTilemapXRange
	}
	if yi > yf || yf > tilemap.height {
		return nil, InvalidTilemapYRange
	}

	result := make([]int, int(xf - xi + 1) * int(yf - yi + 1))
	idx := 0
	for y := yi; y <= yf; y++ {
		for x := xi; x <= xf; x++ {
			result[idx] = tilemap.cells[int16(y) * int16(tilemap.width) + int16(x)]
			idx++
		}
	}
	return result, nil
}

/**
 * Same, but returns directly the in-map tiles.
 */
func (tilemap *Tilemap) GetCellTiles(xi, yi, xf, yf uint8) ([]*Tile, error) {
 	indexes, err := tilemap.GetCellTilesIndexes(xi, yi, xf, yf)
 	if err != nil {
 		return nil, err
	}
	result := make([]*Tile, len(indexes))
	for index, value := range indexes {
		result[index] = tilemap.getTileFromIndex(value)
	}
	return result, nil
}

/**
 * Returns the whole tileset, but transformed to tiles.
 */
func (tilemap *Tilemap) GetWholeCellTiles() []*Tile {
	result := make([]*Tile, len(tilemap.cells))
	for index, value := range tilemap.cells {
		result[index] = tilemap.getTileFromIndex(value)
	}
	return result
}