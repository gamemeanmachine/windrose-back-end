Windrose Spec objects
=====================

**Notes**: Spec objects are intended to be loaded and stored in database. They usually serve as a template.
This implies: no interaction will involve the objects under this `data/` subpackage.

Maps are our main world item. At least one of these must exist before any other object.

We create a map like this:

```
var myMapSpec WorldMapSpec, err error = NewWorldMapSpec("foo", nil, 30, 40)
```

And our map spec will be (width: 30, height> 40). The minimal map is 1x1, and each dimension may span up to 100.

The ID may be useful (here, it is "foo") and may be anything. This data will be up to you and, perhaps, the UI.

Your map MAY have, in its second parameter, an argument being a map string -> any. This map is intended to be used in the UI and/or custom server-side game logic.

Once a map spec is created, we need to add Tilemaps. Tilemaps are layers that stack over the spec.
Once a layer is added, it is considered immutable.

Each layer will have the same dimensions (so the provided tiles information must be an array of `width*height` tiles), and the last layer will perhaps override certain settings over the former layers.

Since the tilemaps will stack, the tiles will stack: They will be rendered in the same order in WindRose UI.

First, you need Tile instances to fill your map with (don't worry: an EMPTY tile is already provided).

```
var myTile = NewTile(nil, BLOCKING_OFF, nil)
```

The fields that matter here are (available as Get*() Methods):

1. ID: It will help the backend to, say, render the tile.
2. BlockingMode: Determines whether this tile blocks the walking of a movable object, if unblocks it, or if respects whatever was in the tile at the exact previous level (we already said tilemaps stack onto each other).
3. Settings: This metadata MAY be useful since it provides data from the tile that can be accessed in a custom game logic.

Now you can add your tilemap. Call this one:

```
var tilemap *Tilemap, err error = myMapSpec.AddTilemap([]*Tile{ myTile }, make([]int, 1200))
```

Arguments are tricky.

The first one is a slice of pointers to tiles. We only have one tile (Aside from the empty tile!).

The second one is a slice of indices. Indices must be between -1 and (len(firstArgument)-1), where -1 will stand for empty and other index will hit the slice ().  

1. The 0th element in this slice is the coordinate `(0, 0)`, the bottom-left corner.
2. The 29th element, the `(29, 0)`, the bottom-right corner.
3. The 1170th element, the `(0, 39)`, the top-left corner.
4. The 1199th element, the `(20, 39)`, the top-right corner.

Congratulations! You have your first map. You can query each Tilemap to see their tiles at specific/valid coordinates:

