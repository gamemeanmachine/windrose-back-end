package spec

import (
	"../../gmm/types"
	"../../gmm/settings"
)

type BlockingMode uint8

const (
	BLOCKING_ON = 1
	BLOCKING_OFF = 0
	BLOCKING_PREVIOUS = 2
)

/**
 * A tile keeps information of the cell, involving reaction to movement and
 *   front-end hinting. While the hinting is useful for user experience and
 *   client-side validation, the tile has more information to proceed with
 *   the true, server-side, validation.
 */
type Tile struct {
	/* Reference to the tile. This reference will only be used in front-end.
	   Appropriate step-blocking mechanism can be applied at front-end, and
	     appropriate rendering graphics, if selected the appropriate tile. */
	id types.ID
	/* Tells whether this tile blockingMode or not. This one is just a tri-state
	     flag. Blocking (1) disallows any movement to that cell, while
	     unblocking (0) allows it and previous-setting (2) respects the
	     same setting in the previous layer for the same tile. */
	blockingMode BlockingMode
	/* Perhaps it has more data. This is just in a per-game basis. */
	settings *settings.Settings
}

func NewTile(id types.ID, blocks BlockingMode, settings *settings.Settings) *Tile {
	return &Tile{id, blocks, settings}
}

/**
 * Getters for all the fields
 */

func (tile *Tile) GetID() types.ID {
	return tile.id
}

func (tile *Tile) GetBlockingMode() BlockingMode {
	return tile.blockingMode
}

func (tile *Tile) GetSettings() *settings.Settings {
	return tile.settings
}

/*  An empty tile which will serve the purpose of tile with index -1 */
var emptyTile = NewTile(types.ID(nil), BLOCKING_PREVIOUS, nil)