package spec

import (
	"errors"
	"../types"
	commonTypes "../../gmm/types"
	"../../gmm/settings"
)

type WorldMapSpec struct {
	id commonTypes.ID // Arbitrary ID
	settings *settings.Settings
	width uint8
	height uint8
	tilemaps []*Tilemap
	blockMaskCells []bool
	blockMask *types.BlockMask // Block mask for this map, as determined by tiles
}

var EmptyMapOrTiles = errors.New("tilemap tiles and indexes must be specified")
var TooBigMap = errors.New("map's width and height must both be between 1 and 100")

/**
 * Constructs a new world map with, perhaps certain metadata.
 */
func NewWorldMapSpec(id commonTypes.ID, settings *settings.Settings, width, height uint8) (*WorldMapSpec, error) {
	if width < 1 || width > 100 || height < 1 || height > 100 {
		return nil, TooBigMap
	}

	cells := make([]bool, int(width) * int(height))
	mask, _ := types.NewBlockMask(width, height, cells)

	return &WorldMapSpec{
		id, settings, width, height, []*Tilemap{nil, nil},
		cells, mask,
	}, nil
}

/**
 * Adds a tilemap to the world map. Returns this new tilemap.
 * This is just for a sort of instantiation or initialization.
 */
func (worldMap *WorldMapSpec) AddTilemap(tiles []*Tile, cells []int) (*Tilemap, error) {
	// null elements are not allowed
	if tiles == nil || cells == nil {
		return nil, EmptyMapOrTiles
	}

	// Bad indexes are converted to -1 (empty).
	// The new array matches in size, or is padded (with -1),
	//   or is capped to the actual bounds.
	newLength := int(worldMap.width) * int(worldMap.height)
	newCells := make([]int, newLength)
	numTiles := len(tiles)
	// Padding with -1 (empty)
	for index := range newCells {
		newCells[index] = -1
	}
	// Filling with tile index (if valid)
	for index, value := range cells {
		if index >= newLength {
			break
		}
		if value >= numTiles {
			continue
		}
		newCells[index] = value
		// If we set a value here, we should also set the blocking mask
		//   according to the tile setting. Ignored on nil or respecting
		//   blocking modes.
		tile := tiles[value]
		if tile == nil || tile.blockingMode == BLOCKING_PREVIOUS {
			continue
		} else if tile.blockingMode == BLOCKING_OFF {
			worldMap.blockMaskCells[index] = false
		} else if tile.blockingMode == BLOCKING_ON {
			worldMap.blockMaskCells[index] = true
		}
	}

	// Yes - in the end, a cell may be empty.
	// Either because being -1, or because its index references a nil tile.
	tilemap := &Tilemap{tiles, worldMap.width, worldMap.height, newCells}
	worldMap.tilemaps = append(worldMap.tilemaps, tilemap)
	return tilemap, nil
}

/**
 * Getters for some fields
 */

func (worldMapSpec *WorldMapSpec) GetID() commonTypes.ID {
	return worldMapSpec.id
}

func (worldMapSpec *WorldMapSpec) GetWidth() uint8 {
	return worldMapSpec.width
}

func (worldMapSpec *WorldMapSpec) GetHeight() uint8 {
	return worldMapSpec.width
}

func (worldMapSpec *WorldMapSpec) GetSettings() *settings.Settings {
	return worldMapSpec.settings
}

func (worldMapSpec *WorldMapSpec) GetBlockMask() *types.BlockMask {
	return worldMapSpec.blockMask
}

/**
 * An iterator over the tilemaps
 */
func (worldMapSpec *WorldMapSpec) EachTilemap(callback func(tilemap *Tilemap) bool) bool {
	for _, value := range worldMapSpec.tilemaps {
		if callback(value) {
			return true
		}
	}
	return false
}