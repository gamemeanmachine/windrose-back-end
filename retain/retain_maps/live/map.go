package live

import (
	"../spec"
	"../../gmm/ortho"
	"../types"
)

const MaxWidth uint8 = 100
const MaxHeight = MaxWidth

/**
 * Defines which methods satisfy this interface. By default we will have just a single type of
 *   world map handler, but that may change or be reimplemented with variations.
 *
 * While a world map handler performs the whole logic, this execution could be unsafe, so this
 *   class will not be directly accessed, but only from a secure context to avoid race conditions.
 */
type WorldMapHandler struct {
    spec *spec.WorldMapSpec
    solidnessMask *types.SolidnessMask
}

func (worldMapHandler *WorldMapHandler) Spec() *spec.WorldMapSpec {
	return worldMapHandler.spec
}

func (worldMapHandler *WorldMapHandler) IncrementBody(x, y, width, height uint8) error {
	return worldMapHandler.solidnessMask.IncSquare(x, y, width, height)
}

func (worldMapHandler *WorldMapHandler) DecrementBody(x, y, width, height uint8) error {
	return worldMapHandler.solidnessMask.DecSquare(x, y, width, height)
}

func (worldMapHandler *WorldMapHandler) IsHittingEdge(x, y, width, height uint8, direction ortho.Direction) bool {
	switch direction {
	case ortho.DIRECTION_UP:
		return y == 0
	case ortho.DIRECTION_DOWN:
		return y + height == worldMapHandler.solidnessMask.GetHeight()
	case ortho.DIRECTION_LEFT:
		return x == 0
	case ortho.DIRECTION_RIGHT:
		return x + width == worldMapHandler.solidnessMask.GetWidth()
	default:
		return false
	}
}

func (worldMapHandler *WorldMapHandler) IsAdjacencyBlocked(x, y, width, height uint8, direction ortho.Direction) (bool, error) {
	switch direction {
	case ortho.DIRECTION_UP:
		return worldMapHandler.Spec().GetBlockMask().BlockedRow(x, y + height, width)
	case ortho.DIRECTION_DOWN:
		return worldMapHandler.Spec().GetBlockMask().BlockedRow(x, y - 1, width)
	case ortho.DIRECTION_LEFT:
		return worldMapHandler.Spec().GetBlockMask().BlockedColumn(x - 1, y, height)
	case ortho.DIRECTION_RIGHT:
		return worldMapHandler.Spec().GetBlockMask().BlockedColumn(x + width, y, height)
	default:
		return false, nil
	}
}

func (worldMapHandler *WorldMapHandler) IsAdjacencyFree(x, y, width, height uint8, direction ortho.Direction) (bool, error) {
	switch direction {
	case ortho.DIRECTION_UP:
		return worldMapHandler.solidnessMask.EmptyRow(x, y + height, width)
	case ortho.DIRECTION_DOWN:
		return worldMapHandler.solidnessMask.EmptyRow(x, y - 1, width)
	case ortho.DIRECTION_LEFT:
		return worldMapHandler.solidnessMask.EmptyColumn(x - 1, y, height)
	case ortho.DIRECTION_RIGHT:
		return worldMapHandler.solidnessMask.EmptyColumn(x + width, y, height)
	default:
		return false, nil
	}
}

func (worldMapHandler *WorldMapHandler) IncrementAdjacent(x, y, width, height uint8, direction ortho.Direction) error {
	switch direction {
	case ortho.DIRECTION_UP:
		return worldMapHandler.solidnessMask.IncRow(x, y + height, width)
	case ortho.DIRECTION_DOWN:
		return worldMapHandler.solidnessMask.IncRow(x, y - 1, width)
	case ortho.DIRECTION_LEFT:
		return worldMapHandler.solidnessMask.IncColumn(x - 1, y, height)
	case ortho.DIRECTION_RIGHT:
		return worldMapHandler.solidnessMask.IncColumn(x + width, y, height)
	default:
		return nil
	}
}

func (worldMapHandler *WorldMapHandler) DecrementAdjacent(x, y, width, height uint8, direction ortho.Direction) error {
	switch direction {
	case ortho.DIRECTION_UP:
		return worldMapHandler.solidnessMask.DecRow(x, y + height, width)
	case ortho.DIRECTION_DOWN:
		return worldMapHandler.solidnessMask.DecRow(x, y - 1, width)
	case ortho.DIRECTION_LEFT:
		return worldMapHandler.solidnessMask.DecColumn(x - 1, y, height)
	case ortho.DIRECTION_RIGHT:
		return worldMapHandler.solidnessMask.DecColumn(x + width, y, height)
	default:
		return nil
	}
}

/**
 * This struct provides the context to execute code on a world map handler. The handler will
 *   be accessed only when a secure context (i.e. a channel being locked) is provided for it.
 */
type WorldMap struct {
	handler *WorldMapHandler
	mutex   chan bool
}

func (worldMap *WorldMap) Run(callback func(*WorldMapHandler) interface{}) interface{} {
	worldMap.mutex <- true
	defer (func(){ <- worldMap.mutex })()
	return callback(worldMap.handler)
}

func (worldMap *WorldMap) GetWidth() uint8 {
	return worldMap.handler.Spec().GetWidth()
}

func (worldMap *WorldMap) GetHeight() uint8 {
	return worldMap.handler.Spec().GetHeight()
}

func (worldMap *WorldMap) GetSize() (uint8, uint8) {
	mapSpec := worldMap.handler.Spec()
	return mapSpec.GetWidth(), mapSpec.GetHeight()
}

/**
 * Returning a wrapper to create a map based on a particular handler
 */
func NewWorldMap(spec *spec.WorldMapSpec) *WorldMap {
	return &WorldMap{
		handler: &WorldMapHandler{
			spec: spec,
			solidnessMask: types.NewSolidnessMask(spec.GetWidth(), spec.GetHeight()),
        },
		mutex: make(chan bool, 1),
    }
}