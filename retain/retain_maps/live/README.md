WindRose Live Maps
===========================

In contrast to data objects, which simply hold the data and are pretty much static, these live objects will implement
the so-called Commanded and Serializable.