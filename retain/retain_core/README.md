Serializable elements
=====================

Quite often, changes in objects need to be notified to clients/UI.

A `SerializableNotificator` is an object satisfying contract to notify when an object is added, removed, updated, or executing a custom action.

A `Serializable` is an object satisfying contract to get the attributes, and the notificator it is bound to.

One scheme of using the notificators and serializables is like this:

```
action ---> Character1 ---> Notificator for character1 ---> Socket for character1
                                                       \
                                                        \
                                                         \
                                                          -> Global notificator ---> Nearby characters' sockets
                                                         /                      \
                                                        /                       \
                                                       /                        \> Administrators' channel
action ---> Character2 ---> Notificator for character2 ---> Socket for character2
```

On the other side, `Serializable` objects may (& will) invoke the utility functions like:

```
NotifyAdded(serializable)
NotifyRemoved(serializable)
NotifyUpdated(serializable, attribute string, value any)
NotifyActed(serializable, action string, params ...any)
```

For this to work, `Serializable` objects must implement the following methods:

```
ID() any : immutable value that will work as the object's ID. Any value can fit here.
Notificator() SerializableNotificator : returns the notificator it is bound to. it must be not null.
Notifies() bool : tells whether this object has the notifications feature enabled or not. Most likely this will always be true.
                  We must ensure that, while this method returns true, the Notificator() method returns a non-null value.
```

These functions will be called most likely at the end of the implementation of relevant methods. As an example, administrators could receive the `NotifyUpdated` notifications while the users (and administrators) could get the other notifications.

By no means this is an extensive implementation (that's why these are interfaces instead of concrete classes).

Concrete classes for these interfaces will be implemented later.

Commanded elements
==================

`Commanded` elements are the counterpart of `Serializable` elements. In this case, it is the clients/UI who send information that will be reflected on the commanded objects.

First we have a `CommandReceiver` that registers and unregisters serializable objects and dispatches actions to those objects.

Being this the inverse path, a possible scheme will work like this:

```
client action for char1 ---> socket for char1 (which has somehow the char1's ID or commanded object bound) ---> picking ID 1, parsing action/arguments & dispatching -> Invoking registered command on char1
client action for char2 ---> socket for char2 (which has somehow the char2's ID or commanded object bound) ---> picking ID 2, parsing action/arguments & dispatching -> Invoking registered command on char2
```

For this you will need to make use of a Pipeline, like this:

```
pipeline := NewCommandPipeline(
    map[string]CommandParser{
       "paramString": SampleParamStringParser{},
       "json": SampleJSONParser{},
       ...
    },
    aParserDetector,
    SampleDispatcher{},
)

# SampleParamStringParser and SampleJSONParser implement CommandParser
# SampleDispatcher implements CommandDispatcher
#   and the return values of Dispatch must implement CommandRegistrar
```

Many elements have to be described here:

1. `CommandParser` defines `ParseCommand` that takes a string and parses it, returning the action name to be triggered, and its arguments.
2. `CommandDispatcher` defines `Dispatch` taking an id (anything), an action, and a parser used beforehand, and returns the appropriate object (implementing CommandRegistrar) to look for that given id. Implement it at your own taste.
3. `CommandRegistrar` defined `Register(commandable)`, `Unregister(commandable)`, and `Find(id)` to handle its registered objects.

Once you have this all, then you can execute an input command with:

1. `pipeline.Execute("USER1", "do-this foo bar baz")`
2. `pipeline.ExecuteUsing("USER1", "do-this foo bar baz", "paramString")`

Remember: commanded objects should register into an instance of `CommandRegistrar` somehow, and unregister when done. This involves self-management of their lifecycle.

You will need to catch possible errors (non-existing id, bad command, unsuitable registry or parser, ...). Otherwise, the command will run appropriately.

Perhaps you'd like to tie one of these last two calls, to a socket (or another type of endpoint) read event or alike.