package dao

import (
	"sync"
	"../ecs"
)

/*********************************************************
 * Persisted base class
 *********************************************************/

/**
 * Provides the base implementation of the BasePersisted
 *   interface. It will define all its methods completely,
 *   but the user/developer MAY redefine any of the methods.
 * EXCEPT for the IsCompatible(DAO) method. This must be
 *   redefined because its current implementation returns
 *   false constantly.
 */
type BasePersisted struct {
	id ID
	dao *DAO
}

/**
 * Getting the ID.
 */
func (basePersisted *BasePersisted) GetID() ID {
	return basePersisted.id
}

/**
 * Getting the DAO.
 */
func (basePersisted *BasePersisted) GetDAO() *DAO {
	return basePersisted.dao
}

/**
 * Setting the ID.
 */
func (basePersisted *BasePersisted) setID(id ID) {
	basePersisted.id = id
}

/**
 * Setting the DAO.
 */
func (basePersisted *BasePersisted) setDAO(dao *DAO) {
	basePersisted.dao = dao
}

/**
 * Saves the current persistable by delegating to the DAO.
 */
func (basePersisted *BasePersisted) Save() error {
	if basePersisted.dao == nil {
		return DAONotAssigned
	} else {
		return basePersisted.dao.Save(basePersisted)
	}
}

/**
 * Deletes the current persistable by delegating to the DAO.
 */
func (basePersisted *BasePersisted) Delete() error {
	if basePersisted.dao == nil {
		return DAONotAssigned
	} else {
		return basePersisted.dao.Delete(basePersisted)
	}
}

/**
 * Instantiation of this object into a live entity is
 *   something you really have to implement. This method
 *   would never be called directly, however: it will be
 *   invoked by Instantiate(Persisted) utility.
 */
func (basePersisted *BasePersisted) Inflate(*ecs.Entity) {
	panic("Persisted `Inflate() *ecs.Entity` method must be implemented")
}

/*********************************************************
 * DAO engine base class
 *********************************************************/

/**
 * This engine will be mostly abstract, in contrast to
 *   the persisted object, on which you only need to
 *   add their properties. Here you will have to define
 *   all these methods (by default they raise a panic).
 * The engine will not be used directly, but instead will
 *   through its wrapper: DAO.
 */
type DAOEngineBase struct {
	wrapper *DAO
}

/**
 * This method will be called here, in this package. It is
 *   useful for us to seal the interface.
 */
func (daoEngineBase *DAOEngineBase) onWrapped(dao *DAO) {
	daoEngineBase.wrapper = dao
}

/**
 * Tells whether this engine is appropriately wrapped, by
 *   returning its DAO.
 */
func (daoEngineBase *DAOEngineBase) GetWrapper() *DAO {
	return daoEngineBase.wrapper
}

/**
 * This method will be pre-implemented as false, always.
 *   This, just to be in contrast to the implementation
 *   in the BasePersisted class.
 */
func (daoEngineBase* DAOEngineBase) IsCompatible(Persisted) bool {
	return false
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase* DAOEngineBase) New() Persisted {
	panic("DAOEngine `New() Persisted` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) Load(ID) (Persisted, error) {
	panic("DAOEngine `Load(ID) (Persisted, error)` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) Save(Persisted) error {
	panic("DAOEngine `Save(Persisted) error` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) LoadAtMost([]ID) ([]Persisted, map[ID]error) {
	panic("DAOEngine `LoadAtMost([]ID) ([]Persisted, map[ID]error)` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) LoadAll([]ID) ([]Persisted, error) {
	panic("DAOEngine `LoadAll([]ID) ([]Persisted, error)` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) Delete(Persisted) error {
	panic("DAOEngine `Delete(Persisted) error` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) DeleteByID(ID) error {
	panic("DAOEngine `DeleteByID(ID) error` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) DeleteAtMost([]Persisted) (map[Persisted]error) {
	panic("DAOEngine `DeleteAtMost([]Persisted) (map[ID]error)` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) DeleteByIDAtMost([]ID) (map[ID]error) {
	panic("DAOEngine `DeleteByIDAtMost([]ID) (map[ID]error)` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) DeleteAll([]Persisted) error {
	panic("DAOEngine `DeleteAll([]Persisted) error` method must be implemented")
}

/**
 * This one needs to be implemented.
 */
func (daoEngineBase *DAOEngineBase) DeleteByIDAll([]ID) error {
	panic("DAOEngine `DeleteByIDAll([]ID) error` method must be implemented")
}

/*********************************************************
 * DAO cache (internal) class
 *********************************************************/

/**
 * List of persistent elements in the DAO.
 */
type daoPersistedCache struct {
	/**
	 * All elements in session, regardless whether they have
	 *   an ID or not.
	 */
	allElements map[Persisted]struct{}
	/**
	 * Elements by ID. It is guaranteed that elements here
	 *   are also in allElements.
	 */
	elementsByID map[interface{}]Persisted
}

/**
 * Adds an entity to the list
 */
func (daoPersistedCache *daoPersistedCache) add(persisted Persisted) {
	daoPersistedCache.allElements[persisted] = struct{}{}
	if persisted.GetID() != nil {
		daoPersistedCache.addToIDsMap(persisted)
	}
}

/**
 * Removes an entity from the list
 */
func (daoPersistedCache *daoPersistedCache) remove(persisted Persisted) {
	if persisted.GetID() != nil {
		daoPersistedCache.removeFromIDsMap(persisted)
	}
	delete(daoPersistedCache.allElements, persisted)
}

/**
 * Adds an entity to the IDs map
 */
func (daoPersistedCache *daoPersistedCache) addToIDsMap(persisted Persisted) {
	daoPersistedCache.elementsByID[persisted.GetID()] = persisted
}

/**
 * Removes an entity from the IDs map
 */
func (daoPersistedCache *daoPersistedCache) removeFromIDsMap(persisted Persisted) {
	delete(daoPersistedCache.elementsByID, persisted.GetID())
}

/*********************************************************
 * DAO class
 *********************************************************/

/**
 * This DAO is the wrapper of DAOEngine objects. It will
 *   call the underlying engine's methods and cache their
 *   results (entities management).
 */
type DAO struct {
	engine DAOEngine
	cache *daoPersistedCache
	mutex sync.RWMutex
}

/**
 * Get its engine.
 */
func (dao *DAO) GetEngine() DAOEngine {
	return dao.engine
}

/**
 * Create a new DAO by wrapping an instance.
 */
func NewDAO(engine DAOEngine) (*DAO, error) {
	if engine.GetWrapper() != nil {
		return nil, DAOAlreadyWrapped
	} else {
		dao := &DAO{
			engine: engine,
			cache: &daoPersistedCache{
				allElements: map[Persisted]struct{}{},
				elementsByID: map[interface{}]Persisted{},
			},
		}
		engine.onWrapped(dao)
		return dao, nil
	}
}

/**
 * Tells whether an object is compatible to this DAO or not.
 */
func (dao *DAO) isCompatible(persisted Persisted) bool {
	return dao.engine.IsCompatible(persisted)
}

/**
 * Creates a new instance (not saving to DB). This method is thread-safe.
 */
func (dao *DAO) New() (Persisted, error) {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	persisted := dao.engine.New()
	if persisted == nil {
		return nil, ObjectIsNull
	}

	err := dao.bind(persisted)
	if err != nil {
		return nil, err
	} else {
		return persisted, nil
	}
}

/**
 * Bind a persisted into a dao, provided the persisted does not
 *   have a dao set, and the dao is compatible to the persisted.
 */
func (dao *DAO) Bind(persisted Persisted) error {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	if persisted == nil {
		return ObjectIsNull
	}
	if persisted.GetDAO() != nil {
		return DAOAlreadyAssigned
	}

	return dao.bind(persisted)
}

/**
 * The actual binding operation is implemented here.
 */
func (dao *DAO) bind(persisted Persisted) error {
	if dao.isCompatible(persisted) {
		persisted.setDAO(dao)
		dao.cache.add(persisted)
		return nil
	} else {
		return DAONotCompatible
	}
}

/**
 * Evicts a persisted from the DAO. This is the counterpart of Bind().
 * The instance being evicted will lose its dao reference so we make
 *   sure it will not be able to perform operations anymore. Evicting
 *   an object also clears its cache.
 *
 * An evicted persisted object is the candidate for GC. You should
 *   always evict your objects when you don't need them anymore in
 *   your scope.
 */
func (dao *DAO) Evict(persisted Persisted) error {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	if persisted == nil {
		return ObjectIsNull
	}
	if persisted.GetDAO() == nil {
		return DAONotAssigned
	}
	if persisted.GetDAO() != dao {
		return DifferentDAOAssigned
	}

	dao.cache.remove(persisted)
	persisted.setDAO(nil)
	return nil
}

/**
 * Saves the instance and adds it to the appropriate cache after it gains
 *   an ID. This method is thread-safe.
 */
func (dao *DAO) Save(persisted Persisted) error {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	if persisted == nil {
		return ObjectIsNull
	}
	if persisted.GetDAO() == nil {
		return DAONotAssigned
	}
	if persisted.GetDAO() != dao {
		return DifferentDAOAssigned
	}

	id, err := dao.engine.Save(persisted)
	if err != nil {
		return err
	} else {
		persisted.setID(id)
		dao.cache.addToIDsMap(persisted)
		return nil
	}
}

/**
 * Loads an instance, or gets a cached one.
 */
func (dao *DAO) Load(id ID) (Persisted, error) {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	persisted, ok := dao.cache.elementsByID[id]
	if ok {
		return persisted, nil
	} else {
		persisted, err := dao.engine.Load(id)
		if err != nil {
			return nil, err
		} else {
			dao.bind(persisted)
			return persisted, nil
		}
	}
}

/**
 * Returns the IDs from the given list that are not present
 *  among cached ones.
 */
func (dao *DAO) splitCached(ids []ID) ([]Persisted, []ID) {
	uncached := make([]ID, 64)
	cached := make([]Persisted, 64)
	for _, id := range ids {
		persisted, ok := dao.cache.elementsByID[id]
		if !ok {
			uncached = append(uncached, id)
		} else {
			cached = append(cached, persisted)
		}
	}
	return cached, uncached
}

/**
 * Copies the selected persisted elements.
 */
func copyPersisted(cached []Persisted) []Persisted {
	result := make([]Persisted, len(cached))
	copy(result, cached)
	return result
}

/**
 * Loads at most the given instances, or gets their cached ones.
 * For each non-cached entry that was failed to load, an error
 *   will be returned.
 */
func (dao *DAO) LoadAtMost(ids []ID) ([]Persisted, map[ID]error) {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	cached, uncachedIds := dao.splitCached(ids)
	result := copyPersisted(cached)
	errorsMap := map[ID]error(nil)
	if len(uncachedIds) > 0 {
		var retrieved []Persisted = nil
		retrieved, errorsMap = dao.engine.LoadAtMost(uncachedIds)
		result = make([]Persisted, 64)
		for _, ret := range retrieved {
			result = append(result, ret)
		}
	}
	return result, errorsMap
}

/**
 * Loads all the given instances, or gets their cached ones.
 * For each non-cached entry that was failed to load, if an
 *   error is triggered, nothing is returned but the error.
 */
func (dao *DAO) LoadAll(ids []ID) ([]Persisted, error) {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	cached, uncachedIds := dao.splitCached(ids)
	var result []Persisted = nil
	if len(uncachedIds) > 0 {
		var retrieved []Persisted = nil
		var err error = nil
		retrieved, err = dao.engine.LoadAll(uncachedIds)
		if err != nil {
			return nil, err
		} else {
			result = copyPersisted(cached)
			for _, ret := range retrieved {
				result = append(result, ret)
			}
			return result, nil
		}
	} else {
		return copyPersisted(cached), nil
	}
}

/**
 * Removes and entity from IDs cache and cleans its ID
 */
func (dao *DAO) cleanID(persisted Persisted) {
	dao.cache.removeFromIDsMap(persisted)
	persisted.setID(nil)
}

/**
 * Deletes the instance, removes its ID, but retains its dao reference.
 * It also removes the entity from the cache.
 */
func (dao *DAO) Delete(persisted Persisted) error {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	if persisted == nil {
		return ObjectIsNull
	}
	if persisted.GetDAO() == nil {
		return DAONotAssigned
	}
	if persisted.GetDAO() != dao {
		return DifferentDAOAssigned
	}
	if persisted.GetID() == nil {
		return nil
	}

	err := dao.engine.Delete(persisted)
	if err != nil {
		return err
	} else {
		dao.cleanID(persisted)
		return nil
	}
}

/**
 * Deletes the instance by its ID. If it was successful, then it
 *   clears the current instance from cache, if present (just by
 *   removing its ID).
 */
func (dao *DAO) DeleteById(id ID) error {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	err := dao.engine.DeleteByID(id)
	if err != nil {
		return err
	} else {
		persisted, ok := dao.cache.elementsByID[id]
		if ok {
			dao.cleanID(persisted)
		}
		return nil
	}
}

func (dao *DAO) splitPersistedObjectsOutOfThisDAO(persistedElements []Persisted) (map[Persisted]error, []Persisted) {
	outOfDao := make(map[Persisted]error)
	inDao := make([]Persisted, 64)
	for _, persisted := range persistedElements {
		dao2 := persisted.GetDAO()
		if dao2 == nil {
			outOfDao[persisted] = DAONotAssigned
		} else if dao2 != dao {
			outOfDao[persisted] = DifferentDAOAssigned
		} else {
			inDao = append(inDao, persisted)
		}
	}
	return outOfDao, inDao
}

/**
 * Deletes the instances. Returns errors for instances being bound
 *   to a different DAO, and for instances not being bound to any
 *   DAO. It also returns errors according to what the engine
 *   considered. Returned errors are Persisted => (a single error).
 */
func (dao *DAO) DeleteAtMost(persistedElements []Persisted) (map[Persisted]error) {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	// No-op on zero
	if len(persistedElements) == 0 {
		return map[Persisted]error{}
	}
	// We will have, initially, not-this-dao-errors
	errors, inDao := dao.splitPersistedObjectsOutOfThisDAO(persistedElements)
	// We only keep the elements with id
	inDaoWithID := make([]Persisted, 16)
	for _, persisted := range inDao {
		if persisted.GetID() != nil {
			inDaoWithID = append(inDaoWithID, persisted)
		}
	}
	// We will then try deleting more elements, and will perhaps
	//   get more errors when deleting.
	for key, err := range dao.engine.DeleteAtMost(inDaoWithID) {
		errors[key] = err
	}
	// For each object not among errors (i.e. successfully deleted)
	//   we remove them from cache.
	for _, persisted := range persistedElements {
		_, ok := errors[persisted]
		if !ok {
			dao.cleanID(persisted)
		}
	}
	// End
	return errors
}

/**
 * Deletes the given ids. Returns errors according to what
 *   the engine considered. Returned errors are id => (a single error).
 */
func (dao *DAO) DeleteByIDAtMost(ids []ID) (map[ID]error) {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	// No-op on zero
	if len(ids) == 0 {
		return map[ID]error{}
	}
	errors := map[ID]error{}
	// We will then try deleting more elements, and will perhaps
	//   get more errors when deleting.
	for key, err := range dao.engine.DeleteByIDAtMost(ids) {
		errors[key] = err
	}
	// For each id not among errors (i.e. successfully deleted)
	//   we remove id from cache.
	for _, id := range ids {
		_, ok := errors[id]
		if !ok {
			delete(dao.cache.elementsByID, id)
		}
	}
	// End
	return errors
}

/**
 * Deletes the given instances. If at least one failed, this
 *   operation should rollback and return a single error.
 */
func (dao *DAO) DeleteAll(persistedElements []Persisted) error {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	// No-op on zero
	if len(persistedElements) == 0 {
		return nil
	}
	// We will have, initially, not-this-dao-errors
	errors, _ := dao.splitPersistedObjectsOutOfThisDAO(persistedElements)
	if len(errors) > 0 {
		return NotThisDAO
	}
	// We only keep the elements with id
	persistedElementsWithID := make([]Persisted, 16)
	for _, persisted := range persistedElements {
		if persisted.GetID() != nil {
			persistedElementsWithID = append(persistedElementsWithID, persisted)
		}
	}
	// Now we try to delete all the elements with ID
	err := dao.engine.DeleteAll(persistedElementsWithID)
	if err != nil {
		return err
	} else {
		for _, persisted := range persistedElementsWithID {
			dao.cleanID(persisted)
		}
		return nil
	}
}

/**
 * Deletes the given ids. If at least one failed, this operation
 *   should rollback and return a single error.
 */
func (dao *DAO) DeleteByIDAll(ids []ID) error {
	dao.mutex.Lock()
	defer dao.mutex.Unlock()

	// No-op on zero
	if len(ids) == 0 {
		return nil
	}
	// Now we try to delete all the elements with ID
	err := dao.engine.DeleteByIDAll(ids)
	if err != nil {
		return err
	} else {
		for _, id := range ids {
			delete(dao.cache.elementsByID, id)
		}
		return nil
	}
}