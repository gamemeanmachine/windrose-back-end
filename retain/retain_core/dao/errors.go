package dao

import "errors"

/**
 * Available errors.
 */
var DAONotCompatible = errors.New("the DAO is not compatible with the persisted object")
var DAONotAssigned = errors.New("no DAO is assigned to this persisted object")
var DAOAlreadyAssigned = errors.New("this object is already bound to a dao")
var DifferentDAOAssigned = errors.New("a different DAO is assigned to this persisted object")
var DAOAlreadyWrapped = errors.New("this DAO is already wrapped")
var DoesNotExist = errors.New("object does not exist in store")
var ObjectIsNull = errors.New("object must not be null")
var NotThisDAO = errors.New("at least one object does not belong to this dao")