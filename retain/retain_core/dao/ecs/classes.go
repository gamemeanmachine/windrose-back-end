package ecs

import (
	"../../../../../../../usr/local/Cellar/go/1.12.1/libexec"
	baseECS "../../ecs"
)

/*********************************************************
 * ECS interaction
 *********************************************************/

/**
 * A persisted component is a component able to get the
 *   access to its related persisted object
 */
type PersistedComponent struct {
	*baseECS.BaseComponent
	persisted dao.Persisted
}

func (persistedComponent *PersistedComponent) GetPersisted() dao.Persisted {
	return persistedComponent.persisted
}

/**
 * Instantiates an Entity by adding a PersistedComponent
 *   and also inflating according to what the persistent
 *   can do.
 */
func Instantiate(persisted dao.Persisted) *baseECS.Entity {
	if persisted == nil {
		return nil
	}

	entity := baseECS.NewEntity()
	persistedComponent := &PersistedComponent{&baseECS.BaseComponent{}, persisted}
	entity.AddComponent(persistedComponent)
	persisted.Inflate(entity)
	return entity
}
