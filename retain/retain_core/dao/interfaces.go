package dao

import "../ecs"

/*********************************************************
 * Persisted interface
 *********************************************************/

/**
 * This sealed interface is only related
 *   to their ID and the DAO they were
 *   fetched from.
 */
type Persisted interface {
	/**
	 * Private setters, and public getters, for the
	 *   DAO and the ID.
	 */
	setDAO(*DAO)
	setID(ID)
	GetDAO() *DAO
	GetID() ID
	/**
	 * Saves the current instance. It will trigger the
	 *   creation or update, depending on the ID being
	 *   absent or present, respectively. Invokes its
	 *   equivalent in the DAO.
	 */
	Save() error
	/**
	 * Deletes the current persisted instance. Invokes
	 *   its equivalent in the DAO.
	 */
	Delete() error
	/**
	 * Inflates an in-game Entity. You will never call
	 *   this method directly (see notes in BasePersisted).
	 */
	Inflate(*ecs.Entity)
}

/*********************************************************
 * DAO engine interface
 *********************************************************/

/**
 * This sealed interface is the base for DAO engines.
 * DAO engines will load and populate objects in and
 *   from database. However this class will be wrapped
 *   inside a DAO.
 */
type DAOEngine interface {
	/**
	 * This method is private and will only be
	 *   called when the DAO is wrapped in this
	 *   package (wrapped by a proper DAO).
	 */
	onWrapped(dao *DAO)
	/**
	 * This method tests whether this DAO engine
	 *   was appropriately wrapped and returns the
	 *   wrapper.
	 */
	GetWrapper() *DAO
	/**
	 * Determines whether the given persisted
	 *   instance (or template) is compatible
	 *   with the current DAO or not. This
	 *   method will be invoked for the instance
	 *   returned by the factory in New().
	 */
	IsCompatible(Persisted) bool
	/**
	 * An abstract method to instantiate a new
	 *   Persisted object. It will have no ID and
	 *   no ties to any dao at the point this
	 *   method returns.
	 */
	New() Persisted
	/**
	 * Loads an object from its store. Each store
	 *   must define appropriately which errors
	 *   does it handle, and how. By default, the
	 *   DoesNotExist error is available for missing
	 *   IDs.
	 */
	Load(ID) (Persisted, error)
	/**
	 * Saves the object in its store. Each store must
	 *   define appropriately which errors does it
	 *   handle, and how. By default, the DoesNotExist
	 *   error is available for missing IDs when
	 *   trying to update.
	 * An attempt to create should be done if the ID
	 *   is null, while an attempt to update should
	 *   be done if the ID is not null.
	 * It returns the ID of the stored object, being
	 *   created in the store or updated.
	 */
	Save(Persisted) (ID, error)
	/**
	 * Loads at most the objects by their given IDs.
	 * The failing IDs will be added to a map of
	 *   ID => error in the return value.
	 */
	LoadAtMost([]ID) ([]Persisted, map[ID]error)
	/**
	 * Tries loading all the objects specified by
	 *   their IDs. This one is an All-or-nothing
	 *   operation, meaning that no entity will be
	 *   returned if an error occurs, but instead
	 *   the error will be returned.
	 */
	LoadAll([]ID) ([]Persisted, error)
	/**
	 * Deletes the persistent elements (or elements
	 *   by IDs, respectively).
	 */
	Delete(Persisted) error
	DeleteByID(ID) error
	DeleteAtMost([]Persisted) (map[Persisted]error)
	DeleteByIDAtMost([]ID) (map[ID]error)
	DeleteAll([]Persisted) error
	DeleteByIDAll([]ID) error
}
