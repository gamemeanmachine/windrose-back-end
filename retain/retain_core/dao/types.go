package dao

/**
 * Anything will satisfy the ID field.
 * Most frequently, an integer or string.
 */
type ID interface{}
