package ortho

import "../utils"

/**
 * Due to its small size, and the fact it will be
 *   constant, this class' methods belong to the
 *   instance instead of the pointer. This will
 *   make it more lightweight than pointers.
 *
 * This will happen to all these struct.
 */

type Position struct {
	x, y uint8
}

type Size struct {
	width, height uint8
}

type Bounds struct {
	Located
	Sized
}

func (size Size) GetWidth() uint8 {
	return size.width
}

func (size Size) GetHeight() uint8 {
	return size.height
}

func (size Size) Clamp() Size {
	return Size{utils.Clamp8(MinWidth, size.width, MaxWidth),
                utils.Clamp8(MinHeight, size.height, MaxHeight)}
}

func SizeFrom(sized Sized) Size {
	return Size{sized.GetWidth(), sized.GetHeight()}
}

func (position Position) GetX() uint8 {
	return position.x
}

func (position Position) GetY() uint8 {
	return position.y
}

func PositionFrom(located Located) Position {
	return Position{located.GetX(), located.GetY()}
}

func BoundsFrom(located Located, sized Sized) Bounds {
	return Bounds{PositionFrom(located), SizeFrom(sized)}
}

/**
 * Direction is orthogonal here. You will have normal directions like:
 *   LEFT, TOP, RIGHT, BOTTOM
 */
type Direction int8
const (
	DIRECTION_NONE Direction = -1
	DIRECTION_DOWN Direction = iota
	DIRECTION_LEFT
	DIRECTION_RIGHT
	DIRECTION_UP
	DIRECTION_FRONT = DIRECTION_DOWN
)

func (direction Direction) Empty() bool {
	return direction == DIRECTION_NONE
}

func (direction Direction) Opposite() Direction {
	switch direction {
	case DIRECTION_DOWN:
		return DIRECTION_UP
	case DIRECTION_UP:
		return DIRECTION_DOWN
	case DIRECTION_LEFT:
		return DIRECTION_RIGHT
	case DIRECTION_RIGHT:
		return DIRECTION_LEFT
	}
	// this one should never happen
	return DIRECTION_NONE
}