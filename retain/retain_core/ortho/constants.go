package ortho

const (
	MaxWidth uint8 = 100
	MaxHeight uint8 = 100
	MinWidth uint8 = 1
	MinHeight uint8 = 1
)