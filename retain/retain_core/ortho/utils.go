package ortho

/**
 * Checks whether a bounding box fits inside a certain size.
 */
func CheckBounds(bounded Bounded, sized Sized) error {
	if bounded.GetWidth() > sized.GetWidth() || bounded.GetHeight() > sized.GetWidth() {
		return OutOfBounds
	}

	if bounded.GetX() > sized.GetWidth() - bounded.GetWidth() || bounded.GetY() > bounded.GetHeight() - bounded.GetHeight() {
		return OutOfBounds
	}

	return nil
}

