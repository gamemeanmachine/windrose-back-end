package ortho

import "errors"

var OutOfBounds = errors.New("cannot fit object's position/dimensions inside given bounds")