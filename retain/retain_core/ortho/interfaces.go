package ortho

type Located interface {
	GetX() uint8
	GetY() uint8
}

type Sized interface {
	GetWidth() uint8
	GetHeight() uint8
}

type Bounded interface {
	Located
	Sized
}
