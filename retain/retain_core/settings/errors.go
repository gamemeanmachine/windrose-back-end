package settings

import "errors"

var CannotModifyLockedKey = errors.New("cannot modify a locked key")