package settings

type Settings struct {
	lockedKeys []string
	innerMap map[string]interface{}
}

func NewSettings(innerMap map[string]interface{}, lockedKeys ...string) *Settings {
	var clonedMap = make(map[string]interface{})
	for key, value := range innerMap {
		clonedMap[key] = value
	}
	return &Settings{innerMap: clonedMap, lockedKeys: lockedKeys}
}

func isLocked(key string, lockedKeys []string) bool {
	for _, key2 := range lockedKeys {
		if key2 == key {
			return true
		}
	}
	return false
}

func (settings *Settings) Put(key string, value interface{}) error {
	if isLocked(key, settings.lockedKeys) {
		return CannotModifyLockedKey
	} else {
		settings.innerMap[key] = value
		return nil
	}
}

func (settings *Settings) Del(key string) error {
	if isLocked(key, settings.lockedKeys) {
		return CannotModifyLockedKey
	} else {
		delete(settings.innerMap, key)
		return nil
	}
}

func (settings *Settings) Get(key string) interface{} {
	value, _ := settings.innerMap[key]
	return value
}

func (settings *Settings) Slice(keys ...string) []interface{} {
	var results = make([]interface{}, cap(keys))
	for index, key := range keys {
		results[index], _ = settings.innerMap[key]
	}
	return results
}

func (settings *Settings) Clone() *Settings {
	return NewSettings(settings.innerMap, settings.lockedKeys...)
}

func (settings *Settings) Each(callback func(key string, value interface{}) bool) bool {
	for key, value := range settings.innerMap {
		if callback(key, value) {
			return true
		}
	}
	return false
}