package types

/**
 * Solidness applies to positionable objects and means:
 * - GHOST: this object can move through anything.
 *          other -solid- objects can move through this.
 * - HOLE: like ghost, but with an additional negative effect.
 *         a position occupied by a hole and a solid (or for-others)
 *           object will count as empty.
 * - FOR_OTHERS: this object can move through anything.
 *               other -solid- objects cannot move through this.
 * - SOLID: this object cannot move through SOLID or FOR_OTHERS objects.
 */
type Solidness int8
const (
	SOLIDNESS_NONE Solidness = -1
	SOLIDNESS_HOLE Solidness = iota
	SOLIDNESS_GHOST
	SOLIDNESS_FOR_OTHERS
	SOLIDNESS_SOLID
)

func (solidness Solidness) Empty() bool {
	return solidness == SOLIDNESS_NONE
}

func (solidness Solidness) Traverses() bool {
	return solidness != SOLIDNESS_SOLID
}

func (solidness Solidness) Carves() bool {
	return solidness == SOLIDNESS_HOLE
}

func (solidness Solidness) Occupies() bool {
	return solidness == SOLIDNESS_SOLID || solidness == SOLIDNESS_FOR_OTHERS
}

func (solidness Solidness) OccupancyChanges(newSolidness Solidness) bool {
	return solidness.Occupies() != newSolidness.Occupies() || solidness.Carves() != newSolidness.Carves()
}