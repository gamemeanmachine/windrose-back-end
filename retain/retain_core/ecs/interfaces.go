package ecs

/**
 * This interface will be exported, but will not be implementable outside
 *   the hierarchy of this package, unless derived from (composed by) a
 *   special class defined in THIS same package
 */
type Component interface {
	setEntity(*Entity)
	GetEntity() *Entity
	OnAdded()
}
