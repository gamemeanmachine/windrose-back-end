package ecs

import (
	"reflect"
)

/**
 * This is the base component. This will be exported and intended for the
 *   developers to use this as main members for their components.
 */
type BaseComponent struct {
	entity *Entity
}

/**
 * Event when the component is added to an entity.
 */
func (baseComponent *BaseComponent) OnAdded() {
	// nothing - implement yourself
}

/**
 * Locally implementing the setter (will be the actual way of distinguishing
 *   the current hierarchy).
 */
func (baseComponent *BaseComponent) setEntity(entity *Entity) {
	baseComponent.entity = entity
}

/**
 * This method is the complement but will serve to be called externally as well.
 */
func (baseComponent *BaseComponent) GetEntity() *Entity {
	return baseComponent.entity
}

/**
 * Now it is time to define objects.
 */
type Entity struct {
	components map[reflect.Type][]Component
}

/**
 * This one will be its constructor.
 */
func NewEntity() *Entity {
	return &Entity{map[reflect.Type][]Component{}}
}

/**
 * This one to add a component to an entity.
 */
func (entity *Entity) AddComponent(component Component) error {
	if component.GetEntity() != nil {
		return AlreadyAddedToAnObject
	} else {
		componentType := reflect.TypeOf(component)
		componentList, ok := entity.components[componentType]
		if !ok {
			componentList = []Component{}
		}
		entity.components[componentType] = append(componentList, component)
		component.setEntity(entity)
		component.OnAdded()
		return nil
	}
}

/**
 * This one lets you get the (first) component of certain type. You tell the
 *   type MyComponent by passing a &MyComponent{} struct.
 */
func (entity *Entity) GetComponent(componentTemplate Component) Component {
	componentType := reflect.TypeOf(componentTemplate)
	componentList, ok := entity.components[componentType]
	if !ok {
		return nil
	} else {
		// We are sure the list will not be empty
		return componentList[0]
	}
}
