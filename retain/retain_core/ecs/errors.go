package ecs

import "errors"

var AlreadyAddedToAnObject = errors.New("this component is already added to an entity")
