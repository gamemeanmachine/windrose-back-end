package interfaces

import (
	"errors"
)

/*****************************************************************************************
 *****************************************************************************************
 Commanded objects registrars (interfaces & utilities)
 *****************************************************************************************
 *****************************************************************************************/

/**
 * Errors for registration
 */
var IdAlreadyRegistered = errors.New("ID already registered")
var IdNotRegistered = errors.New("ID not registered")
var CannotRegisterNil = errors.New("cannot register a nil object")

/**
 * This interface maintains a list of registered objects. Often this will be used to
 *   know which objects dispatch actions to.
 */
type CommandedRegistrar interface {
	/* Will need these two methods to update which objects will it attend */
	Register(commanded Commanded) bool
	Unregister(commanded Commanded) bool
	Find(id ID) (Commanded, bool)
}

/**
 * This tool function will register the commanded into a registrar.
 */
func Register(commanded Commanded, registrar CommandedRegistrar) error {
	if commanded == nil {
		return CannotRegisterNil
	}
	if registrar.Register(commanded) {
		return nil
	} else {
		return IdAlreadyRegistered
	}
}

/**
 * This tool function will unregister the commanded from a registrar.
 */
func Unregister(commanded Commanded, registrar CommandedRegistrar) error {
	if commanded == nil {
		return nil
	}

	if registrar.Unregister(commanded) {
		return nil
	} else {
		return IdNotRegistered
	}
}

/**
 * This tool function finds a registered commanded in a registrar.
 */
func FindRegistered(id ID, registrar CommandedRegistrar) (Commanded, error) {
	result, found := registrar.Find(id)
	if !found {
		return nil, IdNotRegistered
	} else {
		return result, nil
	}
}

/*****************************************************************************************
 *****************************************************************************************
 Command parsers (interfaces, structs & utilities)
 *****************************************************************************************
 *****************************************************************************************/

/**
 * This interface parses a command. It returns action and its arguments.
 */
type CommandParser interface {
	/* And will need this one to parse and send an action to an attended object */
	ParseCommand(command string) (string, []interface{}, error)
}

/**
 * This interface detects the appropriate parser for a command.
 */
type CommandParserDetector interface {
	/* Detects the parser to use, or returns ("", false) if no parser is suitable for
	   the command  */
	DetectParser(command string) (string, CommandParser, bool)
}

/**
 * This struct has a list of available parsers. Parses the command and returns the
 *   action, arguments, and used parser.
 */
type CommandParserSet struct {
	parsers map[string]CommandParser
	detector CommandParserDetector
}

var NoSuitableParser = errors.New("no suitable parser for the command")
var MissingParser = errors.New("the parser is not registered")

/**
 * Parses a command, guessing the parser to use.
 */
func (commandParserSet *CommandParserSet) ParseCommand(command string) (action string, arguments []interface{}, usedParser string, error error) {
	var found bool
	var parser CommandParser
	usedParser, parser, found = commandParserSet.detector.DetectParser(command)
	if !found {
		return "", nil, "", NoSuitableParser
	}

	action, arguments, error = parser.ParseCommand(command)
	return action, arguments, usedParser, error
}

/**
 * Parses a command, specifying the parser to use.
 */
func (commandParserSet *CommandParserSet) ParseCommandUsing(usedParser, command string) (action string, arguments[]interface{}, error error) {
	parser, ok := commandParserSet.parsers[usedParser]
	if !ok {
		return "", nil, MissingParser
	}

	return parser.ParseCommand(command)
}

/*****************************************************************************************
 *****************************************************************************************
 Commanded objects (interfaces, aliases & utilities)
 *****************************************************************************************
 *****************************************************************************************/

/**
 * A generic function to be implemented by the user as they need. This one takes the
 *   instance into account.
 */
type CommandHandler = func(commanded Commanded, value ...interface{})

/**
 * A map of those generic functions.
 */
type CommandHandlers = map[string]CommandHandler

/**
 * This is an interface to a commanded object. It knows which commands will it attend.
 */
type Commanded interface {
	Identified

	/* Whether this process will trigger, or not. This will often be true */
	Receives() bool

	/* Map of commands it will respond to. This will never be changed, and will often
	     be instantiated/prepared only once by a given set of methods. */
	Commands() CommandHandlers
}

var UnknownCommand = errors.New("unknown command")
var CurrentlyNotReceiving = errors.New("this object is currently not receiving commands")

/**
 * This tool invokes a command on a commanded object, if the commanded object is
 *   currently receiving commands.
 */
func InvokeCommand(commanded Commanded, action string, arguments []interface{}) error {
	if commanded.Receives() {
		commands := commanded.Commands()
		command, ok := commands[action]
		if !ok {
			return UnknownCommand
		} else {
			command(commanded, arguments)
			return nil
		}
	}
	return CurrentlyNotReceiving
}

/*****************************************************************************************
 *****************************************************************************************
 Command dispatchers (interfaces & utilities)
 *****************************************************************************************
 *****************************************************************************************/

var NoSuitableRegistrar = errors.New("no suitable registrar found")

/**
 * Given an ID, a (parsed) action (name), and the key of a used parsed, this interface finds
 *   the appropriate registrar to search the id inside, or raises an error. This interface
 *   will be ignored if the commanded object is known beforehand, but will not if we only
 *   know the ID to dispatch the command to.
 *
 * This dispatcher doesn't find the object by ID, but rather find the related registrar to
 *   find an object with that ID, suitable for that action/parser. Most likely the parser
 *   will be ignored, though.
 */
type CommandDispatcher interface {
	Dispatch(id ID, action, parser string) (CommandedRegistrar, bool)
}

/**
 * This tool dispatches a command via a dispatcher.
 */
func Dispatch(id ID, action, parser string, dispatcher CommandDispatcher) (CommandedRegistrar, error) {
	registrar, found := dispatcher.Dispatch(id, action, parser)
	if !found {
		return nil, NoSuitableRegistrar
	} else {
		return registrar, nil
	}
}

/*****************************************************************************************
 *****************************************************************************************
 Command pipeline (interfaces, structs, aliases & utilities)
 *****************************************************************************************
 *****************************************************************************************/

/**
 * This struct will have a parser set and a dispatcher. It will be used to attend a command,
 *   try parsing it, then dispatching it.
 */
type CommandPipeline struct {
	commandParserSet *CommandParserSet
	dispatcher CommandDispatcher
}

var BadCommandPipelineParams = errors.New("parsers, detector, and dispatcher must not be nil when creating a command pipeline")

/**
 * This constructor builds a command pipeline by filling its private members.
 */
func NewCommandPipeline(parsers map[string]CommandParser, detector CommandParserDetector, dispatcher CommandDispatcher) (*CommandPipeline, error) {
	if parsers == nil || detector == nil || dispatcher == nil {
		return nil, BadCommandPipelineParams
	}
	return &CommandPipeline{
		commandParserSet: &CommandParserSet{
			parsers: parsers,
			detector: detector,
		},
		dispatcher: dispatcher,
	}, nil
}

/**
 * Parses (by guess), dispatches, and executes a command in a commanded object.
 */
func (commandPipeline *CommandPipeline) Execute(id interface{}, command string) error {
	action, arguments, usedParser, err1 := commandPipeline.commandParserSet.ParseCommand(command)
	if err1 != nil {
		return err1
	}
	return commandPipeline.dispatchAndGo(id, action, arguments, usedParser)
}

/**
 * Parses (by being explicit), dispatches, and executes a command in a commanded object.
 */
func (commandPipeline *CommandPipeline) ExecuteUsing(id interface{}, command, parser string) error {
	action, arguments, err1 := commandPipeline.commandParserSet.ParseCommandUsing(parser, command)
	if err1 != nil {
		return err1
	}
	return commandPipeline.dispatchAndGo(id, action, arguments, parser)
}

func (commandPipeline *CommandPipeline) dispatchAndGo(id interface{}, action string, arguments []interface{}, parser string) error {
	commanded, ok := id.(Commanded)

	if !ok {
		registrar, err2 := Dispatch(id, action, parser, commandPipeline.dispatcher)
		if err2 != nil {
			return err2
		}

		var err3 error
		commanded, err3 = FindRegistered(commanded, registrar)
		if err3 != nil {
			return err3
		}
	}

	return InvokeCommand(commanded, action, arguments)
}