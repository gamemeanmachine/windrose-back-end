package interfaces

/**
 * This is an interface used to notify about changes in serializable objects.
 *
 * One will never use this interface directly, but the serializable objects
 *   will. However, implementors of serializable objects must provide a mean
 *   to access an instance (struct) implementing this interface.
 *
 * This notifier will tell whether an object is:
 *   * being added (after being created, this could serve to spawn the object
 *     inside its world of interaction)
 *   * being removed (before being removed, analogous to the previous point)
 *   * being updated (a single attribute)
 *   * triggering a particular action (this may be related, or not, to a
 *     particular method, but will primarily be used to tell when this object
 *     triggered a particular action)
 */
type SerializableNotificator interface {
	SerializableObjectAdded(id ID, attributes map[string]interface{})
	SerializableObjectRemoved(id ID)
	SerializableObjectUpdated(id ID, attribute string, value interface{})
	SerializableObjectActed(id ID, action string, params ...interface{})
}

/**
 * This is an interface used to serialize an object and reference a notifier.
 *   Notifiers are used to tell another end (perhaps a network) about CUD on
 *   the object implementing this interface. It also holds an ID to be used as
 *   unique key when notifying.
 */
type Serializable interface {
	Identified

	/* The notificator. This will seldom be changed */
	Notificator() SerializableNotificator

	/* Whether this notification process will trigger, or not. This will often be true */
	Notifies() bool

	/* Attributes. They may change (if there are attributes that work as transient state,
	   like position or grid content, they WILL change a lot!) */
	Attributes() map[string]interface{}
}

/**
 * Tool function to notify when the serializable is added.
 */
func NotifyAdded(serializable Serializable) {
	if serializable.Notifies() {
		serializable.Notificator().SerializableObjectAdded(serializable.ID(), serializable.Attributes())
	}
}

/**
 * Tool function to notify when the serializable is removed.
 */
func NotifyRemoved(serializable Serializable) {
	if serializable.Notifies() {
		serializable.Notificator().SerializableObjectRemoved(serializable.ID())
	}

}

/**
 * Tool function to notify when the serializable has changed one attribute.
 */
func NotifyUpdated(serializable Serializable, attribute string, value interface{}) {
	if serializable.Notifies() {
		serializable.Notificator().SerializableObjectUpdated(serializable.ID(), attribute, value)
	}
}

/**
 * Tool function to act with a custom action, which is beyond the simple attributes change.
 */
func NotifyActed(serializable Serializable, action string, params ...interface{}) {
	if serializable.Notifies() {
		serializable.Notificator().SerializableObjectActed(serializable.ID(), action, params)
	}
}
