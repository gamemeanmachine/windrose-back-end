package interfaces

import (
	"../types"
)

/**
 * This interface is just an ID holder and will be used for both serializable and commanded objects.
 */
type Identified interface {
	/* This ID will never be changed */
	GetID() types.ID
}
