WindRose Data Objects
=====================

Data objects are specs for other objects. This will mostly involve:

  * Regular (interaction-enabled) objects in ground.
  * NPCs (either neutral or enemies).
  * Interaction-disabled, or landscape, objects.

Data objects are intended to be loaded and stored in database. They usually serve as a template.