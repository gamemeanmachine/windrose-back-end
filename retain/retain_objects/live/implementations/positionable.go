package implementations

import (
	"../../../gmm/types"
	"../../../gmm/ortho"
	oInterfaces "../../../objects/live/interfaces"
	maps "../../../maps/live"
	"errors"
	"fmt"
)

var InvalidEvent = errors.New("invalid event key")
var AlreadyAttached = errors.New("object already attached to a map")
var NotAttached = errors.New("object not attached to a map")
var CannotAttachToNilMap = errors.New("cannot attach object to a nil map")

/**
 * A list of callbacks
 */
type EventCallbacks []oInterfaces.EventCallback

/**
 * This invokes the entire list of callbacks, one by one
 */
func (callbacks EventCallbacks) Trigger(args ...interface{}) {
	for _, value := range callbacks {
		value(args...)
	}
}

/**
 * A list of events (a map of list of callbacks)
 */
type Events map[string]EventCallbacks

/**
 * Appending a callback is only valid for currently existing events
 *   (you have to initialize the map beforehand)
 */
func (events Events) AddCallback(event string, callback oInterfaces.EventCallback) error {
	if event == "" {
		return InvalidEvent
	}
	callbacks, ok := events[event]
	if !ok {
		callbacks = EventCallbacks{}
	}
	events[event] = append(callbacks, callback)
	return nil
}

/**
 * Triggering an event will check validity and will call the trigger of the entire list
 */
func (events Events) Trigger(event string, args ...interface{}) error {
	callbacks, ok := events[event]
	if !ok {
		return InvalidEvent
	} else {
		callbacks.Trigger(args...)
		return nil
	}
}

/**
 * A positionable object implements, in back-end, the internal state which is already done in front-end.
 */
type Positionable struct {
	worldMap *maps.WorldMap
	x, y, width, height uint8
	solidness types.Solidness
	movement ortho.Direction
	events Events
}

/**
 * The constructor (it does not specify any map, but checks coordinates against bounding boxes)
 */
func NewPositionable(x, y, width, height uint8, solidness types.Solidness) (*Positionable, error) {
	if x + width >= oInterfaces.MaxWidth || y + height >= oInterfaces.MaxHeight {
		return nil, errors.New(fmt.Sprintf(
			"invalid position or dimensions (exceeding max values). Position: %dx%d, Size: %dx%d",
			x, y, width, height,
		))
	}
	return &Positionable{
		worldMap: nil, x: x, y: y, width: width,
	    height: height, solidness: solidness,
	    movement: ortho.DIRECTION_NONE,
	    events: Events{
	    	"OnAttached": EventCallbacks{},
	    	"OnDetached": EventCallbacks{},
			"OnTeleported": EventCallbacks{},
			"OnSolidnessChanged": EventCallbacks{},
	    	"OnMovementAllocated": EventCallbacks{},
	    	"OnMovementCancelled": EventCallbacks{},
	    	"OnMovementFinished": EventCallbacks{},
		},
    }, nil
}

/**
 * Data getters - will not validate but just return the data.
 */

func (positionable *Positionable) GetMap() *maps.WorldMap {
	return positionable.worldMap
}

func (positionable *Positionable) GetX() uint8 {
	return positionable.x
}

func (positionable *Positionable) GetY() uint8 {
	return positionable.y
}

func (positionable *Positionable) GetXY() (uint8, uint8) {
	return positionable.x, positionable.y
}

func (positionable *Positionable) GetWidth() uint8 {
	return positionable.width
}

func (positionable *Positionable) GetHeight() uint8 {
	return positionable.height
}

func (positionable *Positionable) GetSolidness() types.Solidness {
	return positionable.solidness
}

func (positionable *Positionable) GetMovement() ortho.Direction {
	return positionable.movement
}

/**
 * Event-related methods
 */

/**
 * Adding a callback to a positionable is the only public feature.
 */
func (positionable *Positionable) AddEventCallback(event string, callback oInterfaces.EventCallback) error {
	return positionable.events.AddCallback(event, callback)
}

/**
 * This one triggers the callbacks (this method is intentionally private!).
 */
func (positionable *Positionable) triggerEvent(event string, args ...interface{}) {
	positionable.events.Trigger(event, args...)
}

/**
 * Private methods that will ultimately be run inside the map handler.
 */

/**
 * Increments the map usage by the body.
 */
func (positionable *Positionable) incrementBody(handler *maps.WorldMapHandler) error {
	if positionable.solidness.Occupies() {
		return handler.IncrementBody(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
		)
	} else if positionable.solidness.Carves() {
		return handler.DecrementBody(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
		)
	}
	return nil
}

/**
 * Decrements the map usage by the body.
 */
func (positionable *Positionable) decrementBody(handler *maps.WorldMapHandler) error {
	if positionable.solidness.Occupies() {
		return handler.DecrementBody(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
		)
	} else if positionable.solidness.Carves() {
		return handler.IncrementBody(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
		)
	}
	return nil
}

/**
 * Increments the map usage by the adjacent side (depending on direction).
 */
func (positionable *Positionable) incrementAdjacent(handler *maps.WorldMapHandler) error {
	if positionable.solidness.Occupies() {
		return handler.IncrementAdjacent(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
			positionable.movement,
		)
	} else if positionable.solidness.Carves() {
		return handler.DecrementAdjacent(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
			positionable.movement,
		)
	}
	return nil
}

/**
 * Decrements the map usage by the adjacent side (depending on direction).
 */
func (positionable *Positionable) decrementAdjacent(handler *maps.WorldMapHandler) error {
	if positionable.solidness.Occupies() {
		return handler.DecrementAdjacent(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
			positionable.movement,
		)
	} else if positionable.solidness.Carves() {
		return handler.IncrementAdjacent(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
			positionable.movement,
		)
	}
	return nil
}

/**
 * Decrements the map usage by the adjacent-opposite side (depending on direction).
 */
func (positionable *Positionable) decrementOppositeAdjacent(handler *maps.WorldMapHandler) error {
	if positionable.solidness.Occupies() {
		return handler.DecrementAdjacent(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
			positionable.movement.Opposite(),
		)
	} else if positionable.solidness.Carves() {
		return handler.IncrementAdjacent(
			positionable.x, positionable.y,
			positionable.width, positionable.height,
			positionable.movement.Opposite(),
		)
	}
	return nil
}

/**
 * Tells whether the object can move (depending on direction).
 */
func (positionable *Positionable) canMoveTo(handler *maps.WorldMapHandler, direction ortho.Direction) (bool, error) {
	hitting := handler.IsHittingEdge(
		positionable.x, positionable.y, positionable.width, positionable.height, direction,
	)
	if hitting {
		return false, nil
	}
	blocked, err := handler.IsAdjacencyBlocked(
		positionable.x, positionable.y, positionable.width, positionable.height, direction,
	)
	if err != nil {
		return false, err
	}
	if blocked {
		return false, nil
	}
	if positionable.solidness.Traverses() {
		return true, nil
	}
	return handler.IsAdjacencyFree(
		positionable.x, positionable.y, positionable.width, positionable.height, direction,
	)
}

/**
 * Cancels current movement.
 */
func (positionable *Positionable) cancelMovement(handler *maps.WorldMapHandler) bool {
	if positionable.movement != ortho.DIRECTION_NONE {
		positionable.decrementAdjacent(handler)
		formerMovement := positionable.movement
		positionable.movement = ortho.DIRECTION_NONE
		positionable.triggerEvent("OnMovementCancelled", formerMovement)
		return true
	} else {
		return false
	}
}

/**
 * Public methods that cause side effects.
 */

/**
 * Attaches the object to a map.
 */
func (positionable *Positionable) Attach(worldMap *maps.WorldMap, x, y uint8) error {
	return worldMap.Run(func(handler *maps.WorldMapHandler) interface{} {
		if positionable.worldMap != nil {
			return AlreadyAttached
		}

		if worldMap == nil {
			return CannotAttachToNilMap
		}

		err := ortho.CheckBounds(ortho.BoundsFrom(ortho.Position{x, y}, positionable), worldMap)
		if err != nil {
			return err
		}

		positionable.worldMap = worldMap
		positionable.incrementBody(handler)
		positionable.triggerEvent("OnAttached", worldMap)
		return error(nil)
	}).(error)
}

/**
 * Detaches the object from its map.
 */
func (positionable *Positionable) Detach() error {
	return positionable.worldMap.Run(func(handler *maps.WorldMapHandler) interface{} {
		if positionable.worldMap == nil {
			return NotAttached
		}

		positionable.MovementCancel()
		positionable.decrementBody(handler)
		positionable.triggerEvent("OnDetached")
		return error(nil)
	}).(error)
}

/**
 * Teleports to certain coordinates.
 */
func (positionable *Positionable) Teleport(position ortho.Position) error {
	return positionable.worldMap.Run(func(handler *maps.WorldMapHandler) interface{} {
		if positionable.worldMap != nil && (positionable.x != position.GetX() || positionable.y != position.GetY()) {
			err := ortho.CheckBounds(ortho.BoundsFrom(position, positionable), positionable.worldMap)
			if err != nil {
				return err
			}

			positionable.cancelMovement(handler)
			positionable.decrementBody(handler)
			positionable.x = position.GetX()
			positionable.y = position.GetY()
			positionable.incrementBody(handler)
			positionable.triggerEvent("OnTeleported", position)
			return error(nil)
		} else {
			return NotAttached
		}
	}).(error)
}

/**
 * Changes the solidness.
 */
func (positionable *Positionable) SetSolidness(solidness types.Solidness) error {
	return positionable.worldMap.Run(func(handler *maps.WorldMapHandler) interface{} {
		if positionable.worldMap != nil {
			if positionable.solidness.OccupancyChanges(solidness) {
				positionable.cancelMovement(handler)
				positionable.decrementBody(handler)
				positionable.solidness = solidness
				positionable.incrementBody(handler)
			} else {
				positionable.solidness = solidness
			}
			positionable.triggerEvent("OnSolidnessChanged", solidness)
			return error(nil)
		} else {
			return NotAttached
		}
	}).(error)
}

/**
 * Allocates a movement.
 */
func (positionable *Positionable) MovementAllocate(direction ortho.Direction) (bool, error) {
	ret := positionable.worldMap.Run(func(handler *maps.WorldMapHandler) interface{} {
		if positionable.worldMap != nil {
			if positionable.movement == ortho.DIRECTION_NONE {
				return []interface{}{ false, error(nil) }
			}
			canMove, err := positionable.canMoveTo(handler, direction)
			if err != nil {
				return []interface{}{ false, err }
			} else if canMove {
				positionable.movement = direction
				positionable.incrementAdjacent(handler)
				positionable.triggerEvent("OnMovementAllocated", direction)
				return []interface{}{ true, error(nil) }
			} else {
				return []interface{}{ false, error(nil) }
			}
		} else {
			return []interface{}{ false, NotAttached }
		}
	}).([]interface{})
	return ret[0].(bool), ret[1].(error)
}

/**
 * Confirms the allocated movement.
 */
func (positionable *Positionable) MovementConfirm() (bool, error) {
	ret := positionable.worldMap.Run(func(handler *maps.WorldMapHandler) interface{} {
		if positionable.worldMap != nil {
			movement := positionable.movement
			if movement != ortho.DIRECTION_NONE {
				switch movement {
				case ortho.DIRECTION_UP:
					positionable.y--
				case ortho.DIRECTION_DOWN:
					positionable.y++
				case ortho.DIRECTION_LEFT:
					positionable.x--
				case ortho.DIRECTION_RIGHT:
					positionable.y++
				}
				positionable.decrementOppositeAdjacent(handler)
				positionable.movement = ortho.DIRECTION_NONE
				positionable.triggerEvent("OnMovementAllocated", movement)
				return []interface{}{ true, error(nil) }
			} else {
				return []interface{}{ false, error(nil) }
			}
		} else {
			return []interface{}{ false, NotAttached }
		}
	}).([]interface{})
	return ret[0].(bool), ret[1].(error)
}

/**
 * Cancels the allocated movement.
 */
func (positionable *Positionable) MovementCancel() (bool, error) {
	ret := positionable.worldMap.Run(func(handler *maps.WorldMapHandler) interface{} {
		if positionable.worldMap != nil {
			return []interface{}{ positionable.cancelMovement(handler), error(nil) }
		} else {
			return []interface{}{ false, NotAttached }
		}
	}).([]interface{})
	return ret[0].(bool), ret[1].(error)
}