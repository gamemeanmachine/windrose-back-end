package interfaces

import (
	"../../../gmm/types"
	maps "../../../maps/live"
	"../../../gmm/ortho"
)

const MaxWidth = maps.MaxWidth
const MaxHeight = maps.MaxHeight

// Event callbacks are intended to send events to the positionable elements
type EventCallback func(args ...interface{})

/**
 * A positioned objects has a position and a size.
 * Position will be allowed as:
 * - X in [0 .. Map width - width]
 * - Y in [0 .. Map height - height]
 * Size will be allowed as:
 * - width in [1 .. Map width]
 * - height in [1 .. Map height]
 * Also, a solidness.
 */

/**
 * Defines which methods satisfy this interface. We will have one base structure
 *   satisfying these methods, while other composed structures will satisfy them
 *   by composition.
 */
type IPositionable interface {
	// Handling the map I am attached to.
	Attach(worldMap *maps.WorldMap, x, y uint8) error
	Detach() error
	GetMap() *maps.WorldMap
	// Teleport (abrupt change of x/y coordinates), and getting
	//   x, y, or (x, y) coordinates.
	Teleport(position ortho.Position) error
	GetX() uint8
	GetY() uint8
	GetXY() (uint8, uint8)
	// Solidness
	SetSolidness(solidness types.Solidness) error
	GetSolidness() types.Solidness
	// width / height are read-only.
	GetWidth() uint8
	GetHeight() uint8
	// Allows back-end movement.
	// These will mostly used by Movable objects, but also by an administrator feature.
	//   In this latter case, no speed or gradual movement will be necessarily involved.
	MovementAllocate(direction ortho.Direction) error
	MovementConfirm() error
	MovementCancel() error
	GetMovement() ortho.Direction
	// Handles events
	AddEventCallback(event string, callback EventCallback) error
}

/**
 * Adds more methods on top of the abstract methods of the interface (this type
 *   satisfies by itself the IPositionable interface).
 *
 * Notes: These additional methods should be treated in particular when dealing
 *   with this structure, which should be instantiated mostly as just a helper
 *   or decorator around a given IPositionable element. While this element itself
 *   satisfies IPositionable, if you want to pass this element to another method,
 *   don't forget to call positionable.Unwrap(). Otherwise, everything will work
 *   but if you don't take care, it may become quite inefficient due to actually
 *   unnecessary dereference calls in the chain.
 */
type Positionable struct {
	IPositionable
}

/**
 * Detaches from map (if in any), and attaches to another one.
 */
func (positionable Positionable) AttachForced(worldMap *maps.WorldMap, x, y uint8) error {
	if positionable.GetMap() != nil {
		err := positionable.Detach()
		if err != nil {
			return err
		}
	}
	return positionable.Attach(worldMap, x, y)
}

/**
 * Attaches to another map, holding same position of the last map it was in.
 */
func (positionable Positionable) AttachOnSamePosition(worldMap *maps.WorldMap) error {
	return positionable.Attach(worldMap, positionable.GetX(), positionable.GetY())
}

/**
 * Detaches from map (if in any), and attaches to another one, on same position.
 */
func (positionable Positionable) AttachOnSamePositionForced(worldMap *maps.WorldMap) error {
	if positionable.GetMap() != nil {
		err := positionable.Detach()
		if err != nil {
			return err
		}
	}
	return positionable.AttachOnSamePosition(worldMap)
}

/**
 * Teleports to a different X position
 */
func (positionable Positionable) TeleportX(x uint8) error {
	return positionable.Teleport(ortho.Position{x, positionable.GetY()})
}

/**
 * Teleports to a different Y position
 */
func (positionable Positionable) TeleportY(x uint8) error {
	return positionable.Teleport(ortho.Position{x, positionable.GetY()})
}

/**
 * Gets the rightmost coordinate occupied by the object.
 */
func (positionable Positionable) GetXf() uint8 {
	return positionable.GetX() + positionable.GetWidth() - 1
}

/**
 * Gets the bottommost coordinate occupied by the object.
 */
func (positionable Positionable) GetYf() uint8 {
	return positionable.GetY() + positionable.GetHeight() - 1
}

/**
 * Gets the inner IPositionable
 */
func (positionable Positionable) Unwrap() IPositionable {
	return positionable.IPositionable
}

/**
 * Builds a Positionable from an IPositionable
 */
func Wrap(positionable IPositionable) Positionable {
	e, ok := positionable.(Positionable)
	if ok {
		return e
	} else {
		return Positionable{e}
	}
}
