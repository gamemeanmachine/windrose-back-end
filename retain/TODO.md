Hay que rehacer todo este framework, sin que tenga algo que ver con lo que ocurre en front-end necesariamente
(que algunas de esas cosas ocurren porque Unity se maneja de esa manera). Para arrancar, si nos centramos
solamente el en núcleo de WindRose, solo nos importan dos tipos de objetos:

- Mapas
- Entidades

Esto, aunque podamos tener comportamientos varios y que vayamos agregando (ej. teletransportacion estructural
al tocal un boundary de un mapa, al mapa vecino) o que ya tengamos (ej. teleports puntuales / portales).

Entidades
=========

Las entidades son objetos que pertenecen a un mapa, y van a tener las siguientes caracteristicas:

- Entidades quietas: Muebles, rocas, y plantas son buenos ejemplos.
- Entidades orientables: Son como los quietos, pero se les puede dar una rotación en 4 direcciones.
- Entidades móviles: Son objetos que tienen la capacidad de moverse.

La clase que va a implementar eso, como tal, no va a cambiar. Entonces, vamos a tener atributos de esta manera:

- mapa: Nos dice el mapa al que pertenece esta entidad. Puede ser nulo, si no está en ningún mapa.
- (x, y): Posición dentro del mapa. No tiene sentido si la entidad no pertenece a ningún mapa.
- (width, height): Ancho / alto de la entidad.

Para que todo esto tenga sentido, si el objeto está dentro de un mapa se satisfará la condición
(bajo pena de pánico):

- x + width <= map.width.
- y + height <= map.height.

Esto no varía nunca, realmente. Lo que se agrega entonces es la siguiente idea:

- tipo: {quieto, orientable, móvil}.
- orientación: Siempre hacia abajo en el caso de los quietos. Sino, puede orientarse y especificarse una dirección.
  Esto nos indica que de manera predeterminada una entidad siempre mira "hacia abajo".
- dirección de movimiento: Lo más importante a saber es que puede coincidir con la orientación pero no necesariamente
  debe serlo. Sin embargo, lo más común es que lo sea.

Partiendo de estos principios, 